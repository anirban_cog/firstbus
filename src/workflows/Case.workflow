<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Commendation_Thank_you_email_alert</fullName>
        <description>Commendation Thank you email alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sprint_5_Team_2/Gratitude_Email_for_Commendation</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_Aberdeen</fullName>
        <description>Email Alert For CCTV Footage Aberdeen</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Aberdeen</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_All</fullName>
        <description>Email Alert For CCTV Footage All</description>
        <protected>false</protected>
        <recipients>
            <recipient>All</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_Cymru</fullName>
        <description>Email Alert For CCTV Footage Cymru</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Cymru</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_Essex</fullName>
        <description>Email Alert For CCTV Footage Essex</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Essex</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_First_Eastern_Counties</fullName>
        <description>Email Alert For CCTV Footage First Eastern Counties</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_First_Eastern_Counties</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_Glasgow</fullName>
        <description>Email Alert For CCTV Footage Glasgow</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Glasgow</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_Hampshire_Berkshire_Dorset</fullName>
        <description>Email Alert For CCTV Footage Hampshire Berkshire Dorset</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Hampshire_Berkshire_Dorset</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_Manchester</fullName>
        <description>Email Alert For CCTV Footage Manchester</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Manchester</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_Midlands</fullName>
        <description>Email Alert For CCTV Footage Midlands</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Midlands</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_Scotland_East</fullName>
        <description>Email Alert For CCTV Footage Scotland East</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Scotland_East</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_South_West</fullName>
        <description>Email Alert For CCTV Footage South West</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_South_West</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_South_Yorkshire</fullName>
        <description>Email Alert For CCTV Footage South Yorkshire</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_South_Yorkshire</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_West_Yorkshire</fullName>
        <description>Email Alert For CCTV Footage West Yorkshire</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_West_Yorkshire</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_West_of_England</fullName>
        <description>Email Alert For CCTV Footage West of England</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_West_of_England</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_CCTV_Footage_York</fullName>
        <description>Email Alert For CCTV Footage York</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_York</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_Aberdeen</fullName>
        <description>Email Alert For Injury Aberdeen</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Aberdeen</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_All</fullName>
        <description>Email Alert For Injury All</description>
        <protected>false</protected>
        <recipients>
            <recipient>All</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_Cymru</fullName>
        <description>Email Alert For Injury Cymru</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Cymru</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_Essex</fullName>
        <description>Email Alert For Injury Essex</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Essex</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_First_Eastern_Counties</fullName>
        <description>Email Alert For Injury First Eastern Counties</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_First_Eastern_Counties</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_Glasgow</fullName>
        <description>Email Alert For Injury Glasgow</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Glasgow</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_Hampshire_Berkshire_Dorset</fullName>
        <description>Email Alert For Injury Hampshire Berkshire Dorset</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Hampshire_Berkshire_Dorset</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_Manchester</fullName>
        <description>Email Alert For Injury Manchester</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Manchester</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_Midlands</fullName>
        <description>Email Alert For Injury Midlands</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Midlands</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_South_West</fullName>
        <description>Email Alert For Injury South West</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_South_West</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_South_Yorkshire</fullName>
        <description>Email Alert For Injury South Yorkshire</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_South_Yorkshire</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_West_YorkShire</fullName>
        <description>Email Alert For Injury West YorkShire</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_West_Yorkshire</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_West_of_England</fullName>
        <description>Email Alert For Injury West of England</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_West_of_England</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Injury_York</fullName>
        <description>Email Alert For Injury York</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_York</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Injury</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_All</fullName>
        <description>Email Alert For Major/Crisis Incident All</description>
        <protected>false</protected>
        <recipients>
            <recipient>All</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_Cymru</fullName>
        <description>Email Alert For Major/Crisis Incident Cymru</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Cymru</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_Essex</fullName>
        <description>Email Alert For Major/Crisis Incident Essex</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Essex</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_First_Eastern_Counties</fullName>
        <description>Email Alert For Major/Crisis Incident First Eastern Counties</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_First_Eastern_Counties</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_Glasgow</fullName>
        <description>Email Alert For Major/Crisis Incident Glasgow</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Glasgow</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_Hampshire_Berkshire_Dorset</fullName>
        <description>Email Alert For Major/Crisis Incident Hampshire Berkshire Dorset</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Hampshire_Berkshire_Dorset</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_Machester</fullName>
        <description>Email Alert For Major/Crisis Incident Machester</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Manchester</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>OD_Manchester</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_Midlands</fullName>
        <description>Email Alert For Major/Crisis Incident Midlands</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Midlands</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_Scotland_East</fullName>
        <description>Email Alert For Major/Crisis Incident Scotland East</description>
        <protected>false</protected>
        <recipients>
            <recipient>OD_Scotland_East</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_South_West</fullName>
        <description>Email Alert For Major/Crisis Incident South West</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_South_West</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_West_Yorkshire</fullName>
        <description>Email Alert For Major/Crisis Incident West Yorkshire</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_West_Yorkshire</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>OD_West_Yorkshire</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_West_of_England</fullName>
        <description>Email Alert For Major/Crisis Incident West of England</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_West_of_England</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>OD_West_of_England</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_York</fullName>
        <description>Email Alert For Major/Crisis Incident York</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_York</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Major_Crisis_Incident_southYorkshire</fullName>
        <description>Email Alert For Major/Crisis Incident southYorkshire</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_South_Yorkshire</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>OD_South_Yorkshire</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_Major_Crisis_Incident_Aberdeen</fullName>
        <description>Email Alert Major/Crisis Incident Aberdeen</description>
        <protected>false</protected>
        <recipients>
            <recipient>MD_Aberdeen</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>elaine.rosscraig@firstgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928Major_Crisis_Incident_being_reported</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Escalated_Cases_to_Team_Leader</fullName>
        <ccEmails>pooja.kadam2@cognizant.com</ccEmails>
        <description>Email Alert for Escalated Cases to Team Leader</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Service Team leader</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_Escalate_to_team_leader</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_To_Customers_regarding_Lost_Property</fullName>
        <description>Email Notification To Customers regarding Lost Property</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sprint_5_Team_2/Lost_Property_Notification_for_Customer</template>
    </alerts>
    <alerts>
        <fullName>Escalation_Alert_for_DEpot</fullName>
        <description>Escalation Alert for DEpot</description>
        <protected>false</protected>
        <recipients>
            <recipient>Depot</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>B_16928/B_16928_CCTV_Footage_requested</template>
    </alerts>
    <fieldUpdates>
        <fullName>Closing_Case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Closing Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Report_DateTime_Field_Update</fullName>
        <field>Date_of_Reported_Event__c</field>
        <formula>DATETIMEVALUE(TEXT(Incident_Date__c) &amp; &quot; &quot; &amp; Incident_Time__c)</formula>
        <name>Event Report DateTime Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incident_Date_Update</fullName>
        <field>Incident_Date__c</field>
        <formula>DATEVALUE( Date_of_Reported_Event__c )</formula>
        <name>Incident Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Incident_Time_Value</fullName>
        <field>Incident_Time__c</field>
        <formula>Text(TIMEVALUE( Date_of_Reported_Event__c ))</formula>
        <name>Update Incident Time Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Close Case For Compensation</fullName>
        <actions>
            <name>Closing_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(4 AND (1 OR 3) AND 2)</booleanFilter>
        <criteriaItems>
            <field>Case.Details_Stored__c</field>
            <operation>includes</operation>
            <value>Compensation provided</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Compensation_Amount__c</field>
            <operation>lessOrEqual</operation>
            <value>50</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Details_Stored__c</field>
            <operation>includes</operation>
            <value>Compensation provided</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Product Help</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close case after assignment</fullName>
        <actions>
            <name>Closing_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 AND 2 AND 3 AND 4 AND 5) AND (6 AND 7) AND 8 AND 9)</booleanFilter>
        <criteriaItems>
            <field>Case.Details_Stored__c</field>
            <operation>excludes</operation>
            <value>CCTV Footage requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Details_Stored__c</field>
            <operation>excludes</operation>
            <value>Compensation provided</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Details_Stored__c</field>
            <operation>excludes</operation>
            <value>Compensation provided</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Details_Stored__c</field>
            <operation>excludes</operation>
            <value>Further Investigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Details_Stored__c</field>
            <operation>excludes</operation>
            <value>Further Investigation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Details_Stored__c</field>
            <operation>excludes</operation>
            <value>Using Mobile</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Details_Stored__c</field>
            <operation>excludes</operation>
            <value>Poor Driving Standards</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Details_Stored__c</field>
            <operation>excludes</operation>
            <value>Sexual/Homophobic/Racial Abuse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notEqual</operation>
            <value>Master Case</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email After Case escalation</fullName>
        <actions>
            <name>Escalation_Alert_for_DEpot</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>IsEscalated</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Escalation To CS Team Leader Rule</fullName>
        <actions>
            <name>Email_Alert_for_Escalated_Cases_to_Team_Leader</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Details_Stored__c</field>
            <operation>includes</operation>
            <value>Escalate to team leader</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lost Property Notification for Customer</fullName>
        <actions>
            <name>Email_Notification_To_Customers_regarding_Lost_Property</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Lost Property</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Thank you email Alert</fullName>
        <actions>
            <name>Commendation_Thank_you_email_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Commendation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Details_Stored__c</field>
            <operation>includes</operation>
            <value>Driver Details,Ease of finding information,Functionality/ease of use,Happy with response provided</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Driver_Details__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF Update Date of Reported Event</fullName>
        <actions>
            <name>Event_Report_DateTime_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Source__c , &apos;Web&apos;), AND(NOT( ISNULL(Incident_Date__c )),NOT(ISNULL(Incident_Time__c  )) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF Update Incident Date and Time</fullName>
        <actions>
            <name>Incident_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Incident_Time_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Date_of_Reported_Event__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
