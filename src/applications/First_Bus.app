<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>First Bus</label>
    <logo>SharedDocuments/FBLogo.png</logo>
    <tab>standard-Case</tab>
    <tab>standard-Contact</tab>
    <tab>Transaction_Processor__c</tab>
    <tab>Purchases_Processor__c</tab>
    <tab>Transaction_Summary__c</tab>
    <tab>et4ae5__ExactTargetPhoenix</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
</CustomApplication>
