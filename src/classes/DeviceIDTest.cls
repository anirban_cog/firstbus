/*************************************************************
* Author: Ayan Hore
* Created Date: 12/07/17
* Version1: This is the initial version
* Details: Test class for Device_ID__c related apex components
* Purpose: Code coverage for Device_ID__c related apex components
**************************************************************/
@istest
public class DeviceIDTest {
    public static final List<KeyValue> lstDeviceKV = new List<KeyValue>();
    public static final List<KeyValue> lstDeviceKVupdate = new List<KeyValue>();
    public static final List<KeyValue> contactList = new List<KeyValue>(); 
    Static final String Stringtype='String';
    @isTest
    static void deviceIdTestMethod1(){
        test.startTest();
        	createDeviceIDdata();
        test.stopTest();
    }
    public static void createDeviceIDdata(){
        TestDataComplexFunctions testDataComplexFunctions= new testDataComplexFunctions();
        
        contactList.add(new KeyValue('MTickets_Reference_ID__c','C12',Stringtype));
        contactList.add(new KeyValue('FirstName','TestFirst',Stringtype));
        contactList.add(new KeyValue('LastName','TestLast',Stringtype));
        
        lstDeviceKV.add(new KeyValue('C3_User_Id__c','C12',Stringtype));
        lstDeviceKV.add(new KeyValue('AppVersion__c','V1',Stringtype));
        lstDeviceKV.add(new KeyValue('Device__c','D1',Stringtype));
        
        Map<System.Type, List<KeyValue>> ContactAndDeviceIdMap = new Map<System.Type, List<KeyValue>>();
        ContactAndDeviceIdMap.put(Contact.class,contactList);
        ContactAndDeviceIdMap.put(Device_Id__c.class,lstDeviceKV);
        testDataComplexFunctions.insertContactAndDeviceId(ContactAndDeviceIdMap,null);     
        System.assertEquals(1,[select count() from Device_ID__c]);
        TestDataUpdate testDataUpdate = new TestDataUpdate();
        
        lstDeviceKVupdate.add(new KeyValue('OS__c','Android',Stringtype));
        testDataUpdate.updateDeviceID(lstDeviceKV,lstDeviceKVupdate,null);
        
    }
    
}