public class FBUtilityClass 
{
   public static final String SOURCE_MTICKET = 'MTicket';
   public static final String SOURCE_WEB = 'Web';
   public static final String SOURCE_WIFI = 'Wifi';
   public static final String SOURCE_CSAGENT = 'CS Agent';
   public static final String SOURCE_WEBCHAT = 'Web Chat';
   public static final String SOURCE_SOCIALCS = 'Social CS';
   public static final String SOURCE_PROSPECT = 'Prospect';
}