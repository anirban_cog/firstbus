/*************************************************************
* Author: Ayan Hore
* Created Date: 01/05/18
* Version1: B-16741
* Details: Test class for DataMigrationBatch & DataMigrationLogic
* Purpose: Test class for DataMigrationBatch & DataMigrationLogic
**************************************************************/
@isTest
public class DataMigrationTest {
    static TestDataComplexFunctions tdc = new TestDataComplexFunctions(); 
    @isTest
    static void testMethod1(){
        Profile prof = [select Id, Name from Profile where Name = 'First Group Standard User'];
        User stdUser = new User(Alias = 'standt', Email='standardtestClassUser@firstbus.co.uk', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = prof.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardtestClassUser@firstbus.co.uk');
        insert stdUser;
        
        List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
        CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-19',Account_API__c='PersonEmail',Contact_API__c='Email'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-21',Account_API__c='First_Purchase_Type__c',Contact_API__c='First_Purchase_Type__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-17',Account_API__c='Like_to_receive_offers__c',Contact_API__c='Like_to_receive_offers__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-7',Account_API__c='MailingList3__c',Contact_API__c='MailingList3__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-8',Account_API__c='LastPurchaseDate__c',Contact_API__c='Last_Purchase_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-9',Account_API__c='Last_Purchase_Type__c',Contact_API__c='Last_Purchase_Type__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-10',Account_API__c='Reason_for_use__c',Contact_API__c='Reason_for_use__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-11',Account_API__c='Source__c',Contact_API__c='Source__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-12',Account_API__c='Source_Created_Date__c',Contact_API__c='Source_System_Created_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-13',Account_API__c='Source_Amended_Date__c',Contact_API__c='Source_System_Updated_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-14',Account_API__c='University__c',Contact_API__c='University__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-1',Account_API__c='Age_Range__c',Contact_API__c='Age_Group__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-2',Account_API__c='Control_Group__c',Contact_API__c='Control_Group__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-3',Account_API__c='Gender__c',Contact_API__c='Gender__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-4',Account_API__c='Last_Purchase_Count__c',Contact_API__c='Last_Purchase_Count__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-5',Account_API__c='MailingList1__c',Contact_API__c='MailingList1__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-6',Account_API__c='MailingList2__c',Contact_API__c='MailingList2__c'));
        insert CSObjList;
        
        System.runAs(stdUser){
            Debug_Error__c[] errors = new Debug_Error__c[]{}; 
            
            List<KeyValue> ky = new List<KeyValue>();
            RecordType rt = [select Id, Name from RecordType where sObjectType = 'Account' AND Name = 'Person Account'].get(0);
            ky.add(new KeyValue('RecordTypeId',rt.Id,'String'));
            ky.add(new KeyValue('Source__c','mTicket','String'));
            ky.add(new KeyValue('Phone','0714568978','String'));
            ky.add(new KeyValue('Gender__c','Decline to state','String'));
            //tdc.insertAccount(null, errors);
            TestDataUpdate tdu = new TestDataUpdate();
            tdu.updateAccount(null,ky,errors);
            
            Account testAccount = [select Id,Name,FirstName,PersonEmail,First_Purchase_Type__c,Age_Range__c,Reason_for_use__c,Source__c,Source_Created_Date__c 
               ,Source_Amended_Date__c,University__c,LastName,Like_to_receive_offers__c,Control_Group__c,
               Gender__c,Last_Purchase_Count__c,MailingList1__c,MailingList2__c,MailingList3__c,LastPurchaseDate__c,Last_Purchase_Type__c
                from Account LIMIT 1];
            system.assert(testAccount.Id != NULL, 'Account Not Created');
            Test.startTest();
            	database.executeBatch(new DataMigrationBatch(),200);
            Test.stopTest();
            Contact testContact = [select Id,Name,FirstName,Email,First_Purchase_Type__c,Age_Group__c,Reason_for_use__c,Source__c,Source_System_Created_Date__c
                ,Source_System_Updated_Date__c,University__c,LastName,Account_External_Id__c
                ,Like_to_receive_offers__c,Control_Group__c,Gender__c,Last_Purchase_Count__c,
                MailingList1__c,MailingList2__c,MailingList3__c,Last_Purchase_Date__c,Last_Purchase_Type__c
                 from Contact where Account_External_Id__c = :testAccount.Id];
            system.assert(testContact.Id != NULL, 'Contact Not Created');
            /*Field validation starts*/
            
            System.assert(testAccount.Name == testContact.Name,'Name mapping failed');
            System.assert(testAccount.PersonEmail == testContact.Email,'Email mapping failed');
            System.assert(testAccount.First_Purchase_Type__c == testContact.First_Purchase_Type__c,'First purchase mapping failed');
            
            
            system.assert(testAccount.FirstName == testContact.FirstName, 'FirstName mapping failed');
            system.assert(testAccount.Age_Range__c == testContact.Age_Group__c, 'Age group mapping failed');
            system.assert(testAccount.Reason_for_use__c == testContact.Reason_for_use__c, 'Reason for mapping failed');
            system.assert(testAccount.Source__c == testContact.Source__c, 'Source mapping failed');
            system.assert(testAccount.Source_Created_Date__c == testContact.Source_System_Created_Date__c, 'Source Created Date mapping failed');
            system.assert(testAccount.Source_Amended_Date__c == testContact.Source_System_Updated_Date__c, 'Source_Amended_Date mapping failed');
            
            system.assert(testAccount.University__c == testContact.University__c, 'University mapping failed');
            system.assert(testAccount.LastName == testContact.LastName, 'LastName mapping failed');
            system.assert(testAccount.Like_to_receive_offers__c == testContact.Like_to_receive_offers__c, 'LastName mapping failed');
            system.assert(testAccount.Control_Group__c == testContact.Control_Group__c, 'Control Group mapping failed');
            
            system.assert(testAccount.Last_Purchase_Count__c == testContact.Last_Purchase_Count__c, 'Last Purchase Count mapping failed');
            system.assert(testAccount.MailingList1__c == testContact.MailingList1__c, 'MailingList1 mapping failed');
            system.assert(testAccount.MailingList2__c == testContact.MailingList2__c, 'MailingList2 mapping failed');
            system.assert(testAccount.MailingList3__c == testContact.MailingList3__c, 'MailingList3 mapping failed');
            system.assert(testAccount.LastPurchaseDate__c == testContact.Last_Purchase_Date__c, 'LastPurchaseDate mapping failed');
            system.assert(testAccount.Last_Purchase_Type__c == testContact.Last_Purchase_Type__c, 'Last Purchase Type mapping failed');
            

            /*Field validation ends*/
        }
    }
}