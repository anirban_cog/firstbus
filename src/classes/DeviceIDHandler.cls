/*************************************************************
* Author: Ayan Hore
* Created Date: 12/07/17
* Version1: B-16878
* Details: Handler class for Device_ID__c trigger
* Purpose:Hosts handler methods
**************************************************************/
public class DeviceIDHandler extends TriggerBaseHandler{
    private DeviceIDLogic deviceLogic;
    
    public DeviceIDHandler(){
        deviceLogic = new DeviceIDLogic();
    }
    
    public override void beforeInsert(SObject[] newObj, CommitHandler ch){
        deviceLogic.checkAndConnectContact(newObj); 
    }
    
    public override void beforeUpdate(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){ 
        deviceLogic.checkAndConnectContact(newObj);  
    }

}