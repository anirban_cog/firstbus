/*************************************************************
* Author: Ayan Hore
* Created Date: 12/07/17
* Version1: This is the initial version
* Details: Test class for Transaction_Processor__c related apex components
* Purpose: Code coverage for Transaction_Processor__c related apex components
**************************************************************/
@istest
public class TransactionProcessorTest{
    public static final List<KeyValue> transactionProcessorKV = new List<KeyValue>();
    public static final List<KeyValue> transactionProcessorKVupdate = new List<KeyValue>();
    public static final List<KeyValue> contactKV = new List<KeyValue>();
    public static final List<KeyValue> accountKV = new List<KeyValue>();
    @isTest
    static void testMethod1(){
        List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
        CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-19',Account_API__c='PersonEmail',Contact_API__c='Email'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-21',Account_API__c='First_Purchase_Type__c',Contact_API__c='First_Purchase_Type__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-17',Account_API__c='Like_to_receive_offers__c',Contact_API__c='Like_to_receive_offers__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-7',Account_API__c='MailingList3__c',Contact_API__c='MailingList3__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-8',Account_API__c='LastPurchaseDate__c',Contact_API__c='Mticket_Last_Purchase_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-9',Account_API__c='Last_Purchase_Type__c',Contact_API__c='Mticket_Last_Purchase_Type__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-10',Account_API__c='Reason_for_use__c',Contact_API__c='Reason_for_use__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-11',Account_API__c='Source__c',Contact_API__c='Source__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-12',Account_API__c='Source_Created_Date__c',Contact_API__c='Source_System_Created_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-13',Account_API__c='Source_Amended_Date__c',Contact_API__c='Source_System_Updated_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-14',Account_API__c='University__c',Contact_API__c='University__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-1',Account_API__c='Age_Range__c',Contact_API__c='Age_Group__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-2',Account_API__c='Control_Group__c',Contact_API__c='Control_Group__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-3',Account_API__c='Gender__c',Contact_API__c='Gender__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-4',Account_API__c='Last_Purchase_Count__c',Contact_API__c='Last_Purchase_Count__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-5',Account_API__c='MailingList1__c',Contact_API__c='MailingList1__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-6',Account_API__c='MailingList2__c',Contact_API__c='MailingList2__c'));
        insert CSObjList;
        
        Test.startTest();
        createTestData1();
        createTestData2();
        Test.stopTest();
    }
    public static void createTestData1(){
        TestDataComplexFunctions testDataComplexFunctions=new TestDataComplexFunctions();
        
        Account acc=new Account();
        Contact con=new Contact();
        //Creating account
        accountKV.add(new KeyValue('FirstName','FirstBus','String'));
        accountKV.add(new KeyValue('LastName','Account','String'));
        accountKV.add(new KeyValue('Email__c','account@test.com','String')); 
        acc=testDataComplexFunctions.insertAccount(accountKV,null);
        //System.assertEquals(1, [select count() from Account]);
        
        //Creating contact record
        contactKV.add(new KeyValue('FirstName','WebFirst Name','String'));
        contactKV.add(new KeyValue('LastName','WebLast Name','String'));
        //contactKV.add(new KeyValue('MTickets_Reference_ID__c','MT1Id','String'));
        contactKV.add(new KeyValue('AccountName',String.valueOf(acc.id),'String'));
        contactKV.add(new KeyValue('Email__c','account@test.com','String'));  
        con=testDataComplexFunctions.insertContact(contactKV,null);
                
        //Creating Transaction_Processor__c record
        DateTime currentDateTime = DateTime.now();
        transactionProcessorKV.add(new KeyValue('First_Name__c','FirstName','String'));
        transactionProcessorKV.add(new KeyValue('Last_Name__c','LastName','String'));
        transactionProcessorKV.add(new KeyValue('Amount__c','500.20','DECIMAL'));
        transactionProcessorKV.add(new KeyValue('Payment_Form__c','Cash','String'));
        transactionProcessorKV.add(new KeyValue('PSP_Transaction_ID__c','P-09','String'));
        transactionProcessorKV.add(new KeyValue('Purchase_Date__c',String.valueOf(currentDateTime),'DateTime'));
        transactionProcessorKV.add(new KeyValue('Transaction_Id__c','T-09','String'));
        transactionProcessorKV.add(new KeyValue('Web_Reference_ID__c','WebId09','String'));
        transactionProcessorKV.add(new KeyValue('OpCo_Organisation__c','OpOrg','String'));
        transactionProcessorKV.add(new KeyValue('Other_Street__c','Transaction-OtherStreet','String'));
        transactionProcessorKV.add(new KeyValue('Core3_ID__c','C-968532','String'));
        transactionProcessorKV.add(new KeyValue('Core3_Device_Id__c','C-968532','String'));
        transactionProcessorKV.add(new KeyValue('Other_City__c','Transaction-OtherCity','String'));
        transactionProcessorKV.add(new KeyValue('Other_Postal_Code__c','254136','String'));
        transactionProcessorKV.add(new KeyValue('Other_Country__c','COuntry','String'));
        transactionProcessorKV.add(new KeyValue('Salutation__c','Mr','String'));
        transactionProcessorKV.add(new KeyValue('Phone__c',' 0732587410','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Postal_Code__c','457869','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_City__c',' Transaction-MailingCity','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Street__c',' Transaction-MailingStreet','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Country__c',' Transaction-MailingCountry','String'));
        transactionProcessorKV.add(new KeyValue('Last_Name__c',' Transaction-LastName','String'));
        transactionProcessorKV.add(new KeyValue('First_Name__c',' Transaction-FIrstName','String'));
        transactionProcessorKV.add(new KeyValue('Source__c','Web','String'));
        testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        //System.assertEquals(1, [select count() from Transaction_Processor__c]);
        
        //Creating Another Transaction_Processor__c record
        transactionProcessorKV.add(new KeyValue('First_Name__c','FirstName1','String'));
        transactionProcessorKV.add(new KeyValue('Last_Name__c','LastName1','String'));
        transactionProcessorKV.add(new KeyValue('Amount__c','500.20','DECIMAL'));
        transactionProcessorKV.add(new KeyValue('Payment_Form__c','Cash','String'));
        transactionProcessorKV.add(new KeyValue('PSP_Transaction_ID__c','P-09','String'));
        transactionProcessorKV.add(new KeyValue('Purchase_Date__c',String.valueOf(currentDateTime),'DateTime'));
        transactionProcessorKV.add(new KeyValue('Transaction_Id__c','T-09','String'));
        transactionProcessorKV.add(new KeyValue('Email__c','account@test.com','String'));
        transactionProcessorKV.add(new KeyValue('Web_Reference_ID__c','WebId09','String'));
        transactionProcessorKV.add(new KeyValue('OpCo_Organisation__c','OpOrg','String'));
        transactionProcessorKV.add(new KeyValue('Other_Street__c','Transaction-OtherStreet','String'));
        transactionProcessorKV.add(new KeyValue('Core3_ID__c','C-968532','String'));
        transactionProcessorKV.add(new KeyValue('Core3_Device_Id__c','C-968532','String'));
        transactionProcessorKV.add(new KeyValue('Other_City__c','Transaction-OtherCity','String'));
        transactionProcessorKV.add(new KeyValue('Other_Postal_Code__c','254136','String'));
        transactionProcessorKV.add(new KeyValue('Other_Country__c','COuntry','String'));
        transactionProcessorKV.add(new KeyValue('Salutation__c','Mr','String'));
        transactionProcessorKV.add(new KeyValue('Phone__c',' 0732587410','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Postal_Code__c','457869','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_City__c',' Transaction-MailingCity','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Street__c',' Transaction-MailingStreet','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Country__c',' Transaction-MailingCountry','String'));
        transactionProcessorKV.add(new KeyValue('Last_Name__c',' Transaction-LastName','String'));
        transactionProcessorKV.add(new KeyValue('First_Name__c',' Transaction-FIrstName','String'));
        transactionProcessorKV.add(new KeyValue('Source__c','Web','String'));
        testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        //System.assertEquals(2, [select count() from Transaction_Processor__c]);
    }
    public static void createTestData2(){
         TestDataComplexFunctions testDataComplexFunctions=new TestDataComplexFunctions();
        Account acc=new Account();
        Contact con=new Contact();
        //Creating account    
        accountKV.add(new KeyValue('FirstName','FirstBus','String'));
        accountKV.add(new KeyValue('LastName','Account','String'));
        accountKV.add(new KeyValue('Email__c','account@test.com','String'));        
        acc=testDataComplexFunctions.insertAccount(accountKV,null);
        //System.assertEquals(2, [select count() from Account]);
        
       //Creating contact record
        contactKV.add(new KeyValue('FirstName','WebFirst Name','String'));
        contactKV.add(new KeyValue('LastName','WebLast Name','String'));
        contactKV.add(new KeyValue('Web_Reference_ID__c','WR1Id','String'));
        contactKV.add(new KeyValue('Email__c','account@test.com','String'));
        contactKV.add(new KeyValue('AccountName',String.valueOf(acc.id),'String'));
        con=testDataComplexFunctions.insertContact(contactKV,null);
                
        //Creating Transaction_Processor__c record
        DateTime currentDateTime = DateTime.now();
        transactionProcessorKV.add(new KeyValue('First_Name__c','FirstName','String'));
        transactionProcessorKV.add(new KeyValue('Last_Name__c','LastName','String'));
        transactionProcessorKV.add(new KeyValue('Amount__c','500.20','DECIMAL'));
        transactionProcessorKV.add(new KeyValue('Payment_Form__c','Cash','String'));
        transactionProcessorKV.add(new KeyValue('PSP_Transaction_ID__c','P-09','String'));
        transactionProcessorKV.add(new KeyValue('Purchase_Date__c',String.valueOf(currentDateTime),'DateTime'));
        transactionProcessorKV.add(new KeyValue('Transaction_Id__c','T-09','String'));
        transactionProcessorKV.add(new KeyValue('Email__c','account@test.com','String'));
        transactionProcessorKV.add(new KeyValue('Web_Reference_ID__c','WebId09','String'));
        transactionProcessorKV.add(new KeyValue('OpCo_Organisation__c','OpOrg','String'));
        transactionProcessorKV.add(new KeyValue('Other_Street__c','Transaction-OtherStreet','String'));
        transactionProcessorKV.add(new KeyValue('Core3_ID__c','C-968537','String'));
        transactionProcessorKV.add(new KeyValue('Core3_Device_Id__c','C-868532','String'));
        transactionProcessorKV.add(new KeyValue('Other_City__c','Transaction-OtherCity','String'));
        transactionProcessorKV.add(new KeyValue('Other_Postal_Code__c','254136','String'));
        transactionProcessorKV.add(new KeyValue('Other_Country__c','COuntry','String'));
        transactionProcessorKV.add(new KeyValue('Salutation__c','Mr','String'));
        transactionProcessorKV.add(new KeyValue('Phone__c',' 0732587410','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Postal_Code__c','457869','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_City__c',' Transaction-MailingCity','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Street__c',' Transaction-MailingStreet','String'));
        transactionProcessorKV.add(new KeyValue('Mailing_Country__c',' Transaction-MailingCountry','String'));
        transactionProcessorKV.add(new KeyValue('Last_Name__c',' Transaction-LastName','String'));
        transactionProcessorKV.add(new KeyValue('First_Name__c',' Transaction-FIrstName','String'));
        transactionProcessorKV.add(new KeyValue('Source__c','Web','String'));
        transactionProcessorKV.add(new KeyValue('Customer__c',String.valueOf(con.id),'String'));
        testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        //System.assertEquals(3, [select count() from Transaction_Processor__c]);
        
        TestDataUpdate testDataUpdate = new TestDataUpdate();
        transactionProcessorKVupdate.add(new KeyValue('Mailing_Country__c','MailingCountry Name','String'));
        testDataUpdate.updateTransactionProcessor(transactionProcessorKV,transactionProcessorKVupdate,null);
        
    }
}