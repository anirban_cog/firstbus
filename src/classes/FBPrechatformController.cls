public class FBPrechatformController{

    public string LiveAgentChatURL{get;private set;}
    public string LiveAgentDeploymentJs{get;private set;}
    public string LiveAgentPreChatJs{get;private set;}

    public FBPrechatformController(){

        try{
            LiveAgentChatURL = LiveAgentSettings__c.getValues('LiveAgentChatURL').value__c;
            LiveAgentDeploymentJs = LiveAgentSettings__c.getValues('LiveAgentDeploymentJs').value__c; 
            LiveAgentPreChatJs = LiveAgentSettings__c.getValues('LiveAgentPreChatJs').value__c;    
        }catch(Exception e){

            system.debug('e--->'+e);
        }
    }

}