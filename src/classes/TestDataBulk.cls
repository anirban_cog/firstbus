public virtual class TestDataBulk extends TestDataInsert{
   
    public List<Contact> cts{get;set;} 
    public Account[] insertAccounts(KeyValueBulk kLst, Debug_Error__c[] errors){ 
        Account[] accs = new Account[]{}; 
            bulkModeOn = true; // prevents the records from being inserted 
        KeyValue[] kVals = kLst.keyValueBulkLst;
        for(Integer i = 0; i < kLst.insertRecs; i++){
            accs.add(super.insertAccount(kVals, errors)); 
        } 
        UtilDML_Master.insertObjects(accs); 
        bulkModeOn = false; 
        return accs;
    } 
    public Account insertAccountAndContacts(Map<System.Type, List<KeyValueBulk>> kMaps, Debug_Error__c[] errors){ 
        // prevents null exception
        if(kMaps == null){ 
            kMaps = new Map<System.Type, List<KeyValueBulk>>();
            kMaps.put(Account.class, new List<KeyValueBulk>());
            kMaps.put(Contact.class, new List<KeyValueBulk>());
        } 
        if (!kMaps.containskey(Account.class))
            kMaps.put(Account.class, new List<KeyValueBulk>()); 
        if (!kMaps.containskey(Contact.class))
            kMaps.put(Contact.class, new List<KeyValueBulk>()); 
        Account acc = new Account();
        this.cts = new List<Contact>(); 
        //acc = super.insertAccount(kMaps.get(Account.class).keyValueBulkLst, errors); 
        bulkModeOn = true; // prevents the records from being inserted 
        // links sObjects
        // kMaps.get(Contact.class).keyValueBulkLst.add(new KeyValue('AccountId', acc.id, 'ID'));
        List<KeyValueBulk> kLst = kMaps.get(Contact.class); 
       /* KeyValue[] kVals = kLst.keyValueBulkLst;
        for(Integer i = 0; i < kLst.insertRecs; i++){ 
            this.cts.add(super.insertContact(kVals, errors));
        } */
        UtilDML_Master.insertObjects(this.cts);
        bulkModeOn = false; 
        return acc; 
    } 
    
}