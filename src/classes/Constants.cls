public with sharing class Constants {
	public static final string CONST_CUSTOM_ACCOUNT = 'CUSTOM_ACCOUNT';
    public static final String TP = 'Transaction_Processor__c';
    public static final String PP = 'Purchases_Processor__c';
    public static final String contact = 'Contact';
    public static final String DeviceID = 'Device_ID__c';
    public static final String IndividualEmailResult = 'et4ae5__IndividualEmailResult__c';
    public static final String MTicket = 'MTicket';
    public static final String Web = 'Web';
    public static final String January = 'January';
    public static final String February = 'February';
    public static final String March = 'March';
    public static final String April = 'April';
    public static final String May = 'May';
    public static final String June = 'June';
    public static final String July = 'July';
    public static final String August = 'August';
    public static final String September = 'September';
    public static final String October = 'October';
    public static final String November = 'November';
    public static final String December = 'December';
	public static final String TransactionManagementChainName = 'Transaction Management Chain';
    public static final Integer TransactionManagementJobInterval = 180;
	public static final String ContactOperationChainName = 'Transaction Deletion Chain';
    public static final Integer ContactOperationJobInterval = 720; 
    public static final String TransactionBatchQuery = 'SELECT Id, Name, Amount__c, Payment_Form__c, PSP_Transaction_ID__c, Purchase_Date__c, Transaction_Id__c, Email__c, Web_Reference_ID__c, '+
        												'Customer__c, OpCo_Organisation__c, Device_ID__c, Other_Street__c, Core3_ID__c, Other_City__c, Other_Postal_Code__c, Other_Country__c, '+
        												'Salutation__c, Phone__c, Mailing_Postal_Code__c, Mailing_City__c, Mailing_Street__c, Mailing_Country__c, Last_Name__c, First_Name__c, Processed__c, Source__c '+
        												'FROM Transaction_Processor__c where Processed__c = false';
    public static final String PurchasesBatchQuery = 'SELECT Id, Name, Ticket_Category__c, Promotional_Code__c, Product_Issued__c, Price__c, Activation_Time__c, Expiry_Time__c, Device_Id__c, '+
            											'Transaction__c, TTL__c, Ticket_Id__c, PurchaseBO__c, Processed__c, Updated__c '+
            											'FROM Purchases_Processor__c where Processed__c = false';
    public static final String DuplicateContactDeletionQuery = 'select id,Duplicate__c from Contact where Duplicate__c=true and id not in(Select customer__c from Transaction_Processor__c)';
    public static final Integer DefaultLimit = 50000;
    public static Map<Integer,String> getMonthMap(){
        Map<Integer,String> monthMap = new Map<Integer,String>();
        monthMap.put(1,Constants.January);
        monthMap.put(2,Constants.February);
        monthMap.put(3,Constants.March);
        monthMap.put(4,Constants.April);
        monthMap.put(5,Constants.May);
        monthMap.put(6,Constants.June);
        monthMap.put(7,Constants.July);
        monthMap.put(8,Constants.August);
        monthMap.put(9,Constants.September);
        monthMap.put(10,Constants.October);
        monthMap.put(11,Constants.November);
        monthMap.put(12,Constants.December);
        
        return monthMap;
    }
}