public class Task_Creation_for_OD {
    
    @InvocableMethod
    public static void newTaskAssignment(List<String> lstCaseIds){
        
        System.debug('---lstCaseIds--'+lstCaseIds);
        Set<String> setofOpCo = new Set<String>();
        
        List<Case> lstCase = [Select Id,Operating_Company__c,ContactId, CreatedDate  From Case where Id IN : lstCaseIds];
        System.debug('--Size of Cases--'+lstCase.size()+'---lstCase--'+lstCase);
        
        String strOperatingCompany = '';
        
        for(Case s : lstCase){
            strOperatingCompany = 'OD '+s.Operating_Company__c;
            setofOpCo.add(strOperatingCompany);
        }
        System.debug('---strOperatingCompany --'+strOperatingCompany);
        System.debug('---setofOpCo --'+setofOpCo);
        
        List<Group> lstGroup = [SELECT Id, Name,(Select Id,UserOrGroupId From GroupMembers )FROM Group WHERE Type = 'Regular' and Name In : setofOpCo];
        System.debug('---lstGroup---'+lstGroup);
        
        List<Task> lstTaskToCreate = new List<Task>();
        for(Group g : lstGroup){
            
            for(GroupMember gp : g.GroupMembers){
                System.debug('---gp---'+gp.UserOrGroupId);
                
                for(Case objCase : lstCase){
                    Task objTask = new Task();
                    objTask.WhatId = objCase.Id;
                    objTask.OwnerId = gp.UserOrGroupId;
                    objTask.Subject = 'CCTV Footage requested';
                    objTask.IsReminderSet = true;
                    objTask.WhoId = objCase.ContactId;
                    objTask.ReminderDateTime = objCase.CreatedDate + 4;
                    objTask.ActivityDate = Date.Today().addDays(4);
                    objTask.Priority = 'Normal';
                    lstTaskToCreate.add(objTask);
                }
                
            }
            
        }
        System.debug('--Size--'+lstTaskToCreate.size());
        System.debug('---lstTaskToCreate---'+lstTaskToCreate);
        if(lstTaskToCreate.size()>0){
            insert lstTaskToCreate; 
        }
        
    }
    
}