global with sharing class batchProcessDMLConsolidation implements Database.Batchable<sObject> { 
global Sobject[] allSobj; 
global String dmlType; 

global batchProcessDMLConsolidation(Sobject[] thisSobj, String thisdmlType){ 
    allSobj = thisSobj; 
    dmlType = thisdmlType; 
} 

global list<Sobject> start(Database.BatchableContext BC) { 
    return allSobj; 
} 

global void execute(Database.BatchableContext BC, list<Sobject> sobj) { 
    Database.saveresult[] saveRes; 
    if (dmlType == 'INSERT') 
        saveRes = Database.insert(sobj,false); 
    else if (dmlType == 'UPDATE') 
        saveRes = Database.update(sobj,false); 
    else if (dmlType == 'DELETE') 
        Database.Deleteresult[] delRes = Database.delete(sobj,false); 

} 

global void finish(Database.BatchableContext BC) { 
} 

}