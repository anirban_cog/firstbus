global class FBContactCleanupBatch implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug('**************Hello1**************');
        String query = '';
        String dateRangeForContactCleanupOperation = ContactCleanupSettings__c.getInstance('DateRangeForContactCleanupOperation').Value__c;
        //String dateRangeForIndividualEmailResult = ContactCleanupSettings__c.getInstance('DateRangeForIndividualEmailResult').Value__c;
        //String dateRangeForContactEmailOpened = ContactCleanupSettings__c.getInstance('DateRangeForContactEmailOpened').Value__c;
        
        Integer contactCreatedDateRange = Integer.valueof(DateRangeForContactCleanupOperation)-1; 
        //Integer contactEmailOpenedDateRange = Integer.valueof(DateRangeForContactEmailOpened)-1; 
                
        Date createdDtRange = DateTime.now().date().addDays(-contactCreatedDateRange);
        //Date emailOpenedDtRange = System.today().addDays(-contactEmailOpenedDateRange);
        system.debug('**************createdDtRange**************'+ createdDtRange);
        //system.debug('**************emailOpenedDtRange**************'+ emailOpenedDtRange);
        
        // Select Contacts(not duplicate and not person account)
        //query ='SELECT CreatedDate, Id, Name, Total_Purchase_Count__c,Total_Purchase_Value__c, (SELECT Id, CreatedDate, ClosedDate, Status from Cases) FROM Contact WHERE IsPersonAccount = false AND Duplicate__c = false AND CreatedDate < :createdDtRange AND Total_Purchase_Count__c <= 0 AND (Total_Purchase_Value__c = null OR Total_Purchase_Value__c <= 0) ORDER BY CreatedDate DESC NULLS FIRST';
        
        //query ='SELECT Id,(SELECT Id, Email_Opened__c CreatedDate, ClosedDate, Status from Cases) FROM Contact WHERE IsPersonAccount = false AND Duplicate__c = false AND CreatedDate <= :createdDtRange AND (Total_Purchase_Count__c = null OR Total_Purchase_Count__c <= 0) AND (Total_Purchase_Value__c = null OR Total_Purchase_Value__c <= 0) AND OwnerId = \'0050E000000p3rUQAQ\' ORDER BY CreatedDate DESC NULLS FIRST';
        
        
        query ='SELECT Id,Email_Opened__c, (SELECT Id, CreatedDate, ClosedDate, Status from Cases) FROM Contact WHERE IsPersonAccount = false AND Duplicate__c = false AND CreatedDate <= :createdDtRange AND (Total_Purchase_Count__c = null OR Total_Purchase_Count__c <= 0) AND (Total_Purchase_Value__c = null OR Total_Purchase_Value__c <= 0) ORDER BY CreatedDate DESC NULLS FIRST';
        
        
        system.debug('**************query**************'+ query);
        return database.getQueryLocator(query);
   }

    global void execute(Database.BatchableContext BC, List<Contact> conatctList){
        system.debug('**************Hello2**************');
        system.debug('**************conatctList**************' + conatctList);
        system.debug('**************conatctList.size()**************' + conatctList.size());
        
        String DateRangeForContactCleanupOperation = ContactCleanupSettings__c.getInstance('DateRangeForContactCleanupOperation').Value__c;
        String DateRangeForContactEmailOpened = ContactCleanupSettings__c.getInstance('DateRangeForContactEmailOpened').Value__c;
        String DateRangeForCaseClosedDate = ContactCleanupSettings__c.getInstance('DateRangeForCaseClosedDate').Value__c;
        
        List<Contact> lstContactsToBeDeleted  = new List<Contact>();
        List<Case> lstCasesToBeDeleted  = new List<Case>();
        List<Case> lstRelatedCases = new List<Case>();
        
        Integer dateRange = Integer.valueof(DateRangeForContactCleanupOperation)-1;     
        Date dt = DateTime.now().date().addDays(-dateRange);
        system.debug('**************dt**************' + dt);
        
        Integer contactEmailOpenedDateRange = Integer.valueof(DateRangeForContactEmailOpened)-1; 
        Date emailOpenedDtRange = DateTime.now().date().addDays(-contactEmailOpenedDateRange);
        system.debug('**************emailOpenedDtRange**************' + emailOpenedDtRange);
        
        Integer CaseClosedDateRange = Integer.valueof(DateRangeForCaseClosedDate)-1; 
        Date caseClosedDtRange = DateTime.now().date().addDays(-CaseClosedDateRange);
        system.debug('**************caseClosedDtRange**************' + caseClosedDtRange);
        
        for(Contact contact : conatctList)
        {
            system.debug('**************contact**************' + contact);
            if(contact != null)
            {               
                lstRelatedCases = contact.Cases;
                system.debug('**************lstRelatedCases**************' + lstRelatedCases);
                if(lstRelatedCases != null && lstRelatedCases.size() > 0 )
                {
                    boolean canDeleteContact = true;
                    for(Case childCase : lstRelatedCases)
                    {
                        system.debug('**************childCase**************' + childCase);
                        system.debug('**************childCase.Status**************' + childCase.Status);
                        system.debug('**************childCase.ClosedDate**************' + childCase.ClosedDate);
                        if(childCase.Status != null &&
                            !(childCase.Status == 'Closed' && childCase.ClosedDate < caseClosedDtRange && childCase.ClosedDate != null))
                        {
                            canDeleteContact = false;
                            break;
                        }
                        system.debug('**************canDeleteContact**************' + canDeleteContact);
                    }
                    system.debug('**************canDeleteContact**************' + canDeleteContact);
                    system.debug('**************contact.Email_Opened__c**************' + contact.Email_Opened__c);
                    system.debug('**************emailOpenedDtRange**************' + emailOpenedDtRange);
                    if(canDeleteContact)
                    {
                        if(contact.Email_Opened__c == null || 
                            (contact.Email_Opened__c != null && contact.Email_Opened__c < emailOpenedDtRange))
                        {
                            lstContactsToBeDeleted.add(contact);
                            lstCasesToBeDeleted.addAll(lstRelatedCases);
                        }
                    }
                }
                else
                {
                    system.debug('**************No related cases**************');
                    system.debug('**************contact.Email_Opened__c**************' + contact.Email_Opened__c);
                    system.debug('**************emailOpenedDtRange**************' + emailOpenedDtRange);
                    if(contact.Email_Opened__c == null || 
                        (contact.Email_Opened__c != null && contact.Email_Opened__c < emailOpenedDtRange))
                    {
                        lstContactsToBeDeleted.add(contact);
                        lstCasesToBeDeleted.addAll(lstRelatedCases);
                    }
                }
            }
        }
        system.debug('**************lstCasesToBeDeleted**************' + lstCasesToBeDeleted); 
        system.debug('**************lstCasesToBeDeleted.size()**************' + lstCasesToBeDeleted.size()); 
        
        system.debug('**************lstContactsToBeDeleted**************' + lstContactsToBeDeleted); 
        system.debug('**************lstContactsToBeDeleted.size()**************' + lstContactsToBeDeleted.size()); 
        
        Database.DeleteResult[] caseResultList = Database.delete(lstCasesToBeDeleted, false);
        handleDeleteResultList(caseResultList);
        Database.DeleteResult[] contactResultList = Database.delete(lstContactsToBeDeleted, false);
        handleDeleteResultList(contactResultList);

        /*String dateRangeForIndividualEmailResult = ContactCleanupSettings__c.getInstance('DateRangeForIndividualEmailResult').Value__c;
        Integer indEmailResultDateRange = Integer.valueof(dateRangeForIndividualEmailResult);         
        Date today = DateTime.now().date();
        system.debug('**************indEmailResultDateRange**************' + indEmailResultDateRange);
        
        List<et4ae5__IndividualEmailResult__c> lstIndEmailResultToBeDeleted = new List<et4ae5__IndividualEmailResult__c>();
        List<et4ae5__IndividualEmailResult__c> lstEmailResults = [SELECT CreatedDate,Id, et4ae5__Opened__c FROM et4ae5__IndividualEmailResult__c WHERE et4ae5__Opened__c = false AND et4ae5__DateOpened__c = null];
        system.debug('**************lstEmailResults**************' + lstEmailResults);
        if(lstEmailResults != null && lstEmailResults.size() > 0)
        {
            for(et4ae5__IndividualEmailResult__c ier : lstEmailResults)
            {
                //system.debug('**************ier**************' + ier);
                //system.debug('**************ier.CreatedDate**************' + ier.CreatedDate);
                DateTime tempDtTime = ier.CreatedDate.addDays(indEmailResultDateRange);
                //system.debug('**************tempDtTime**************' + tempDtTime);
                Date emailResultDtRange = tempDtTime.date();//Date.newinstance(tempDtTime.year(), tempDtTime.month(), tempDtTime.day());
                //system.debug('**************emailResultDtRange**************' + emailResultDtRange);
                if(today > emailResultDtRange)
                {
                    lstIndEmailResultToBeDeleted.add(ier);
                }
            }
        }        
        system.debug('**************lstIndEmailResultToBeDeleted**************' + lstIndEmailResultToBeDeleted);
        system.debug('**************lstIndEmailResultToBeDeleted.size()**************' + lstIndEmailResultToBeDeleted.size());        
        
        Database.DeleteResult[] indEmailResultList = Database.delete(lstIndEmailResultToBeDeleted, false);
        handleDeleteResultList(indEmailResultList);*/
        
    }

    global void finish(Database.BatchableContext BC){
       system.debug('**************Hello3**************');
    }
   
    private void handleDeleteResultList(Database.DeleteResult[] resultList)
    {
        for (Database.DeleteResult result : resultList) 
        {
            if (result.isSuccess()) 
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted record. Record ID: ' + result.getId());
            }
            else 
            {
                // Operation failed, so get all errors                
                for(Database.Error err : result.getErrors()) 
                {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }    
    }
}