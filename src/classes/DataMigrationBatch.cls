/*************************************************************
* Author: Ayan Hore
* Created Date: 01/05/18
* Version1: B-16741
* Details: Batch job for the following:
* 			1. Convert Person Account to Business Account,
* 			2. Decouple Person Account and Contact, 
* 			3. Migrating Person Account information to Contact,
* Purpose: Batch job for the following:
* 			1. Convert Person Account to Business Account,
* 			2. Decouple Person Account and Contact, 
* 			3. Migrating Person Account information to Contact,
**************************************************************/
public class DataMigrationBatch implements Database.Batchable<sObject>{
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'Select RecordTypeId, Id, Age_Range__c, Reason_for_use__c, Source__c, Source_Created_Date__c, Source_Amended_Date__c, '+
                     'University__c, LastName, Like_to_receive_offers__c, PersonEmail, Control_Group__c, FirstName, First_Purchase_Type__c, '+
                     'Salutation, FirstPurchaseDate__c, Organisation_1__c, Organisation_2__c, Organisation_3__c, Organisation_4__c, '+
                     'Organisation_5__c, Frequency_of_use__c, Gender__c, Total_Purchase_Count__c, Like_to_receive_latest_offers__c, '+
                     'Purchase_Count__c, MailingList1__c, MailingList2__c, MailingList3__c, LastPurchaseDate__c, Last_Purchase_Type__c, Last_Purchase_Count__c, '+
                     'Reference_ID__c, Phone, IsPersonAccount, PersonContactId '+ 
                     'from Account '+
                     'where Recordtype.name=\'Person Account\' '+
            		 'LIMIT '+Limits.getLimitQueryRows();
        system.debug(query);
        return Database.getQueryLocator(query);
   }

   public void execute(Database.BatchableContext BC, List<sObject> scope){
       system.debug('SIZE : '+scope.size());
       List<Account> OriginalAccountList = new List<Account>();
       for(sObject s: scope){
           OriginalAccountList.add((Account) s);
       }
       
       //Person Account to Business Account Conversion
       Id recId = [Select id From RecordType where sobjecttype='Account' and name='Business Account'].id;
       List<Account> updateAccList = new List<Account>();
       Set<id> accSet = new Set<id>();
       for (Account ac : OriginalAccountList){
           updateAccList.add(new Account(Id=ac.Id, RecordTypeId=recId));
           accSet.add(ac.Id);
       }
       update updateAccList;
       
       //Decouple Account and Contact
       List<Contact> ct = [Select Accountid from Contact where Accountid != null and Accountid In : accSet limit :Limits.getLimitQueryRows()];
       List<Contact> contactUpdateList = new List<Contact>();
       for(Contact c : ct){
           contactUpdateList.add(new Contact(Id=c.Id, AccountId = null));
       }
       update contactUpdateList;
       
       //Update Contact with Account Information
       DataMigrationLogic.upsertContact(OriginalAccountList);          
   }

   public void finish(Database.BatchableContext BC){
       Integer remainingCount = [Select count() from Account where Recordtype.name='Person Account' LIMIT :Limits.getLimitQueryRows()];
       Integer scheduledJobCount = [SELECT count() FROM CronTrigger where CronJobDetail.Name = 'Data Migration Batch'];
       if(remainingCount>0){
           if(scheduledJobCount == 0)
       			System.scheduleBatch(new DataMigrationBatch(), 'Data Migration Batch' ,5);
       }
       else if(scheduledJobCount>0){
           System.abortJob([SELECT Id FROM CronTrigger where CronJobDetail.Name = 'Data Migration Batch' LIMIT 1].get(0).Id);
       }
   }
    
}