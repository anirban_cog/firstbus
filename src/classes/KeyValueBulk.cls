public class KeyValueBulk{ 
    public integer insertRecs;
    public KeyValue[] keyValueBulkLst; 
    public KeyValueBulk(){ 
        
    } 
    public KeyValueBulk(integer insRecs, KeyValue[] kys){ 
        insertRecs = (insRecs!= null && insRecs> 0) ? insRecs: 1;
        keyValueBulkLst = kys;
    } 
}