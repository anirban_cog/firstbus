public class DataMigrationPatchJob implements Database.Batchable<sObject>{
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id, Source__c, Channels__c, Last_Interaction__c, Original_Source__c, Original_Web_Source__c, Account_External_Id__c FROM Contact '+
            ' where Account_External_Id__c != NULL AND (Original_Source__c = NULL OR Last_Interaction__c = NULL OR Channels__c = NULL)AND Source__c != NULL ' + // AND Source__c != \'Prospect\' 
            'LIMIT '+Limits.getLimitQueryRows();
        system.debug(query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        system.debug('SIZE : '+scope.size());
        List<Contact> OriginalContactList = new List<Contact>();
        List<Id> accountIdList = new List<Id>();
        for(sObject s: scope){
            Contact temp = (Contact) s;
            OriginalContactList.add(temp);
            accountIdList.add(temp.Account_External_Id__c);
        }
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, Source__c FROM Account where Id IN:accountIdList]);
        List<Contact> contactListToBeUpdated = new List<Contact>();
        for(Contact obj : OriginalContactList){
            if(accountMap.containsKey(obj.Account_External_Id__c)){
                Account acc = accountMap.get(obj.Account_External_Id__c);
                String str = obj.Channels__c != NULL ? obj.Channels__c : '';
                obj.Channels__c = !str.containsIgnoreCase(obj.Source__c) ? ((str != '' ? obj.Channels__c+';' : '')+obj.Source__c) : str;
                obj.Last_Interaction__c = obj.Source__c;
                obj.Original_Source__c = obj.Source__c; 
                obj.Original_Web_Source__c = obj.Source__c == 'Web' ? acc.Source__c : null;
                contactListToBeUpdated.add(obj);
            }
        }
        if(!contactListToBeUpdated.isEmpty() && Schema.sObjectType.Contact.isUpdateable())
            update contactListToBeUpdated;
    }
    
    public void finish(Database.BatchableContext BC){
        Integer remainingCount = [Select count() FROM Contact where Account_External_Id__c != NULL AND (Original_Source__c = NULL OR Last_Interaction__c = NULL OR Channels__c = NULL) AND Source__c != NULL LIMIT :Limits.getLimitQueryRows()];// AND Source__c != 'Prospect'
        Integer scheduledJobCount = [SELECT count() FROM CronTrigger where CronJobDetail.Name = 'Data Migration Patch Job'];
        if(remainingCount>0){
            if(scheduledJobCount == 0)
                System.scheduleBatch(new DataMigrationPatchJob(), 'Data Migration Patch Job' ,5);
        }
        else if(scheduledJobCount>0){
            System.abortJob([SELECT Id FROM CronTrigger where CronJobDetail.Name = 'Data Migration Patch Job' LIMIT 1].get(0).Id);
        }
    }
}