/*************************************************************
* Author: Ayan Hore
* Created Date: 12/07/17
* Version1: This is the initial version
* Details: Test class for Purchases_Processor__c related apex components
* Purpose: Code coverage for Purchases_Processor__c related apex components
**************************************************************/
@istest
public class PurchasesProcessorTest {
    //Purchase Processor List of key value pairs to insert
    Public static final List<KeyValue> purchaseProcessorKV = new List<KeyValue>();
    //Contact creation
    Public static final List<KeyValue> contactKV = new List<KeyValue>();
    //Transaction Processor List of Key value pairs to insert
    Public static final List<KeyValue> transactionProcessorKV = new List<KeyValue>();
    //Updated List
    Public static final List<KeyValue> purchaseProcessorKVupdate = new List<KeyValue>();
    
    @isTest
    static void testMethod1(){
        test.startTest();
        purchasesProcessorMethod();
        purchasesProcessorUpdate();
        test.stopTest();
    }
    public static void purchasesProcessorMethod(){
        TestDataComplexFunctions testDataComplexFunctions= new testDataComplexFunctions();
        
        Contact con= new Contact();
        //Creating contact record
        contactKV.add(new KeyValue('FirstName','WebFirst Name','String'));
        contactKV.add(new KeyValue('LastName','WebLast Name','String'));
        //contactKV.add(new KeyValue('MTickets_Reference_ID__c','MT1Id','String'));
        //contactKV.add(new KeyValue('AccountName',String.valueOf(acc.id),'String'));
        contactKV.add(new KeyValue('Email__c','account@test.com','String'));  
        con=testDataComplexFunctions.insertContact(contactKV,null);
        System.assertEquals(1, [Select count() from Contact]);
            
        //Transaction Processor Record creation
        Transaction_Processor__c tp=new Transaction_Processor__c();
        transactionProcessorKV.add(new keyValue('Name','TName','String'));
        transactionProcessorKV.add(new KeyValue('Customer__c',String.valueOf(con.id),'String'));
        tp=testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        //System.assertEquals(1, [select count() from Transaction_Processor__c]);
        
        
        Datetime currentDateTime= Datetime.now();
        purchaseProcessorKV.add(new KeyValue('Ticket_Category__c','Transaction-Name','String'));
        purchaseProcessorKV.add(new KeyValue('Promotional_Code__c','P-09','String'));
        purchaseProcessorKV.add(new KeyValue('Product_Issued__c',String.valueOf(currentDateTime),'DateTime'));
        purchaseProcessorKV.add(new KeyValue('Price__c','120','DECIMAL'));
        purchaseProcessorKV.add(new KeyValue('Activation_Time__c',String.valueOf(currentDateTime),'DateTime'));
        purchaseProcessorKV.add(new KeyValue('Expiry_Time__c',String.valueOf(currentDateTime),'DateTime'));
        purchaseProcessorKV.add(new KeyValue('Device_Id__c','D-01','String'));
        purchaseProcessorKV.add(new KeyValue('TTL__c','1235','INTEGER'));
        purchaseProcessorKV.add(new KeyValue('Ticket_Id__c','P-01','String'));
        purchaseProcessorKV.add(new KeyValue('PurchaseBO__c','Transaction-Name','String'));
        purchaseProcessorKV.add(new KeyValue('Core3_Transaction_Id__c','C3Id','String'));
        purchaseProcessorKV.add(new KeyValue('Web_Transaction_ID__c','W13Id','String'));
        purchaseProcessorKV.add(new KeyValue('Shared_Ticket_ID__c','Sh1ID','String'));
        //purchaseProcessorKV.add(new KeyValue('Transaction__c',String.valueOf(tp.id),'String'));
        
        
        Map<System.Type, List<KeyValue>> ProcessAndTransactionProcessorMap = new Map<System.Type, List<KeyValue>>();
        ProcessAndTransactionProcessorMap.put(Purchases_Processor__c.class,purchaseProcessorKV);
        ProcessAndTransactionProcessorMap.put(Transaction_Processor__c.class,transactionProcessorKV);
        testDataComplexFunctions.insertTransactionProcessorAndPurchaseProcessor(ProcessAndTransactionProcessorMap,null);
        System.assertEquals(1,[select count() from Purchases_Processor__c]);
    }
    public static void purchasesProcessorUpdate(){
        
        TestDataUpdate testDataUpdate = new TestDataUpdate();
        
        purchaseProcessorKVupdate.add(new KeyValue('Ticket_Category__c','Transaction-Name','String'));
        testDataUpdate.updatePurchasesProcessor(purchaseProcessorKV,purchaseProcessorKVupdate,null);
    }
    
}