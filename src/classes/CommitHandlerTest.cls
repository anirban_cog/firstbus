/*************************************************************
* Author: Steve Fouracre
* Created Date: 05/01/18
* Version1: B-16777
* Details: Test class for CommitHandler
**************************************************************/
@istest
public class CommitHandlerTest {
static TestDataComplexFunctions tdc = new TestDataComplexFunctions();
public static Account acc1;
public static Account acc2;
 
static void setupData1() {
    List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
    CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
    insert CSObjList;

	acc1 = tdc.insertAccount(null,null);
	system.debug('## acc1 ' + acc1);
}

static void setupData2() {
	List<KeyValue> ky = new List<KeyValue>();
	ky.add(new KeyValue('Phone','07774368688','String'));
	acc2 = tdc.insertAccount(ky,null);
	system.debug('## acc2 ' + acc2);
}

private static testmethod void InsertAndUpdateTest(){
	setupData1();
	system.assertequals([Select id From Account].size() , 1);
	Test.startTest();
		CommitHandler ch = new CommitHandler();
		Account[] cs = new Account[]{acc1};
		ch.addToCommit(cs, 'UPDATE');
		
		ch.mergedCommitToDataBase();
		system.assertequals([Select id From Account].size() , 1);
	Test.stopTest();
}

private static testmethod void InsertAndUpdateTwiceTest(){
	setupData1();
	setupData2();
	Test.startTest();
		system.assertequals([Select id From Account].size() , 2);
		CommitHandler ch = new CommitHandler();
		system.debug('## acc1 ' + acc1);
		acc1.Phone = '07774563689';
		
		Account[] cs = new Account[]{acc1,acc2};
		Account updAgain = [Select id,Phone From Account where id=:acc1.id];
		updAgain.Phone = '07774563681';
		cs.add(updAgain);
		
		ch.addToCommit(cs, 'UPDATE');
		
		ch.mergedCommitToDataBase();
		system.assertequals([Select id From Account].size() , 2);
	Test.stopTest();
}

private static testmethod void InsertAndUpdateAndDeleteAllTest(){
	setupData1();
	system.assertequals([Select id From Account].size() , 1);
	Test.startTest();
		CommitHandler ch = new CommitHandler();
		system.debug('## acc1 ' + acc1);
		acc1.Phone = '07774563689';
		Account[] cs = new Account[]{acc1};
		ch.addToCommit(cs, 'UPDATE');
		ch.addToCommit(cs, 'DELETE');
		
		ch.mergedCommitToDataBase();
		system.assertequals([Select id From Account].size() , 0);
	Test.stopTest();
}


}