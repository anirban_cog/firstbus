/*************************************************************
* Author: Prashanth Dasari
* Created Date: 12/22/17
* Version1: B-16912
* Details: Logic class for ContactOperations batch job 
* Purpose: Logic class for ContactOperations batch job
**************************************************************/
public without sharing class ContactOperationsLogic{
    DateTime currentTime = DateTime.now();
    public List<Purchases_Processor__c> generateListOfPurchasesToDelete(){
        List<Purchases_Processor__c> deleteList = new List<Purchases_Processor__c>();
        Map<Id,List<Purchases_Processor__c>> transactionToPurchaseMap = new Map<Id,List<Purchases_Processor__c>>();
        for(Purchases_Processor__c obj : [Select id,Expiry_Time__c,Transaction__c from Purchases_Processor__c 
                                          where Processed__c=true AND Expiry_Time__c < :currentTime order by Expiry_Time__c desc LIMIT :Limits.getLimitQueryRows()]){
            if(transactionToPurchaseMap.containsKey(obj.Transaction__c)){
                if(transactionToPurchaseMap.get(obj.Transaction__c).size()<5)
                    transactionToPurchaseMap.get(obj.Transaction__c).add(obj);
                else
                    deleteList.add(obj);
            }
            else
                transactionToPurchaseMap.put(obj.Transaction__c,new List<Purchases_Processor__c>{obj});
        }
        return deleteList;
    }
    
    public Map<String,List<SObject>> generateListOfTransactionsToDelete(){
        Map<String,List<SObject>> deleteMap = new Map<String,List<SObject>>();
        deleteMap.put('Transaction', new List<SObject>());
        deleteMap.put('Purchase', new List<SObject>());
        Map<Id,Transaction_Processor__c> transMap = new Map<Id,Transaction_Processor__c>();
        Map<Id,List<Transaction_Processor__c>> contactToTransactionMap = new Map<Id,List<Transaction_Processor__c>>();
        Map<Id,List<Purchases_Processor__c>> transactionToPurchaseMap = new Map<Id,List<Purchases_Processor__c>>();
        for(Transaction_Processor__c obj : [SELECT Id, Customer__c FROM Transaction_Processor__c where Processed__c=true order by Purchase_Date__c desc LIMIT :Limits.getLimitQueryRows()]){
            if(obj.Customer__c == NULL)
                transMap.put(obj.Id,obj);
            else{
                if(contactToTransactionMap.containsKey(obj.Customer__c)){
                    if(contactToTransactionMap.get(obj.Customer__c).size()<5)
                        contactToTransactionMap.get(obj.Customer__c).add(obj);
                    else{
                        transMap.put(obj.Id,obj);
                    }
                }
                else
                    contactToTransactionMap.put(obj.Customer__c,new List<Transaction_Processor__c>{obj});
            }
        }
        
        if(!transMap.isEmpty()){
            for(Purchases_Processor__c obj : [Select id,Expiry_Time__c,Transaction__c,Processed__c from Purchases_Processor__c where Transaction__c IN :transMap.keySet()]){
                if(transactionToPurchaseMap.containsKey(obj.Transaction__c))
                    transactionToPurchaseMap.get(obj.Transaction__c).add(obj);
                else
                    transactionToPurchaseMap.put(obj.Transaction__c, new List<Purchases_Processor__c>{obj});
            }
            
            for(Id transId : transMap.keySet()){
                Boolean flag = true;
                if(transactionToPurchaseMap.containsKey(transId)){
                    for(Purchases_Processor__c pp : transactionToPurchaseMap.get(transId)){
                        flag = pp.Processed__c == false ? false : (pp.Expiry_Time__c > currentTime ? false : flag);
                    }
                }
                if(flag){
                    deleteMap.get('Transaction').add((SObject) transMap.get(transId));
                    if(transactionToPurchaseMap.containsKey(transId))
                        for(Purchases_Processor__c pp : transactionToPurchaseMap.get(transId)){
                        	deleteMap.get('Purchase').add((SObject) pp);
                        }
                }
            }
        }
        return deleteMap;
    }
}