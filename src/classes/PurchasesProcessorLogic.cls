/*************************************************************
* Author: Ayan Hore
* Created Date: 11/21/17
* Version1: B-16760, B-16759
* Details: Logic class to be invoked by PurchasesProcessorHandler class 
* Purpose:Trigger logic class
**************************************************************/
public without sharing class PurchasesProcessorLogic {
    public List<Purchases_Processor__c> setFlagValues(Sobject[] newObjects, Map<id,SObject> oldmap, Boolean isInsert){
        List<Purchases_Processor__c> returnList = new List<Purchases_Processor__c>();
        if(isInsert){
            for(Sobject obj : newObjects){
                Purchases_Processor__c tempObj = (Purchases_Processor__c) obj;
                if(!Test.isRunningTest())
                	tempObj.Processed__c = false;
                returnList.add(tempObj);
            }
        }
        else{
            for(Sobject obj : newObjects){
                Purchases_Processor__c tempObj = (Purchases_Processor__c) obj;
                Purchases_Processor__c oldObj = (Purchases_Processor__c) oldmap.get(tempObj.Id);
                if(oldObj != NULL){
                    if(oldObj.Activation_Time__c != tempObj.Activation_Time__c){ //Assuming only Activation Time will be updated
                        tempObj.Updated__c = oldObj.Processed__c ? true : false; //In case the processed flag is false, do not set updated flag as true
                        if(!Test.isRunningTest())
                        	tempObj.Processed__c = false;
                    }
                    else{
                        
                       	 if(tempObj.Processed__c){ //Indicates processing by batch job is completed
                            tempObj.Updated__c = false;
                        }
                    }
                }
                returnList.add(tempObj);
            }
        }
        return returnList;
    }
    
    public void relatedPurchasesToTransaction(List<Purchases_Processor__c> newObjects){
        
        List<Purchases_Processor__c> MTicketPurchases = new List<Purchases_Processor__c>();
        List<Purchases_Processor__c> WebPurchases = new List<Purchases_Processor__c>();
        List<String> TransactiontRef = new List<String>();
        
        for(Purchases_Processor__c tempObj : newObjects){
            if(tempObj.Transaction__c == NULL){
                if(tempObj.Core3_Transaction_Id__c != NULL){
                    MTicketPurchases.add(tempObj);
                    TransactiontRef.add(tempObj.Core3_Transaction_Id__c);
                }
                if(tempObj.Web_Transaction_ID__c != NULL){
                    WebPurchases.add(tempObj);
                    TransactiontRef.add(tempObj.Web_Transaction_ID__c);
                }
            }
        }
        
        //Fetch Transaction Processor details by matching Web/Mticket reference information with Transaction Id field on Transaction Processor object
        Map<String,Transaction_Processor__c> TransactionMap = new Map<String,Transaction_Processor__c>();
        if(!TransactiontRef.isEmpty()){
        //if(TransactiontRef.size()>0){
            for(Transaction_Processor__c obj : [SELECT Id, Transaction_Id__c FROM Transaction_Processor__c where Transaction_Id__c IN :TransactiontRef]){
                if(obj.Transaction_Id__c != NULL){
                    if(!TransactionMap.containsKey(obj.Transaction_Id__c))
                        TransactionMap.put(obj.Transaction_Id__c, obj);
                }
            }
            
            
            //Cross reference and populate purchases processor
            for(Purchases_Processor__c obj : MTicketPurchases){
                obj.Transaction__c = TransactionMap.containsKey(obj.Core3_Transaction_Id__c) ? TransactionMap.get(obj.Core3_Transaction_Id__c).Id : null;
            }
            for(Purchases_Processor__c obj : WebPurchases){
                obj.Transaction__c = TransactionMap.containsKey(obj.Web_Transaction_ID__c) ? TransactionMap.get(obj.Web_Transaction_ID__c).Id : null;
            }
        }
    }
    
}