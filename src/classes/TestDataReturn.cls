public abstract class TestDataReturn  implements ITestData{ 
    public Map<System.Type, String> overrideJson = new Map<System.Type, String>(); 
    public Boolean bulkModeOn = false; 
    public List<sObject> prepareSObject(String jsonStr, KeyValue[] kVals){ 
        List<sObject> sObj = new List<sObject>();
            // if it is a list
            if(jsonStr.startsWith('[')){
                sObj=(List<sObject>) System.Json.deserialize(jsonStr, List<sObject>.class);
            }
        else {
            sObj=(List<sObject>) System.Json.deserialize('['+jsonStr+']', List<sObject>.class);
        } 
        
        if(kVals != null){
            // For multi object 
            for(sObject so : sObj){
                so = UtilityObjectData.setObjData(so, kVals);
            }
        } 
        return sObj;
    } 
}