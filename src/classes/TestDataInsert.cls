public virtual class TestDataInsert extends TestDataReturn{
    public sObject insertAnyObject(String jsonStr, list<KeyValue> kVals, Debug_Error__c[] errors, System.Type objType){
        // checks if override JSON is defined for this objType
        if(overrideJson != null && overrideJson.containsKey(objType)){
            jsonStr = overrideJson.get(objType);
        }
        
        sObject so = super.prepareSObject(jsonStr, kVals)[0];
        
        // bulkMode
        if(bulkModeOn == false){
            //allow option of saving errors this can be useful in many situations such as when testing addError in triggers 
            UtilDML_Master.insertObjects(new sObject[] {so},false,0,errors,true, 'NONE'); 
        } 
        
        return so;
    }
    
    public Account insertAccount(list<KeyValue> kVals, Debug_Error__c[] errors){
        return (Account) insertAnyObject(new TestDataJsonLibrary.Standard().jsonMap.get('ACCOUNT'), kVals, errors, Account.class);
    }
    public Contact insertContact(list<KeyValue> kVals, Debug_Error__c[] errors){
        return (Contact) insertAnyObject(new TestDataJsonLibrary.Standard().jsonMap.get('CONTACT'), kVals, errors, Contact.class);
    }
    
    /* this uses the any of Account json string in Custom() class passed into the function such as Constants.CONST_CUSTOM_ACCOUNT */
    public Account insertAccount(String jsonKey, list<KeyValue> kVals, Debug_Error__c[] errors){
        if (new TestDataJsonLibrary.Custom().jsonMap.containskey(jsonKey))
            return (Account) insertAnyObject(new TestDataJsonLibrary.Custom().jsonMap.get(jsonKey), kVals, errors, Account.class);
        else 
            return null;
    }
    
    
    //Method to insert the TP record
    public Transaction_Processor__c insertTransactionProcessor(List<KeyValue> kVals, Debug_Error__c[] errors){
        if (new TestDataJsonLibrary.Custom().jsonMap.containskey(Constants.TP))
            return (Transaction_Processor__c) insertAnyObject(new TestDataJsonLibrary.Custom().jsonMap.get('Transaction_Processor__c'), kVals, errors, Transaction_Processor__c.class);
        else 
            return null;
    }
    //Method to insert the PP record
    public  Purchases_Processor__c insertPurchasesProcessor(List<KeyValue> kVals, Debug_Error__c[] errors){
        if (new TestDataJsonLibrary.Custom().jsonMap.containskey(Constants.PP))
            return (Purchases_Processor__c) insertAnyObject(new TestDataJsonLibrary.Custom().jsonMap.get('Purchases_Processor__c'), kVals, errors, Purchases_Processor__c.class);
        else 
            return null;
    }
    
    //To insert the DeviceId record
    public Device_ID__c insertDeviceId(List<KeyValue> kVals, Debug_Error__c[] errors){
        if (new TestDataJsonLibrary.Custom().jsonMap.containskey(Constants.DeviceID))
            return (Device_ID__c) insertAnyObject(new TestDataJsonLibrary.Custom().jsonMap.get('Device_ID__c'), kVals, errors, Device_ID__c.class);
        else 
            return null;
    }
    
    //Method to insert the IndividualEmailResult__c object records
    public  et4ae5__IndividualEmailResult__c insertIndividualEmailResult(List<KeyValue> kVals, Debug_Error__c[] errors){
        if (new TestDataJsonLibrary.Custom().jsonMap.containskey(Constants.IndividualEmailResult))
            return (et4ae5__IndividualEmailResult__c) insertAnyObject(new TestDataJsonLibrary.Custom().jsonMap.get('et4ae5__IndividualEmailResult__c'), kVals, errors, et4ae5__IndividualEmailResult__c.class);
        else 
            return null;
    }
    
    //Method to insert the Case record
    public Case insertCase(List<KeyValue> kVals,Debug_Error__c[] errors){
        if(new TestDataJsonLibrary.Standard().jsonMap.containsKey('CASE'))
            return (Case) insertAnyObject(new TestDataJsonLibrary.Standard().jsonMap.get('CASE'),kVals,errors,Case.class);
        else 
            return null;
    }
    
    public Monitoring__c[] insertMonitoring(list<KeyValue> kVals){ 
        sObject[] sobj = super.prepareSObject(new TestDataJsonLibrary.StandardCustSettings().custJsonMap.get('MONITORING__C'), kVals);
        system.debug('##sobj ' + sobj);
        insert sobj;
        
        
        return sobj;
    }
    
    public Triggers_Off__c insertTriggersOff(KeyValue[] kVals, Debug_Error__c[] errors){             
        return (Triggers_Off__c) insertAnyObject(new TestDataJsonLibrary.StandardCustSettings().custJsonMap.get('TRIGGERS_OFF__C'), kVals, errors, Triggers_Off__c.class);
    }
    
    public Trigger_Per_Object__c insertTriggersPerObject(KeyValue[] kVals, Debug_Error__c[] errors){             
        return (Trigger_Per_Object__c) insertAnyObject(new TestDataJsonLibrary.StandardCustSettings().custJsonMap.get('TRIGGER_PER_OBJECT__C'), kVals, errors, Trigger_Per_Object__c.class);
    }
    
    
}