/*************************************************************
* Author: Steve Fouracre
* Created Date: 12/07/17
* Version1: 
* Details: Handler to be invoked by CaseTrigger on Case object 
* Purpose:Trigger logic on Case object
**************************************************************/
public with sharing class CaseLogic {
    Public Static Boolean isUpdatedRecurssion = false;
    Public Static Boolean isInsertedRecurssion = false;
    
    public void closeParentCase(Map<Id,SObject> newMap){
        
        Set<Id> setParentCaseId = new Set<Id>();
        Map<Id,List<Case>> mapParentCaseToChildCases = new Map<Id,List<Case>>();
        List<Case> lstToBeClosedParentCases = new List<Case>();
        List<Case> lstWebCase = new List<Case>();
        
        for(SObject objsobject : newMap.values()){
            Case objCase = (Case)objsobject;
            if( objCase.ParentId != null && objCase.Status == 'Closed')
                setParentCaseId.add(objCase.ParentId);
        }
        
        
        List<Case> lstParentCase = [SELECT Id, Status FROM Case WHERE Id IN :setParentCaseId];
        
        List<Case> lstChildCases = [SELECT Id, Status, ParentId FROM Case WHERE ParentId IN :setParentCaseId];
        
        
        for(Case objCase : lstChildCases){
            
            if(mapParentCaseToChildCases.containsKey(objCase.ParentId))
                mapParentCaseToChildCases.get(objCase.ParentId).add(objCase);
            else
                mapParentCaseToChildCases.put(objCase.ParentId, new List<Case>{objCase});
        }
        
        
        for(Case objCase : lstParentCase){
            Boolean isAllChildClosed = true;
            if(mapParentCaseToChildCases.containsKey(objCase.Id)){
                
                for(Case objChildCase : mapParentCaseToChildCases.get(objCase.Id)){
                    if(objChildCase.Status != 'Closed'){
                        isAllChildClosed = false;
                        break;
                    }
                }
                if(isAllChildClosed)
                    objCase.Status = 'Closed'; 
            }
            lstToBeClosedParentCases.add(objCase);
        }
        
        if (Schema.sObjectType.Case.fields.Status.isUpdateable())
            update lstToBeClosedParentCases;
        
        
            
    }
    
    public void ContactLinking(List<SObject> newList){
        
        List<Contact> lstContact = new List<Contact>();        
        List<Case> lstWebCase = new List<Case>();
        
        for(Sobject objSobject : newList){
            
            Case objCase = (Case)objSobject;
            if(objCase.Source__c == 'Web')
                lstWebCase.add(objCase);
        }
        lstContact = ContactLinkingOrCreationForWebCase.getExistingOrNewContact(lstWebCase);
        
        
        
        for(Sobject objSobject : lstWebCase){
            Case objCase = (Case)objSobject;
            objCase.ContactId = lstContact.isEmpty() ? null : lstContact[0].Id;
            //lstUpdatedCase.add(objCase);
        }
        System.debug('-----newList------'+newList);
    }

    public void updateContactOrganisation(List<Case> newList){
        Map<String,String> caseToContactOrgMap = new Map<String,String>();
        Case_To_Contact_Organisation_Map__mdt[] caseToContactOrg = [Select MasterLabel, Contact_Organisation__c From Case_To_Contact_Organisation_Map__mdt];
        for(Case_To_Contact_Organisation_Map__mdt val : caseToContactOrg){
            caseToContactOrgMap.put(val.MasterLabel, val.Contact_Organisation__c);
        }
        System.debug('----------caseToContactOrgMap-------'+caseToContactOrgMap);   
        
        set<id> contSet = new set<id>();
        for(Case objCase : newList)
            contSet.add(objCase.contactID);
         
        Map<id,Contact> contactMap = new Map<id,Contact>();
        contactMap.putall([Select id,Organisation_1__c From Contact where Id In :contSet]);
        
        for(Case record : newList){
            System.debug('----------record-------'+record); 
            if (record.Operating_Company__c != null && caseToContactOrgMap.containskey(record.Operating_Company__c)){
                Contact ct = contactMap.get(record.contactID);
                ct.Organisation_1__c = caseToContactOrgMap.get(record.Operating_Company__c);
            }
                            
        }
        System.debug('----------contactMap.values()-------'+contactMap.values());
        UtilDML_Master.updateObjects((Sobject[])contactMap.values());
    }
    
    public void parentContactLinking(List<SObject> newList)//Paramita | B-16964 
    {
        //Start |Paramita | B-16964 
        List<Contact> lstParentContact = new List<Contact>();
        List<Case> lstAllCase = new List<Case>();
        for(Sobject objSobject : newList)
            lstAllCase.add((Case)objSobject);
        lstParentContact = ContactLinkingOrCreationForWebCase.updateParentContactSourceRelatedFields(lstAllCase);
        //End |Paramita | B-16964 
    }
}