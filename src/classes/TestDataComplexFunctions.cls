public virtual class TestDataComplexFunctions extends TestDataInsert{ 
    
    public Contact ct; 
    public Account insertAccountAndContact(Map<System.Type, List<KeyValue>> kMaps, Debug_Error__c[] errors){ 
        // prevents null exception
        if(kMaps == null){ 
            kMaps = new Map<System.Type, List<KeyValue>>();
            kMaps.put(Account.class, new List<KeyValue>());
            kMaps.put(Contact.class, new List<KeyValue>());
        }
        else{
            if (!kMaps.containskey(Account.class))
                kMaps.put(Account.class, new List<KeyValue>()); 
            if (!kMaps.containskey(Contact.class))
                kMaps.put(Contact.class, new List<KeyValue>());
        } 
        Account acc = super.insertAccount(kMaps.get(Account.class), errors); 
        // links sObjects
        kMaps.get(Contact.class).add(new KeyValue('AccountId', acc.id, 'ID')); 
        ct = super.insertContact(kMaps.get(Contact.class), errors); 
        return acc;
    } 
    
   
    public Transaction_Processor__c insertContactAndTransactionProcessor(Map<System.Type,List<KeyValue>> kMaps, Debug_Error__c[] errors){
        Contact contactObject = null;
        Transaction_Processor__c transactionProcessorObject = null;
        if(kMaps != null){
            if(kMaps.containsKey(Contact.class) == false || kMaps.containsKey(Transaction_Processor__c.class) == false)
                System.debug('Contact or Transaction Processor record details are not in the map to insert!' + 'transaction data : ' + kMaps.get(Transaction_Processor__c.class) + 'Contact data :'+ kMaps.get(Contact.class));
            else{
                contactObject = super.insertContact(kMaps.get(Contact.class),errors);
                kMaps.get(Transaction_Processor__c.class).add(new KeyValue('Customer__c',contactObject.id,'ID'));
                transactionProcessorObject = super.insertTransactionProcessor(kMaps.get(Transaction_Processor__c.class),errors);
            }
        }
        return transactionProcessorObject;
    }
    
    //insert contact and DeviceID records
    public Device_ID__c insertContactAndDeviceId(Map<System.Type,List<KeyValue>> kMaps, Debug_Error__c[] errors){
        Contact contactObject = null;
        Device_ID__c deviceIdObject = null;
        if(kMaps != null){
            if(kMaps.containsKey(Contact.class) == false || kMaps.containsKey(Device_ID__c.class) == false)
                System.debug('Contact or DeviceId record details are not in the map to insert!' + 'DeviceID data : ' + kMaps.get(Device_ID__c.class) + 'Contact data :'+ kMaps.get(Contact.class));
            else{
                contactObject = super.insertContact(kMaps.get(Contact.class),errors);
                //kMaps.get(Device_ID__c.class).add(new KeyValue('Customer__c',contactObject.id,'ID'));
                deviceIdObject = super.insertDeviceId(kMaps.get(Device_ID__c.class),errors);
            }
        }
        return deviceIdObject;
    }
    
    
    public Purchases_Processor__c insertTransactionProcessorAndPurchaseProcessor(Map<System.Type,List<KeyValue>> kMaps, Debug_Error__c[] errors){
        Transaction_Processor__c transactionProcessorObject = null;
        Purchases_Processor__c purchaseProcessorObject = null;
        if(kMaps != null){
            if(kMaps.containsKey(Transaction_Processor__c.class) == false || kMaps.containsKey(Purchases_Processor__c.class) == false)
                System.debug('Purchases Processor or Transaction Processor record details are not in the map to insert!'+ 'transaction data : ' + kMaps.get(Transaction_Processor__c.class) + 'Purchase data :'+ kMaps.get(Purchases_Processor__c.class));
            else{
                transactionProcessorObject = super.insertTransactionProcessor(kMaps.get(Transaction_Processor__c.class),errors);
                //kMaps.get(Purchases_Processor__c.class).add(new KeyValue('Transaction__c',transactionProcessorObject.id,'ID'));
                PurchaseProcessorObject = super.insertPurchasesProcessor(kMaps.get(Purchases_Processor__c.class),errors);
            }
        }
        return purchaseProcessorObject;
    }
    
 
    public void insertContactAndTransactionProcessorAndPurchaseProcessor(Map<System.Type,List<KeyValue>> kMaps, Debug_Error__c[] errors){
        Transaction_Processor__c transactionProcessorObject = null;
        Purchases_Processor__c purchaseProcessorObject = null;
        if(kMaps != null){
            if( kMaps.containsKey(Contact.class) == false || kMaps.containsKey(Transaction_Processor__c.class) == false || kMaps.containsKey(Purchases_Processor__c.class) == false)
                System.debug('Contact or Purchases Processor or Transaction Processor record details are not in the map to insert!'+'Conatct data : ' + kMaps.get(Contact.class) + 'transaction data : ' + kMaps.get(Transaction_Processor__c.class) + 'Purchase data :'+ kMaps.get(Purchases_Processor__c.class));
            else{
                transactionProcessorObject = insertContactAndTransactionProcessor(kMaps, errors);
                kMaps.get(Purchases_Processor__c.class).add(new KeyValue('Transaction__c',transactionProcessorObject.id,'ID'));
                PurchaseProcessorObject = super.insertPurchasesProcessor(kMaps.get(Purchases_Processor__c.class),errors);
            }
        }

    }
   /* public void insertAccountAndinsertContactAndTransactionProcessor(Map<System.Type,List<KeyValue>> kMaps, Debug_Error__c[] errors){
        Contact accountAndContact=null;
        Transaction_Processor__c transactionProcessorObject = null;
        if(kMaps != null){
            if(kMaps.containsKey(Account.class) == false || kMaps.containsKey(Contact.class) == false || kMaps.containsKey(Transaction_Processor__c.class) == false)
                System.debug('Contact or Purchases Processor or Transaction Processor record details are not in the map to insert!'+'Account data : ' + kMaps.get(Account.class)  +'Conatct data : ' + kMaps.get(Contact.class) + 'transaction data : ' + kMaps.get(Transaction_Processor__c.class));
            else{
                accountAndContact = iinsertAccountAndContact(kMaps, errors);
                kMaps.get(Transaction_Processor__c.class).add(new KeyValue('Customer__c',accountAndContact.id,'ID'));
                transactionProcessorObject = super.insertTransactionProcessor(kMaps.get(Transaction_Processor__c.class),errors);
        	}
        }

    }
   
    public Contact iinsertAccountAndContact(Map<System.Type, List<KeyValue>> kMaps, Debug_Error__c[] errors){ 
        // prevents null exception
        if(kMaps == null){ 
            kMaps = new Map<System.Type, List<KeyValue>>();
            kMaps.put(Account.class, new List<KeyValue>());
            kMaps.put(Contact.class, new List<KeyValue>());
        }
        else{
            if (!kMaps.containskey(Account.class))
                kMaps.put(Account.class, new List<KeyValue>()); 
            if (!kMaps.containskey(Contact.class))
                kMaps.put(Contact.class, new List<KeyValue>());
        } 
        Account acc = super.insertAccount(kMaps.get(Account.class), errors); 
        // links sObjects
        kMaps.get(Contact.class).add(new KeyValue('AccountId', acc.id, 'ID')); 
        ct = super.insertContact(kMaps.get(Contact.class), errors); 
        return ct;
    }
    */
    
   
}