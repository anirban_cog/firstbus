/*************************************************************
* Author: Ayan Hore
* Created Date: 12/07/17
* Version1: B-16878
* Details: Logic class for Device_ID__c trigger
* Purpose:Hosts logic methods
**************************************************************/
public without sharing class DeviceIDLogic {
    
    public Map<String,Id> getContactReference(List<Device_ID__c> objectList){
        Map<String,Id> deviceToContactMap = new Map<String,Id>();
        List<String> C3IdList = new List<String>();
        for(Device_ID__c obj : objectList){
            C3IdList.add(obj.C3_User_Id__c);
        }
        for(Contact obj : [select Id, MTickets_Reference_ID__c, Duplicate__c from Contact where MTickets_Reference_ID__c IN :C3IdList]){
            if(!deviceToContactMap.containsKey(obj.MTickets_Reference_ID__c))
                deviceToContactMap.put(obj.MTickets_Reference_ID__c, obj.Id);
            else{
                if(!obj.Duplicate__c)
                    deviceToContactMap.put(obj.MTickets_Reference_ID__c, obj.Id);
            }
        }
        return deviceToContactMap;
    }
    
    public void checkAndConnectContact(SObject[] newObj){
        try{
            Map<String,Device_ID__c> objectMap = new Map<String,Device_ID__c>();
            for(SObject obj : newObj){
                Device_ID__c tempObj = (Device_ID__c) obj;
                if(tempObj.Customer__c == NULL)
                    objectMap.put(tempObj.C3_User_Id__c, tempObj);
            }
            if(!objectMap.isEmpty()){
                Map<String,Id> contactReferenceMap = new Map<String,Id>();
                contactReferenceMap = getContactReference(objectMap.values());
                for(String deviceId : contactReferenceMap.keySet()){
                    Device_ID__c tempObj = objectMap.get(deviceId);
                    tempObj.Customer__c = contactReferenceMap.get(deviceId);
                }
            }
        }
        catch(Exception e){
            system.debug('EXCEPTION : '+e.getMessage()+' | STACKTRACE : '+e.getStackTraceString());
        }
    }
}