public with sharing class TestDataUpdate extends TestDataInsert{ 
    
    public Account updateAccount(list<KeyValue> inskVals, list<KeyValue> updkVals, Debug_Error__c[] errors){
        Account acc = super.insertAccount(inskVals, errors); 
        if(updkVals != null)
            acc = (Account) UtilityObjectData.setObjData(acc, updkVals); 
        UtilDML_Master.updateObjects(new sObject[]{acc},false,0,errors,true, 'NONE'); 
        return acc;
    }
    
    //DeviceID update
    public Device_ID__c updateDeviceID(list<KeyValue> inskVals, list<KeyValue> updkVals, Debug_Error__c[] errors){
        Device_ID__c device = super.insertDeviceId(inskVals, errors); 
        if(updkVals != null)
            device = (Device_ID__c) UtilityObjectData.setObjData(device, updkVals); 
        UtilDML_Master.updateObjects(new sObject[]{device},false,0,errors,true, 'NONE'); 
        return device;
    }
     public Purchases_Processor__c updatePurchasesProcessor(list<KeyValue> inskVals, list<KeyValue> updkVals, Debug_Error__c[] errors){
        Purchases_Processor__c purchase = super.insertPurchasesProcessor(inskVals, errors); 
        if(updkVals != null)
            purchase = (Purchases_Processor__c) UtilityObjectData.setObjData(purchase, updkVals); 
        UtilDML_Master.updateObjects(new sObject[]{purchase},false,0,errors,true, 'NONE'); 
        return purchase;
    }
   public Transaction_Processor__c updateTransactionProcessor(list<KeyValue> inskVals, list<KeyValue> updkVals, Debug_Error__c[] errors){
        Transaction_Processor__c transactions = super.insertTransactionProcessor(inskVals, errors); 
        if(updkVals != null)
            transactions = (Transaction_Processor__c) UtilityObjectData.setObjData(transactions, updkVals); 
        UtilDML_Master.updateObjects(new sObject[]{transactions},false,0,errors,true, 'NONE'); 
        return transactions;
    }
}