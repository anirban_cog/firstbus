/*************************************************************
* Author: Steve Fouracre
* Created Date: 12/07/17
* Version1: 
* Details: Handler to be invoked by CaseTrigger on Case object 
* Purpose:Trigger handler on Case object
**************************************************************/
public with sharing class CaseHandler extends TriggerBaseHandler{
    
    public CaseLogic csLogic;
    
    public CaseHandler(){
        csLogic = new CaseLogic();
    }
    
    public override void beforeInsert(SObject[] newObj, CommitHandler ch){
        System.debug('==after Insert');
        csLogic.ContactLinking(newObj);
    }
    
    public override void afterInsert(SObject[] newObj, Map<id,SObject> newmap, CommitHandler ch){
        System.debug('==after Insert'+newmap.size());
        csLogic.parentContactLinking(newObj);//Paramita | B-16964 
        if(!CaseLogic.isInsertedRecurssion){
            csLogic.closeParentCase(newmap);
            csLogic.updateContactOrganisation((Case[])newObj);
            CaseLogic.isInsertedRecurssion = true; 
        }
    }
    
    public override void afterUpdate(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){
        System.debug('==after Update'+CaseLogic.isUpdatedRecurssion);
        System.debug('==newMap'+newMap.size());
        csLogic.parentContactLinking(newObj);//Paramita | B-16964 
        if(!CaseLogic.isUpdatedRecurssion){
            csLogic.closeParentCase(newMap);
            CaseLogic.isUpdatedRecurssion = true;
        }
    }
}