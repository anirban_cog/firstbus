/*************************************************************
* Author: Ayan Hore
* Created Date: 11/23/17
* Version1: B-16753, B-16813, B-16786, B-16787
* Details: Centralized logic class 
* Purpose:Trigger logic utility class
**************************************************************/
public without sharing class TransactionManagementLogic {
    
    public List<Purchase__b> upsertPurchaseBO(List<Purchases_Processor__c> purchaseProcessorList, Map<Id,Id> purchaseToTransBOMap){
        List<Purchase__b> returnList = new List<Purchase__b>();
        for(Purchases_Processor__c obj: purchaseProcessorList){
            Purchase__b purchaseBO = new Purchase__b();
            purchaseBO.Activation_Time__c = obj.Activation_Time__c;
            purchaseBO.Device_Id__c = obj.Device_Id__c;
            purchaseBO.Expiry_Time__c = obj.Expiry_Time__c;
            purchaseBO.Price__c = obj.Price__c;
            purchaseBO.Product_Issued__c = obj.Product_Issued__c;
            purchaseBO.Promotional_Code__c = obj.Promotional_Code__c;
            purchaseBO.Ticket_Category__c = obj.Ticket_Category__c;
            purchaseBO.Ticket_Id__c = obj.Ticket_Id__c;
            purchaseBO.Purchases_Processor__c = obj.Id;
            purchaseBO.TTL__c = obj.TTL__c;
            if(purchaseToTransBOMap.containsKey(obj.Id) && purchaseToTransBOMap.get(obj.Id) != NULL){
                purchaseBO.Transaction__c = purchaseToTransBOMap.get(obj.Id);
                returnList.add(purchaseBO);
                system.debug('Purchase Processor BO Created : '+purchaseBO);
            }
            else{
                system.debug('Purchase Processor object misses out : '+obj.Id);
            }
        }
        
        return returnList;
    }
    
    public Map<Id,Id> getPurchaseToTransactionMap(Map<id,Purchases_Processor__c> newmap, Map<Id,Transaction_Processor__c> transMap){
        Map<Id,Id> purchaseToTransBOMap = new Map<Id,Id>();
        List<Id> listOfTransProcessIds = new List<Id>();
        
        for(Purchases_Processor__c obj : newmap.values()){
            purchaseToTransBOMap.put(obj.Id, obj.Transaction__c);
        }
        return purchaseToTransBOMap;
    }
    
    public Map<Id,Transaction_Processor__c> getTransMap(List<Id> listOfTransProcessIds){
        Map<Id,Transaction_Processor__c> transMap = new Map<Id,Transaction_Processor__c>();
        transMap = new Map<Id,Transaction_Processor__c>([select Id, Name, Amount__c, Payment_Form__c, PSP_Transaction_ID__c, Purchase_Date__c, 
                                                         Transaction_Id__c, Email__c, Web_Reference_ID__c, Customer__c, OpCo_Organisation__c, Source__c,
                                                         Device_ID__c, TransactionBO__c from Transaction_Processor__c where Id IN :listOfTransProcessIds]);
        return transMap;
    }
    
    public List<Transaction__b> upsertTransactionBO(List<Transaction_Processor__c> transactionList, Map<Id,Contact> correctContactMap){
        List<Transaction__b> transBOList = new List<Transaction__b>();
        
        for(Transaction_Processor__c transactionObj : transactionList){
            Transaction__b tranBigO = new Transaction__b();
            if(correctContactMap.containsKey(transactionObj.Id)){
                Contact tempContact = correctContactMap.get(transactionObj.Id);
                tranBigO.Customer__c = tempContact.Duplicate__c ? tempContact.Original_Contact__c : tempContact.Id;
                tranBigO.Amount__c = transactionObj.Amount__c;
                tranBigO.Core3_ID__c = transactionObj.Core3_ID__c;
                tranBigO.Device_Id__c = transactionObj.Device_ID__c;
                tranBigO.OpCo_Organisation__c = transactionObj.OpCo_Organisation__c;
                tranBigO.Payment_Form__c = transactionObj.Payment_Form__c;
                tranBigO.PSP_Transaction_ID__c = transactionObj.PSP_Transaction_ID__c;
                tranBigO.Purchase_Date__c = transactionObj.Purchase_Date__c;
                tranBigO.Source__c = transactionObj.Source__c;
                tranBigO.Transaction_Id__c = transactionObj.Transaction_Id__c;
                tranBigO.Transaction_Processor__c = transactionObj.Id;
                transBOList.add(tranBigO);
                
            }
            else
                system.debug('Missed out : '+transactionObj.Id);
        } 
        
        return transBOList;
    }
    
    public Map<Id,Contact> createContactToTransactionMap(List<Transaction_Processor__c> transRecordList){
        Map<Id,Contact> returnMap = new Map<Id,Contact>();
        List<Id> contactIds = new List<Id>();
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        for(Transaction_Processor__c obj : transRecordList){
            contactIds.add(obj.Customer__c);
        }
        contactMap = new Map<Id,Contact>([select id,Original_Contact__c,Duplicate__c,Source__c from Contact where id in :contactIds]);
        for(Transaction_Processor__c obj : transRecordList){
            if(contactMap.containsKey(obj.Customer__c)){
                returnMap.put(obj.Id,contactMap.get(obj.Customer__c));
            }
            
        }
        return returnMap;
    }
    
    
    public void updateSummaryInformation(Map<Id,Transaction_Processor__c> transMap, Map<id,SObject> newmap){
        Map<Id,Contact> contactMapToBeUpdated = new Map<Id,Contact>();
        Map<Id,Contact> contactListToBeUpdated = new Map<Id,Contact>();
        Map<String,Transaction_Summary__c> transactionSummaryMap = new Map<String,Transaction_Summary__c>(); // Key of (Contact-Month-Year) -> Transaction Summary
        Map<String,Transaction_Summary__c> transSummaryToBeUpdated = new Map<String,Transaction_Summary__c>();
        Map<Id,String> TransactionCategoryMap = new Map<Id,String>();
        List<Id> contactIds = new List<Id>();
        for(Transaction_Processor__c obj : transMap.values()){
            contactIds.add(obj.Customer__c);
            TransactionCategoryMap.put(obj.Id,obj.Source__c);
        }
        
        Map<String,Integer> LastCountPerCategoryMap = new Map<String,Integer>();
        
        Map<Id,Integer> PurchasesPerTransactionMap = new Map<Id,Integer>();
        for(SObject obj : newmap.values()){
            Purchases_Processor__c tempObj = (Purchases_Processor__c) obj;
            if(!TransactionCategoryMap.containsKey(tempObj.Transaction__c))
                TransactionCategoryMap.put(tempObj.Transaction__c,tempObj.Ticket_Category__c);
            if(PurchasesPerTransactionMap.containsKey(tempObj.Transaction__c))
                PurchasesPerTransactionMap.put(tempObj.Transaction__c, PurchasesPerTransactionMap.get(tempObj.Transaction__c)+1);
            else
                PurchasesPerTransactionMap.put(tempObj.Transaction__c,1);
        }
        system.debug('TransactionCategoryMap : '+TransactionCategoryMap);
        Transaction_Processor__c lastWebTransaction = null;
        Transaction_Processor__c lastMTicketTransaction = null;
        for(Transaction_Processor__c obj : transMap.values()){
            system.debug('TransactionCategoryMap.get(obj.Id) : '+TransactionCategoryMap.get(obj.Id));
            if(TransactionCategoryMap.get(obj.Id) == Constants.Web){
                if(lastWebTransaction != NULL){
                    //system.debug('lastWebTransaction.Purchase_Date__c : '+lastWebTransaction.Purchase_Date__c + ' | obj.Purchase_Date__c : '+obj.Purchase_Date__c);
                    if(lastWebTransaction.Purchase_Date__c < obj.Purchase_Date__c)
                        lastWebTransaction = obj;
                }
                else
                    lastWebTransaction = obj;
            }
            else{
                if(lastMTicketTransaction != NULL){
                    if(lastMTicketTransaction.Purchase_Date__c < obj.Purchase_Date__c)
                        lastMTicketTransaction = obj;
                }
                else
                    lastMTicketTransaction = obj;                
            }
        }
        
        system.debug('CHECK-0 : '+lastWebTransaction);
        contactMapToBeUpdated = new Map<Id,Contact>([SELECT Id, Salutation, Phone, Fax, MobilePhone, Email, Title, Source__c, Gender__c, Control_Group__c, Last_Ticket_Activated_Date__c, 
                                                     Age_Group__c, MTickets_Reference_ID__c, Reason_for_use__c, Web_Reference_ID__c, Source_System_Created_Date__c, Source_System_Updated_Date__c, 
                                                     MailingList2__c, MailingList1__c, MailingList3__c, University__c, Mticket_Total_Purchase_Value__c, Mticket_Last_Purchase_Type__c, 
                                                     Mticket_Last_Purchase_Date__c, Mticket_First_Purchase_Type__c, Web_Total_Purchase_Count__c, Web_Total_Purchase_Value__c, Last_Purchase_Count__c, 
                                                     Account_External_Id__c, Last_Purchase_Date__c, First_Purchase_Type__c, Like_to_receive_offers__c, Last_Purchase_Type__c, Channels__c, 
                                                     Web_First_Purchase_Date__c, Web_Last_Purchase_Type__c, Frequency_of_Use__c, Web_Last_Purchase_Date__c, Web_First_Purchase_Type__c, 
                                                     First_Purchase_Date__c, Like_to_receive_latest_update_and_offer__c, Organisation_5__c, Organisation_4__c, Organisation_3__c, Organisation_2__c, 
                                                     Organisation_1__c, Last_Interaction__c, Original_Source__c, Original_Web_Source__c, Total_Purchase_Value__c, 
                                                     Last_Marketing_Preference_Date__c, Original_Contact__c, Duplicate__c, Mticket_Total_Purchase_Count__c FROM Contact where Id IN :contactIds]);
        
        for(Transaction_Summary__c obj : [SELECT Id, Name, Total_Purchase_Value__c, Customer__c, Year__c, Month__c FROM Transaction_Summary__c where Customer__c IN :contactIds]){
            transactionSummaryMap.put(obj.Customer__c+'-'+obj.Month__c+'-'+obj.Year__c, obj);
        }
        Map<Integer,String> monthMap = Constants.getMonthMap();
        
        for(SObject obj : newmap.values()){
            Purchases_Processor__c tempObj = (Purchases_Processor__c) obj;
            //Update contact details - start
            if(tempObj.Transaction__c != NULL){
                Transaction_Processor__c transObj = transMap.get(tempObj.Transaction__c);
                
                Contact tempContact = contactListToBeUpdated.containsKey(transObj.Customer__c) ? contactListToBeUpdated.get(transObj.Customer__c) : contactMapToBeUpdated.get(transObj.Customer__c);
                system.debug('tempContact : '+tempContact+' | '+contactListToBeUpdated.containsKey(transObj.Customer__c));
                if(tempContact != NULL){
                    if(tempObj.Updated__c){
                        tempContact.Last_Ticket_Activated_Date__c = tempContact.Last_Ticket_Activated_Date__c == NULL ? tempObj.Activation_Time__c.date() : (tempContact.Last_Ticket_Activated_Date__c < tempObj.Activation_Time__c.date() ? tempObj.Activation_Time__c.date() : tempContact.Last_Ticket_Activated_Date__c);
                            }
                    else{
                        //Version 2 change start
                        if(transObj.Source__c == Constants.MTicket){
                            //Version 2 change end
                            //MTicket
                            tempContact.Mticket_Total_Purchase_Value__c = !tempObj.Updated__c ? (tempContact.Mticket_Total_Purchase_Value__c != NULL ? (tempContact.Mticket_Total_Purchase_Value__c + /*transObj.Amount__c*/ tempObj.Price__c) : /*transObj.Amount__c*/tempObj.Price__c) : tempContact.Mticket_Total_Purchase_Value__c;
                            tempContact.Mticket_Last_Purchase_Date__c = (tempContact.Mticket_Last_Purchase_Date__c == NULL || tempContact.Mticket_Last_Purchase_Date__c < transObj.Purchase_Date__c) ? transObj.Purchase_Date__c : tempContact.Mticket_Last_Purchase_Date__c;
                            tempContact.Mticket_First_Purchase_Date__c = (tempContact.Mticket_First_Purchase_Date__c == NULL || tempContact.Mticket_First_Purchase_Date__c > transObj.Purchase_Date__c) ? transObj.Purchase_Date__c : tempContact.Mticket_First_Purchase_Date__c;
                            
                            if(tempObj != NULL){
                                system.debug('CURRENT Value : '+tempContact.Mticket_Total_Purchase_Count__c);
                                tempContact.Mticket_Total_Purchase_Count__c = tempContact.Mticket_Total_Purchase_Count__c != NULL ? tempContact.Mticket_Total_Purchase_Count__c + 1 : 1;
                                tempContact.Mticket_Last_Purchase_Type__c = !tempObj.Updated__c ? tempObj.Ticket_Category__c : tempContact.Mticket_Last_Purchase_Type__c;
                                tempContact.Mticket_First_Purchase_Type__c = String.isBlank(tempContact.Mticket_First_Purchase_Type__c) ? tempObj.Ticket_Category__c : tempContact.Mticket_First_Purchase_Type__c;
                                tempContact.Mticket_Last_Purchase_Count__c = PurchasesPerTransactionMap.get(lastMTicketTransaction.Id);
                            }
                        }
                        else{
                            //WEB
                            tempContact.Web_First_Purchase_Date__c = (tempContact.Web_First_Purchase_Date__c == NULL || tempContact.Web_First_Purchase_Date__c > transObj.Purchase_Date__c) ? transObj.Purchase_Date__c : tempContact.Web_First_Purchase_Date__c;
                            tempContact.Web_Last_Purchase_Date__c = (tempContact.Web_Last_Purchase_Date__c == NULL || tempContact.Web_Last_Purchase_Date__c < transObj.Purchase_Date__c) ? transObj.Purchase_Date__c : tempContact.Web_Last_Purchase_Date__c;
                            tempContact.Web_Total_Purchase_Value__c = !tempObj.Updated__c && tempContact.Web_Total_Purchase_Value__c != NULL ? (tempContact.Web_Total_Purchase_Value__c + /*transObj.Amount__c*/tempObj.Price__c) : /*transObj.Amount__c*/tempObj.Price__c;
                            
                            if(tempObj != NULL){
                                //system.debug('CHECK-01 : '+PurchasesPerTransactionMap+' | '+lastWebTransaction);
                                tempContact.Web_Last_Purchase_Count__c = PurchasesPerTransactionMap.get(lastWebTransaction.Id);
                                //system.debug('CHECK-02 : '+tempObj.Ticket_Category__c+' | '+tempContact.Web_Last_Purchase_Type__c);
                                tempContact.Web_Last_Purchase_Type__c = TransactionCategoryMap.containsKey(tempObj.Transaction__c) ? TransactionCategoryMap.get(tempObj.Transaction__c) : tempContact.Web_Last_Purchase_Type__c;
                                tempContact.Web_Total_Purchase_Count__c = !tempObj.Updated__c && tempContact.Web_Total_Purchase_Count__c != NULL ? tempContact.Web_Total_Purchase_Count__c + 1 : 1;
                                tempContact.Web_First_Purchase_Type__c = String.isBlank(tempContact.Web_First_Purchase_Type__c) ? tempObj.Ticket_Category__c : tempContact.Web_First_Purchase_Type__c;
                            }
                        }
                        //AGGREGATE
                        
                        if(tempObj != NULL){
                            if(tempContact.Mticket_First_Purchase_Date__c < tempContact.Web_First_Purchase_Date__c || (tempContact.Web_First_Purchase_Date__c == NULL)){
                                tempContact.First_Purchase_Date__c = tempContact.Mticket_First_Purchase_Date__c;
                                tempContact.First_Purchase_Type__c = !String.isBlank(tempObj.Ticket_Category__c) ? tempObj.Ticket_Category__c : Constants.MTicket;
                            }
                            else{
                                tempContact.First_Purchase_Date__c = tempContact.Web_First_Purchase_Date__c;
                                tempContact.First_Purchase_Type__c = !String.isBlank(tempObj.Ticket_Category__c) ? tempObj.Ticket_Category__c : Constants.Web;
                            }
                            
                            if(tempContact.Mticket_Last_Purchase_Date__c > tempContact.Web_Last_Purchase_Date__c || (tempContact.Web_Last_Purchase_Date__c == NULL)){
                                tempContact.Last_Purchase_Date__c = tempContact.Mticket_Last_Purchase_Date__c;
                                tempContact.Last_Purchase_Type__c = !String.isBlank(tempObj.Ticket_Category__c) ? tempObj.Ticket_Category__c : Constants.MTicket;
                            }
                            else{
                                tempContact.Last_Purchase_Date__c = tempContact.Web_Last_Purchase_Date__c;
                                tempContact.Last_Purchase_Type__c = !String.isBlank(tempObj.Ticket_Category__c) ? tempObj.Ticket_Category__c : Constants.Web;
                            }
                            if(tempObj.Activation_Time__c != NULL)
                                tempContact.Last_Ticket_Activated_Date__c = tempContact.Last_Ticket_Activated_Date__c == NULL ? tempObj.Activation_Time__c.date() : (tempContact.Last_Ticket_Activated_Date__c < tempObj.Activation_Time__c.date() ? tempObj.Activation_Time__c.date() : tempContact.Last_Ticket_Activated_Date__c);
                                    tempContact.Total_Purchase_Value__c = (tempContact.Web_Total_Purchase_Value__c != NULL ? tempContact.Web_Total_Purchase_Value__c : 0) + (tempContact.Mticket_Total_Purchase_Value__c != NULL ? tempContact.Mticket_Total_Purchase_Value__c : 0);
                            tempContact.Last_Purchase_Count__c = !tempObj.Updated__c && tempContact.Web_Last_Purchase_Count__c != NULL ? tempContact.Web_Last_Purchase_Count__c : (tempContact.Mticket_Last_Purchase_Count__c != NULL ? tempContact.Mticket_Last_Purchase_Count__c : 1);
                                tempContact.Total_Purchase_Count__c = (tempContact.Mticket_Total_Purchase_Count__c != NULL ? tempContact.Mticket_Total_Purchase_Count__c : 0) + (tempContact.Web_Total_Purchase_Count__c != NULL ? tempContact.Web_Total_Purchase_Count__c : 0);    
                        }
                    }
                    contactListToBeUpdated.put(tempContact.Id,tempContact);
                    //Update contact details - end
                    
                    // Update transaction summary - start
                    if(tempObj != NULL && !tempObj.Updated__c){
                        Integer transactionYear = tempObj.Activation_Time__c != NULL ? tempObj.Activation_Time__c.year() : (transObj.Purchase_Date__c != NULL ? transObj.Purchase_Date__c.year() : NULL);
                            Integer transactionMonth = tempObj.Activation_Time__c != NULL ? tempObj.Activation_Time__c.month() : (transObj.Purchase_Date__c != NULL ? transObj.Purchase_Date__c.month() : NULL);
                                
                                //if existing transaction summary record exists for the customer
                                if(transactionSummaryMap.containsKey(transObj.Customer__c+'-'+monthMap.get(transactionMonth)+'-'+transactionYear)){
                                    Transaction_Summary__c temp = transactionSummaryMap.get(transObj.Customer__c+'-'+monthMap.get(transactionMonth)+'-'+transactionYear);
                                    temp.Total_Purchase_Value__c +=tempObj.Price__c;
                                    transSummaryToBeUpdated.put(transObj.Customer__c+'-'+monthMap.get(transactionMonth)+'-'+transactionYear, temp);
                                }
                        //if no record exists for the customer in transaction summary
                        //or if no match is found with the year and month of transaction
                        else{
                            Transaction_Summary__c temp = new Transaction_Summary__c(Total_Purchase_Value__c = tempObj.Price__c, Month__c = monthMap.get(transactionMonth), Year__c = transactionYear, Customer__c = transObj.Customer__c);
                            transSummaryToBeUpdated.put(transObj.Customer__c+'-'+monthMap.get(transactionMonth)+'-'+transactionYear, temp);
                            
                            //Update transaction summary map to ensure subsequent transactions do not create duplicate summary information
                            transactionSummaryMap.put(transObj.Customer__c+'-'+monthMap.get(transactionMonth)+'-'+transactionYear, temp);
                        }
                        
                        // Update transaction summary - end
                    }
                }
            }
            /*
// Update contact list
if(!contactListToBeUpdated.isEmpty() && Schema.sObjectType.Contact.isUpdateable()){
try{
update contactListToBeUpdated.values();
}
catch(Exception e){
system.debug('ERROR : '+e.getMessage()+' | '+e.getCause());
}
}*/
            /*
// Update transaction summary
if(!transSummaryToBeUpdated.isEmpty() && Schema.sObjectType.Transaction_Summary__c.isCreateable() && Schema.sObjectType.Transaction_Summary__c.isUpdateable()){
upsert transSummaryToBeUpdated.values();
}*/
        }
        // Update contact list
        if(!contactListToBeUpdated.isEmpty() && Schema.sObjectType.Contact.isUpdateable()){
            try{
                update contactListToBeUpdated.values();
            }
            catch(Exception e){
                system.debug('ERROR : '+e.getMessage()+' | '+e.getCause());
            }
        }
        // Update transaction summary
        if(!transSummaryToBeUpdated.isEmpty() && Schema.sObjectType.Transaction_Summary__c.isCreateable() && Schema.sObjectType.Transaction_Summary__c.isUpdateable()){
            upsert transSummaryToBeUpdated.values();
        }
    }
}