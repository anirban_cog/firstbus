public with sharing class UtilsMonitoring {
//Monitoring variables
public static Set< String > monitorSet;
public static Map<System.Type, Map<String, list<ErrorMessage>>> saveMonitoringMessagesMap;

	public class ErrorMessage{
		//If the Debugger__c contains other fields than the standard msg__c and DebugParent__c such as you may want to record 
		//specific error codes returned in a webservice in a separate field so it is easier to report on, so you can provide KeyValue[] kVals which will 
		//populate other bespoke fields to hold these values. Monitoring now becomes very bespoke for your application
		
		public String msg;
		public KeyValue[] kVals;
		
		public ErrorMessage(String aMsg, KeyValue[] aKVals){
			msg = aMsg;
			kVals = aKVals;
		}
	}
	
	public static Boolean getMonitoringCoverage(){
		//if this is set to true then monitoring will output everything held in saveMonitoringMessagesMap otherwise it will revert to whatever is set to the default section to monitor
		//eg UtilsMonitoring.saveMonitoringMesages(Account.class); will only save messgaes generated whereby the key is Account.class, but if the CS is True it will just save everything in saveMonitoringMessagesMap  
		return (((MonitoringCoverage__c.getOrgDefaults() != null) ? MonitoringCoverage__c.getOrgDefaults().value__c : false) || MonitoringCoverage__c.getInstance(UserInfo.getUserId()).value__c  || MonitoringCoverage__c.getInstance(UserInfo.getProfileId()).value__c) ;
	}
	
	public static void setupMonitoring(){
		//builds monitorSet of Monitoring that has been activated by the CS
		
		if (monitorSet == null || monitorSet.isempty()){
			Map<String, Monitoring__c> mon = Monitoring__c.getAll();
			system.debug('##mon ' + mon);
			if (!mon.isempty()){
				monitorSet = new Set< String >();
				for (Monitoring__c thisMon: mon.Values()){
					system.debug('##thisMon ' + thisMon);
					if ((thisMon.Active__c && thisMon.Monitor_Datetime_From__c == null && thisMon.Monitor_Datetime_To__c == null) || (thisMon.Active__c && thisMon.Monitor_Datetime_From__c >= Datetime.Now() && thisMon.Monitor_Datetime_To__c <= Datetime.Now())){
						if (thisMon.Name != null) monitorSet.add(thisMon.Name);
					}
				}
				system.debug('##monitorSet ' + monitorSet);
				if (!monitorSet.isempty() && saveMonitoringMessagesMap == null){
					saveMonitoringMessagesMap = new Map<System.Type, Map<String, list<ErrorMessage>>>();
				}
			}
		}		
	}
	
	public static void buildMonitoringMessage(System.Type objMonitor, String ref, String msg, KeyValue[] kVals){		
	//if getMonitoringCoverage() return true all monitoring messages are saved, otherwise if the Type is Active specified in setupMonitoring()
	 
		if (getMonitoringCoverage() || (monitorSet != null && !monitorSet.isempty() && monitorSet.contains(ref))){
			system.debug('##saveMonitoringMessagesMap1 ' + saveMonitoringMessagesMap);
			if (saveMonitoringMessagesMap != null && saveMonitoringMessagesMap.containskey(objMonitor)){
				if (saveMonitoringMessagesMap.get(objMonitor).containskey(ref))
					(saveMonitoringMessagesMap.get(objMonitor).get(ref)).add(new ErrorMessage(msg, kVals));
				else{
					saveMonitoringMessagesMap.get(objMonitor).put(ref, new list<ErrorMessage>{new ErrorMessage(msg, kVals)}); 
				}		
				system.debug('##saveMonitoringMessagesMap2 ' + saveMonitoringMessagesMap);		
			}
			else{
				saveMonitoringMessagesMap.put(objMonitor, new Map<String, list<ErrorMessage>>{ref => new list<ErrorMessage>{new ErrorMessage(msg, kVals)}}); 
				system.debug('##saveMonitoringMessagesMap3 ' + saveMonitoringMessagesMap);
			}
		}
	}
	
	public static void saveMonitoringMesages(System.Type objMonitor){
		//saves only a particular type of monitoring, so you can save only debug log coming from Trigger using Account.class, if you want to output only from a class called XYZ you will specify XYZ.class instead
		
		system.debug('##saveMonitoringMessagesMap3 ' + saveMonitoringMessagesMap);
		if (saveMonitoringMessagesMap != null && saveMonitoringMessagesMap.containskey(objMonitor)){
			if (Limits.getDmlStatements() <= (Limits.getLimitDmlStatements() -2)){ 
				
				Map<String, list<ErrorMessage>> saveMsgs = saveMonitoringMessagesMap.get(objMonitor);
				if (saveMsgs.keyset().size() < Limits.getLimitDmlRows()){ 				
					DebugParent__c[] newDbgParents = new DebugParent__c[]{};
					for (String kys : saveMsgs.keyset())
						newDbgParents.add(new DebugParent__c(Run_Category__c = kys));
					
					insert newDbgParents;
					
					Map<String,id> debugParentMap = new Map<String,id>(); 
					for (DebugParent__c ErrParent : newDbgParents)
						debugParentMap.put(ErrParent.Run_Category__c, ErrParent.id);
						
				    Debug_Error__c[] allErrs = new Debug_Error__c[]{};
			        for (String ky : saveMsgs.keyset()){
			            for (ErrorMessage errMsg : saveMsgs.get(ky)){
			            	if (allErrs.size() < Limits.getLimitDmlRows())  			
			            		allErrs.add((Debug_Error__c)UtilityObjectData.setObjData(new Debug_Error__c(Error__c=errMsg.msg, Debug_Parent__c = (debugParentMap.get(ky) != null) ? debugParentMap.get(ky) : null), errMsg.kVals));
			            }      
			        }

			        if (!allErrs.isempty())
			        	UtilDML_Master.insertObjects(allErrs);
			        	
			        saveMonitoringMessagesMap = new Map<System.Type, Map<String, list<ErrorMessage>>>();
				}
			}
			
		}          
	}

	public static void saveMonitoringMesages(){
		//no monitor type passed to function so this will output all monitoring messages that has been accepted in buildMonitoringMessage() using getMonitoringCoverage()  and monitorSet
		  
		system.debug('##saveMonitoringMessagesMap4 ' + saveMonitoringMessagesMap);
		if (saveMonitoringMessagesMap != null){
			if (Limits.getDmlStatements() <= (Limits.getLimitDmlStatements() -2)){ 
				integer imap = 0;
				Map<integer, DebugParent__c> newDbgParents = new Map<integer, DebugParent__c>();
				Map<integer, Debug_Error__c[]> allErrs = new Map<integer, Debug_Error__c[]>();
				
				for (System.Type eachType : saveMonitoringMessagesMap.keyset()){				
					Map<String, list<ErrorMessage>> saveMsgs = saveMonitoringMessagesMap.get(eachType);
					for (String ky : saveMsgs.keyset()){
						newDbgParents.put(imap, new DebugParent__c(Run_Category__c = ky));
							
			            for (ErrorMessage errMsg : saveMsgs.get(ky)){
			            	if (allErrs.size() < Limits.getLimitDmlRows()) { 			
			            		if (!allErrs.containskey(imap))
			            			allErrs.put(imap, new Debug_Error__c[]{});
			            		system.debug('##allErrs ' + allErrs);
			            		allErrs.get(imap).add((Debug_Error__c)UtilityObjectData.setObjData(new Debug_Error__c(Error__c=errMsg.msg), errMsg.kVals));
			            	}
			            } 
			            ++imap;     
			        }			        
				}
				
				insert newDbgParents.values();
				system.debug('##allErrs ' + allErrs);
				system.debug('##newDbgParents.values() ' + newDbgParents.values());
				if (!allErrs.isempty()){
					Debug_Error__c[] totalErrors = new Debug_Error__c[]{};
					for (integer thisIMap : allErrs.keyset()){
						DebugParent__c dP = newDbgParents.get(thisIMap);
						system.debug('##dp ' + dP);
						system.debug('##allErrs.get(thisIMap) ' + thisIMap + ' ' + allErrs.get(thisIMap));
						if (dP != null && allErrs != null && allErrs.containskey(thisIMap) && allErrs.get(thisIMap) != null){
							for (Debug_Error__c er : allErrs.get(thisIMap)){
								system.debug('##er ' + er);
								if (er != null ){
									er.Debug_Parent__c = dP.id;
									totalErrors.add(er);
								}	
							}
						}
					}
				    
				    if (!totalErrors.isempty())
				    	UtilDML_Master.insertObjects(totalErrors);
				}
							        	
			    saveMonitoringMessagesMap = new Map<System.Type, Map<String, list<ErrorMessage>>>();
			}
		}
				          
	}

}