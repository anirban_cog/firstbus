public class UpsertContact{
  
    //Metod to Update contacts 
    @InvocableMethod
    public static void updateContacts(List<Id> caseIds)
    {
        Set<Id> setContactids = new Set<Id>();
        
        List<Case> lstCase = [Select Id, ContactId From Case where Id In : caseIds];
        for(Case objCase : lstCase ){
            setContactids.add(objCase.ContactId);
        }
        
        List<Contact> lstContactToUpdate = new List<Contact>();
        
        List<Contact> lstContact = [select id,Like_to_receive_latest_update_and_offer__c from contact where Id IN : setContactids];      
        
        for(Contact objContact : lstContact){
            Contact objCon = new Contact();
            //if (Schema.sObjectType.Contact.fields.Id.isUpdateable())
            objCon.Id = objContact.Id;
            if (Schema.sObjectType.Contact.fields.Like_to_receive_latest_update_and_offer__c.isUpdateable())
                objCon.Like_to_receive_latest_update_and_offer__c = true;
            lstContactToUpdate.add(objCon);
        }
        
        //Update if list is not empty
        if(lstContactToUpdate.size() > 0){
            update lstContactToUpdate;
        }        
    }    
    

}