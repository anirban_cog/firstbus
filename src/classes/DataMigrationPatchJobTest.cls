@isTest
public class DataMigrationPatchJobTest {
    static TestDataComplexFunctions tdc = new TestDataComplexFunctions(); 
    @isTest
    static void testMethod1(){
        Profile prof = [select Id, Name from Profile where Name = 'First Group Standard User'];
        User stdUser = new User(Alias = 'standt', Email='standardtestClassUser@firstbus.co.uk', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = prof.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='standardtestClassUser@firstbus.co.uk');
        insert stdUser;
        
        List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
        CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-19',Account_API__c='PersonEmail',Contact_API__c='Email'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-21',Account_API__c='First_Purchase_Type__c',Contact_API__c='First_Purchase_Type__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-17',Account_API__c='Like_to_receive_offers__c',Contact_API__c='Like_to_receive_offers__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-7',Account_API__c='MailingList3__c',Contact_API__c='MailingList3__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-8',Account_API__c='LastPurchaseDate__c',Contact_API__c='Last_Purchase_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-9',Account_API__c='Last_Purchase_Type__c',Contact_API__c='Last_Purchase_Type__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-10',Account_API__c='Reason_for_use__c',Contact_API__c='Reason_for_use__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-11',Account_API__c='Source__c',Contact_API__c='Source__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-12',Account_API__c='Source_Created_Date__c',Contact_API__c='Source_System_Created_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-13',Account_API__c='Source_Amended_Date__c',Contact_API__c='Source_System_Updated_Date__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-14',Account_API__c='University__c',Contact_API__c='University__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-1',Account_API__c='Age_Range__c',Contact_API__c='Age_Group__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-2',Account_API__c='Control_Group__c',Contact_API__c='Control_Group__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-3',Account_API__c='Gender__c',Contact_API__c='Gender__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-4',Account_API__c='Last_Purchase_Count__c',Contact_API__c='Last_Purchase_Count__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-5',Account_API__c='MailingList1__c',Contact_API__c='MailingList1__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-6',Account_API__c='MailingList2__c',Contact_API__c='MailingList2__c'));
        insert CSObjList;
        
        System.runAs(stdUser){
            RecordType rt = [select Id, Name from RecordType where sObjectType = 'Account' AND Name = 'Person Account'].get(0);
            Account testAccount = new Account(
                FirstName = 'Test',
                LastName = 'Account',
                RecordTypeId = rt.Id,
                BillingStreet = '11 Irwin Street',
                BillingCity = 'LEEDS',
                BillingState = 'West Yorkshire',
                BillingCountry = 'United Kingdom',
                CelerityBusinessKey__c = '4147347',
                Like_to_receive_latest_offers__c = false,
                Total_Purchase_Count__c = 1.0,
                FirstPurchaseDate__c = Date.newInstance(2017,10,21),
                LastPurchaseDate__c = Date.newInstance(2017,10,21),
                SupplierBusinessKey__c = 'sArJPRxKrAO',
                Like_to_receive_offers__c = false,
                Reference_ID__c = 'sArJPRxKrAO2',
                Organisation_1__c = 'Aberdeen',
                Organisation_2__c = 'First Bus Bath',
                Organisation_3__c = 'First Bus Berkshire',
                First_Purchase_Type__c = 'FirstDay - Adult - Leeds (FirstDay)',
                Last_Purchase_Type__c = 'FirstDay - Adult - Leeds (FirstDay)',
                Source__c = 'MTicket',
                Purchase_Count__c = 1.0,
                Ticket_Activation_Date__c = Date.newInstance(2017,10,21),
                Organisation_4__c = 'First Bus Borders',
                Organisation_5__c = 'First Bus Bradford',
                Source_Created_Date__c = DateTime.newInstance(2017,10,12,10,604,36249),
                Source_Amended_Date__c = DateTime.newInstance(2017,10,22,15,935,56138),
                Amended_Transaction_Date__c = DateTime.newInstance(2017,10,23,7,423,25425),
                Control_Group__c = false);
            insert testAccount;
            system.assert(testAccount.Id != NULL, 'Account Not Created');
            Contact testContact = new Contact(
                LastName = 'Contact',
                FirstName = 'Test',
                MobilePhone = '7815156457',
                Email = 'thom4snewton@outlook.com',
                Source__c = 'MTicket',
                Control_Group__c = false,
                MTickets_Reference_ID__c = 'sArJPRxKrAO',
                Source_System_Created_Date__c = DateTime.newInstance(2017,10,12,10,604,36249),
                Source_System_Updated_Date__c = DateTime.newInstance(2017,10,22,15,935,56138),
                Last_Purchase_Count__c = 1.0,
                Account_External_Id__c = testAccount.Id,
                Last_Purchase_Date__c = DateTime.newInstance(2017,10,21,0,0,0),
                First_Purchase_Type__c = 'FirstDay - Adult - Leeds (FirstDay)',
                Like_to_receive_offers__c = false,
                Last_Purchase_Type__c = 'FirstDay - Adult - Leeds (FirstDay)',
                First_Purchase_Date__c = DateTime.newInstance(2017,10,21,0,0,0),
                Like_to_receive_latest_update_and_offer__c = false,
                Total_Purchase_Count__c = 1.0,
                Duplicate__c = false,
                FirstName__c = 'Colin',
                Email__c = 'thom4snewton@outlook.com',
                Organisation_1__c = 'Aberdeen',
                Organisation_2__c = 'First Bus Bath',
                Organisation_3__c = 'First Bus Berkshire',
                Organisation_4__c = 'First Bus Borders',
                Organisation_5__c = 'First Bus Bradford');
            insert testContact;
            testContact = [select Id, Source__c, Channels__c, Last_Interaction__c, Original_Source__c, Original_Web_Source__c, Account_External_Id__c from Contact where  MTickets_Reference_ID__c = 'sArJPRxKrAO'];
            system.assert(testContact.Id != NULL, 'Contact Not Created');
            testContact.Last_Interaction__c = null;
            testContact.Original_Source__c = NULL;
            testContact.Original_Web_Source__c = NULL;
            testContact.Channels__c = null;
            update testContact;
            Test.startTest();
            database.executeBatch(new DataMigrationPatchJob(),1);
            Test.stopTest();
        }
    }
}