/*************************************************************
* Author: Ayan Hore
* Created Date: 11/29/17
* Version1: This is the initial version
* Details: Test class for Transaction Management
* Purpose:Test class for Transaction Management
**************************************************************/
@isTest
public class TransactionManagementLogicTest {
    
    static testMethod void TransactionManagementMethod(){
        createTestData();
        Test.startTest();
        try{
            Database.executeBatch(new TransactionManagementBatch());
        }
        catch(Exception e){
            system.debug('Exception : '+e.getMessage());
        }
        Test.stopTest();
    }

    public static void createTestData(){
        List<Contact> conList = new List<Contact>();
        Contact con = new Contact();
        DateTime currentDateTime = DateTime.now();
        DateTime expDateTime = DateTime.now();
        expDateTime=DateTime.newInstance(expDateTime.month()+1);
        con.LastName = 'TestClass';
        con.Duplicate__c = true;
        con.Source__c ='Web';
        conList.add(con);
        insert con;
		System.assertEquals(1,[select count() from Contact]);
        
        //Transaction Processor List of key value pairs to insert
        List<Transaction_Processor__C> transactionProcessorKV = new List<Transaction_Processor__C>();
        Transaction_Processor__C tp = new Transaction_Processor__C();
        tp.Name = 'Transaction-name';
        tp.Amount__c=500;
        tp.Payment_Form__c='Cash';
        tp.PSP_Transaction_ID__c='P-09';    
        tp.Purchase_Date__c= currentDateTime;
        tp.Transaction_Id__c='T09';
        tp.Email__c='test@test.com';
        tp.Web_Reference_ID__c='WebID01';
        tp.OpCo_Organisation__c='OpOrg';
        tp.Other_Street__c='OtherStreet';
        tp.Core3_ID__c='C968532';
        tp.Other_City__c='Transaction OtherCity';
        tp.Other_Postal_Code__c='254136';
        tp.Other_Country__c='COuntry';
        tp.Salutation__c='Mr';
        tp.Phone__c='9632587410';
        tp.Mailing_Postal_Code__c='457869';
        tp.Mailing_City__c ='Transaction-MailingCity';
        tp.Mailing_Street__c='Transaction-MailingStreet';
        tp.Mailing_Country__c=' Transaction-MailingCountry';
        tp.Last_Name__c='Transaction-LastName';
        tp.First_Name__c= 'Transaction-FIrstName';
        tp.Processed__c=false;
        tp.Source__c='Web';
        tp.Customer__c = con.id;
        transactionProcessorKV.add(tp);
        //Insert Transaction Processor record
        insert transactionProcessorKV;
        System.assertEquals(1,[select count() from Transaction_Processor__C]);
        
 		//Transaction Processor List of key value pairs to insert
        List<Purchases_Processor__c> purchaseProcessorKV = new List<Purchases_Processor__c>();
       	Purchases_Processor__c pp = new Purchases_Processor__c();
        pp.Transaction__c = tp.Id;
        pp.Ticket_Category__c='Transaction-Name';
        pp.Promotional_Code__c='P-09';
        pp.Product_Issued__c= currentDateTime;
        pp.Price__c = 120;
        pp.Activation_Time__c= currentDateTime;
        pp.Expiry_Time__c= expDateTime;
        pp.Device_Id__c='D-01';
        pp.TTL__c = 001;
        pp.Ticket_Id__c='P01';
        pp.PurchaseBO__c='Transaction-Name';
        pp.Processed__c=false;
        pp.Updated__c=false;
        purchaseProcessorKV.add(pp);
        insert purchaseProcessorKV;
        System.assertEquals(1,[select count() from Purchases_Processor__c]);
    }
    
}