/*************************************************************
* Author: Rajat Tripathi
* Created Date: 11/22/17
* Version1: B-16759, B-16891, B-16893
* Details: Handler to be invoked by TransactionProcessor on Transaction_Processor__c object 
* Purpose:Trigger handler on Transaction Processor object
**************************************************************/
public class TransactionProcessorHandler extends TriggerBaseHandler{
    public TransactionProcessorLogic transactionLogic;
    
    public TransactionProcessorHandler(){
        transactionLogic = new TransactionProcessorLogic();
    }
    
    public override void beforeInsert(SObject[] newObj, CommitHandler ch){
        List<Transaction_Processor__c> updatedValues = new List<Transaction_Processor__c>();
        updatedValues = transactionLogic.setFlagValues(newObj,new Map<id,SObject>(),true);
        updatedValues = transactionLogic.relateTransactionToDevices(updatedValues);
        transactionLogic.relatePurchasesToContact(updatedValues); 
    }
    
    public override void beforeUpdate(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){ 
        List<Transaction_Processor__c> updatedValues = new List<Transaction_Processor__c>();
        updatedValues = transactionLogic.setFlagValues(newObj,oldmap,false);
        updatedValues = transactionLogic.relateTransactionToDevices(updatedValues);        
        transactionLogic.relatePurchasesToContact(updatedValues); 
    }
}