public interface ITrigger{
    
    void beforeInsert(SObject[] newObj, CommitHandler ch); 
    
    void afterInsert(SObject[] newObj, Map<id,SObject> newmap, CommitHandler ch); 
    
    void beforeUpdate(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch); 
    
    void afterUpdate(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch); 
    
    void beforeDelete(SObject[] oldObj, Map<id,SObject> oldmap, CommitHandler ch); 
    
    void afterDelete(SObject[] oldObj, Map<id,SObject> oldmap, CommitHandler ch); 
    
    void beforeUnDelete(SObject[] oldObj, Map<id,SObject> oldmap, CommitHandler ch); 
    
    void afterUnDelete(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch); 
    
}