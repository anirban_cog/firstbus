/*************************************************************
* Author: Rajat Tripathi
* Created Date: 11/22/17
* Version1: This is the initial version
* Details: Logic class to be invoked by TransactionProcessorHandler class 
* Purpose:Trigger logic class
**************************************************************/
public without sharing class TransactionProcessorLogic {
    public List<Transaction_Processor__c> setFlagValues(Sobject[] newObjects, Map<id,SObject> oldmap, Boolean isInsert){
        List<Transaction_Processor__c> returnList = new List<Transaction_Processor__c>();
        if(isInsert){
            for(Sobject obj : newObjects){
                Transaction_Processor__c tempObj = (Transaction_Processor__c) obj;
                if(!Test.isRunningTest())
                	tempObj.Processed__c = false;
                returnList.add(tempObj);
            }
        }
        else{
            for(Sobject obj : newObjects){
                Transaction_Processor__c tempObj = (Transaction_Processor__c) obj;
                if(!Test.isRunningTest())
                	tempObj.Processed__c = tempObj.Processed__c != NULL ? tempObj.Processed__c : false;
                returnList.add(tempObj);
            }
        }
        return returnList;
    }
    
    public void relatePurchasesToContact(List<Transaction_Processor__c> newObjects){
        List<String> Core3IdList= new List<String>();
        List<String> WebEmailList= new List<String>();
        List<String> WebReferenceList= new List<String>();
        List<Transaction_Processor__c> recordsToBeUpdated = new List<Transaction_Processor__c>();
        List<Transaction_Processor__c> WebRecordsToBeUpdated = new List<Transaction_Processor__c>();
        Map<String,Contact> matchedContactMap = new Map<String,Contact>();
        for(Transaction_Processor__c obj : newObjects){
            if(obj.Source__c == Constants.MTicket && obj.Customer__c == NULL){
                Core3IdList.add(obj.Core3_ID__c);
                recordsToBeUpdated.add(obj);
            }
            
            if(obj.Source__c == Constants.Web && obj.Customer__c == NULL){
                WebEmailList.add(obj.Email__c);
                if(obj.Web_Reference_ID__c != '0')
                    WebReferenceList.add(obj.Web_Reference_ID__c);
                recordsToBeUpdated.add(obj);
            }
        }
        String queryString = 'select Id, MTickets_Reference_ID__c, Email__c, Web_Reference_ID__c from Contact ';
        List<Contact> contactList = new List<Contact>();
        List<String> tempStringList = new List<String>();
        
        if(!Core3IdList.isEmpty()){
            tempStringList = new List<String>();
            for(String str : Core3IdList)
                tempStringList.add('\''+str+'\'');
            queryString += 'where MTickets_Reference_ID__c IN ('+String.join(tempStringList, ',')+')';
        }
		if(!WebEmailList.isEmpty()){
            tempStringList = new List<String>();
            for(String str : WebEmailList)
                tempStringList.add('\''+str+'\'');
            queryString += !Core3IdList.isEmpty() ? ' OR Email__c IN ('+String.join(tempStringList, ',')+')' : 'where Email__c IN ('+String.join(tempStringList, ',')+')';
        }
        if(!WebReferenceList.isEmpty()){
            tempStringList = new List<String>();
            for(String str : WebReferenceList)
                tempStringList.add('\''+str+'\'');
            queryString += !Core3IdList.isEmpty() ? (!WebEmailList.isEmpty() ? ' OR Web_Reference_ID__c IN ('+String.join(tempStringList, ',')+')' : ' OR Web_Reference_ID__c IN ('+String.join(tempStringList, ',')+')' ) : (!WebEmailList.isEmpty() ? ' OR Web_Reference_ID__c IN ('+String.join(tempStringList, ',')+')' : 'where Web_Reference_ID__c IN ('+String.join(tempStringList, ',')+')');
            //queryString += !Core3IdList.isEmpty() || !WebEmailList.isEmpty() ? ' OR Web_Reference_ID__c IN ('+String.join(tempStringList, ',')+')' : ' Web_Reference_ID__c IN ('+String.join(tempStringList, ',')+')';
        }
        system.debug('queryString : '+queryString);
        if(!Core3IdList.isEmpty() || !WebEmailList.isEmpty() || !WebReferenceList.isEmpty())
        	contactList = DataBase.query(queryString);
        //for(Contact obj : [select Id, MTickets_Reference_ID__c, Email__c, Web_Reference_ID__c from Contact where MTickets_Reference_ID__c IN :Core3IdList OR Email__c IN :WebEmailList OR Web_Reference_ID__c IN :WebReferenceList]){
        for(Contact obj : contactList){
            if(obj.MTickets_Reference_ID__c != NULL && !matchedContactMap.containsKey(obj.MTickets_Reference_ID__c))
                matchedContactMap.put(obj.MTickets_Reference_ID__c, obj);
            
            if(obj.Email__c != NULL || (obj.Web_Reference_ID__c != NULL && obj.Web_Reference_ID__c != '0')){
                //Create a composite key of Email__c-Web_Reference_ID__c to uniquely identify a transaction processor record
                String compositeKey = '';
                if(obj.Email__c != NULL){
                    compositeKey = obj.Email__c;
                    matchedContactMap.put(obj.Email__c, obj);
                    matchedContactMap.put(obj.Email__c.toLowerCase(), obj);
                }
                if(obj.Web_Reference_ID__c != NULL && obj.Web_Reference_ID__c != '0'){
                    compositeKey += !String.isBlank(compositeKey) ? '-'+obj.Web_Reference_ID__c : obj.Web_Reference_ID__c;
                    matchedContactMap.put(obj.Web_Reference_ID__c, obj);
                }
                if(!matchedContactMap.containsKey(compositeKey))
                    matchedContactMap.put(compositeKey, obj);
            }
        }
        
        //Map contact to Transaction Processor record
        Map<String,Contact> contactRecordToBeCreated = new Map<String,Contact>();
        Map<String,Contact> contactRecordToBeUpdated = new Map<String,Contact>();
        Map<String, Contact> createdMap = new Map<String,Contact>();
        for(Transaction_Processor__c obj : recordsToBeUpdated){
            if(obj.Source__c == Constants.MTicket){
                obj.Customer__c = matchedContactMap.containsKey(obj.Core3_ID__c) ? matchedContactMap.get(obj.Core3_ID__c).Id : NULL;
            }
            else{
                String compositeKey = obj.Email__c != NULL ? obj.Email__c : '';
                if(obj.Web_Reference_ID__c != NULL && obj.Web_Reference_ID__c != '0') //0 indicates a guest user
                    compositeKey += !String.isBlank(compositeKey) ? '-'+obj.Web_Reference_ID__c : obj.Web_Reference_ID__c;
                
                if(matchedContactMap.containsKey(compositeKey)){ //Indicates a matching contact is found for both web reference and email
                    obj.Customer__c = matchedContactMap.get(compositeKey).Id;
                }
                else if(obj.Web_Reference_ID__c != NULL && obj.Web_Reference_ID__c != '0' && matchedContactMap.containsKey(obj.Web_Reference_ID__c)){ //indicates only a match with web reference
                    obj.Customer__c = matchedContactMap.get(obj.Web_Reference_ID__c).Id;
                    if(!contactRecordToBeUpdated.containsKey(obj.Customer__c))
                        contactRecordToBeUpdated.put(obj.Customer__c,new Contact(Id=obj.Customer__c,Email__c=obj.Email__c != NULL ? obj.Email__c : NULL));
                    else{
                        Contact tempCnt = contactRecordToBeUpdated.get(obj.Customer__c);
                        tempCnt.put('Email__c',obj.Email__c != NULL ? obj.Email__c : NULL);
                    }
                }
                else if(obj.Email__c != NULL && matchedContactMap.containsKey(obj.Email__c)){ //Indicates only a match with email
                    obj.Customer__c = matchedContactMap.get(obj.Email__c).Id;
                    if(!contactRecordToBeUpdated.containsKey(obj.Customer__c))
                        contactRecordToBeUpdated.put(obj.Customer__c,new Contact(Id=obj.Customer__c,Web_Reference_ID__c=obj.Web_Reference_ID__c != NULL ? obj.Web_Reference_ID__c : NULL));
                    else{
                        Contact tempCnt = contactRecordToBeUpdated.get(obj.Customer__c);
                        tempCnt.put('Web_Reference_ID__c',obj.Web_Reference_ID__c != NULL ? obj.Web_Reference_ID__c : NULL);
                    }
                } 
                else{ //Need to create a new contact from Transaction processor record
                    Account FirstBusAccount = [select Id, Name from Account where Name = 'FirstBus Account'];
                    Contact newContact = new Contact(FirstName__c=obj.First_Name__c,
                                                     LastName=obj.Last_Name__c,
                                                     Town__c=obj.Mailing_City__c,
                                                     MailingCountry=obj.Mailing_Country__c,
                                                     MailingPostalCode=obj.Mailing_Postal_Code__c,
                                                     MailingStreet=obj.Mailing_Street__c,
                                                     Del_Town__c=obj.Other_City__c,
                                                     OtherCountry=obj.Other_Country__c,
                                                     OtherPostalCode=obj.Other_Postal_Code__c,
                                                     OtherStreet=obj.Other_Street__c,
                                                     AccountId=FirstBusAccount.Id,
                                                     Source_System_Created_Date__c=obj.Purchase_Date__c,
                                                     Salutation=obj.Salutation__c,
                                                     Web_Reference_ID__c=obj.Web_Reference_ID__c,
                                                     Email__c=obj.Email__c,
                                                     Email=!String.isBlank(obj.Email__c) && obj.Email__c.length()<80 ? obj.Email__c : NULL,
                                                     Source__c=obj.Source__c,
                                                     MobilePhone=!String.isBlank(obj.Phone__c) && (obj.Phone__c.startsWith('07') || obj.Phone__c.startsWith('08')) ? obj.Phone__c : NULL,
                                                     Phone=!String.isBlank(obj.Phone__c) && (obj.Phone__c.startsWith('01') || obj.Phone__c.startsWith('02') || obj.Phone__c.startsWith('44') || obj.Phone__c.startsWith('+44')) ? obj.Phone__c : NULL);
                    
                    contactRecordToBeCreated.put(compositeKey,newContact);
                }
            }
            
        }
        if(!contactRecordToBeCreated.isEmpty() && Schema.sObjectType.Contact.isCreateable()){
            try{
                Database.SaveResult[] saveResultList = Database.insert(contactRecordToBeCreated.values(), false);
                List<Id> contactsCreated = new List<Id>();
                for (Database.SaveResult sr : saveResultList) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        contactsCreated.add(sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account fields that affected this error: ' + err.getFields());
                        }
                    }
                }
                //Fetch result from backend and populate in map
                for(Contact obj : [select Id, Email__c, Web_Reference_ID__c from Contact where Id IN :contactsCreated]){
                    String compositeKey = obj.Email__c != NULL ? obj.Email__c : '';
                    if(obj.Web_Reference_ID__c != NULL)
                        compositeKey = !String.isBlank(compositeKey) ? '-'+obj.Web_Reference_ID__c : obj.Web_Reference_ID__c;
                    if(!createdMap.containsKey(compositeKey))
                        createdMap.put(compositeKey,obj);
                }
                
                //Update transaction records for which new contacts are created
                for(Transaction_Processor__c obj : recordsToBeUpdated){
                    String compositeKey = obj.Email__c != NULL ? obj.Email__c : '';
                    if(obj.Web_Reference_ID__c != NULL)
                        compositeKey = !String.isBlank(compositeKey) ? '-'+obj.Web_Reference_ID__c : obj.Web_Reference_ID__c;                    
                    if(obj.Customer__c == NULL && createdMap.containsKey(compositeKey)){
                        obj.Customer__c= createdMap.get(compositeKey).Id;
                    }
                    
                }
            }
            catch(Exception e){
                system.debug('EXCEPTION : '+e.getMessage());
            }
        }
        if(!contactRecordToBeUpdated.isEmpty()){
            try{
                upsert contactRecordToBeUpdated.values();
            }
            catch(Exception e){
                system.debug('EXCEPTION : '+e.getMessage());
            }
        }
    }
    
    public List<Transaction_Processor__c> relateTransactionToDevices(List<Transaction_Processor__c> newObjects){
        List<Transaction_Processor__c> returnList = new List<Transaction_Processor__c>();
        List<String> C3UserId = new List<String>();
        Map<String, Transaction_Processor__c> C3ToTPMap = new Map<String, Transaction_Processor__c>();
        Map<String, Device_ID__c> C3ToDevMap = new Map<String, Device_ID__c>();
        LisT<Device_Id__c> devicesToBeCreated = new List<Device_Id__c>();
        List<Transaction_Processor__c> TP_record_wo_Device = new List<Transaction_Processor__c>();
        List<String> newC3Ids = new List<String>();
        for(Transaction_Processor__c obj : newObjects){
            if(obj.Core3_Device_Id__c != NULL && obj.Device_Id__c == NULL){
                C3UserId.add(obj.Core3_Device_Id__c);
                C3ToTPMap.put(obj.Core3_Device_Id__c, obj);
            }
            else{
                returnList.add(obj);
            }
        }
        
        for(Device_ID__c obj : [SELECT Id, Name, C3_User_Id__c, OS__c, AppVersion__c, Device__c, Customer__c FROM Device_ID__c where Device__c IN :C3UserId]){
            C3ToDevMap.put(obj.Device__c, obj); //C3_User_Id__c
        }
        
        for(String c3Id : C3ToTPMap.keySet()){
            Transaction_Processor__c tempObj = C3ToTPMap.get(c3Id);
            if(C3ToDevMap.containsKey(c3Id)){
                tempObj.Device_Id__c = C3ToDevMap.get(c3Id).Id;
                returnList.add(tempObj);
            }
            else{
                //Create Device Id record
                TP_record_wo_Device.add(tempObj);
                devicesToBeCreated.add(new Device_Id__c(C3_User_Id__c=tempObj.Core3_ID__c,Device__c=tempObj.Core3_Device_Id__c));
                newC3Ids.add(tempObj.Core3_ID__c);
            }
        }
        
        if(!devicesToBeCreated.isEmpty()){
            try{
                insert devicesToBeCreated;
                for(Device_ID__c obj : [select Id, C3_User_Id__c from Device_Id__c where C3_User_Id__c IN :newC3Ids]){
                    C3ToDevMap.put(obj.C3_User_Id__c, obj);
                }
                for(Transaction_Processor__c obj : TP_record_wo_Device){
                    if(C3ToDevMap.containsKey(obj.Core3_ID__c)){
                        obj.Device_Id__c = C3ToDevMap.get(obj.Core3_ID__c).Id;
                        returnList.add(obj);
                    }
                }
            }
            catch(Exception e){
                system.debug('EXCEPTION : '+e.getMessage()+' | STACKTRACE : '+e.getStackTraceString());
            }
        }
        return returnList;
    }
}