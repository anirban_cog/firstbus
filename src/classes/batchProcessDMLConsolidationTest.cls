@istest
public class batchProcessDMLConsolidationTest {
    static TestDataInsert tdi = new TestDataInsert();
    
    static testMethod void accountTest() {
        List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
        CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
        insert CSObjList;

    	Account[] acc = (Account[])(tdi.prepareSObject(new TestDataJsonLibrary.Standard().jsonMap.get('ACCOUNT'), null));    	
    	Test.startTest();
    		Database.executebatch(new batchProcessDMLConsolidation(acc, 'INSERT'), 1);
    	test.stopTest();
    	system.assert([Select id From Account].size() > 0);
    }
    
}