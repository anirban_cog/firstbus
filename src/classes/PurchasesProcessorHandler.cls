/*************************************************************
* Author: Ayan Hore
* Created Date: 11/21/17
* Version1: B-16760, B-16759
* Details: Handler to be invoked by PurchasesProcessorTrigger on Purchases_Processor__c object 
* Purpose:Trigger handler on Purchases Processor object
**************************************************************/
public class PurchasesProcessorHandler extends TriggerBaseHandler{
    
    public PurchasesProcessorLogic purchaseLogic;
    
    public PurchasesProcessorHandler(){
        purchaseLogic = new PurchasesProcessorLogic();
    }
    
    public override void beforeInsert(SObject[] newObj, CommitHandler ch){
        List<Purchases_Processor__c> updatedValues = new List<Purchases_Processor__c>();
        updatedValues = purchaseLogic.setFlagValues(newObj,new Map<id,SObject>(),true);
        purchaseLogic.relatedPurchasesToTransaction(updatedValues);
    }
    
    public override void beforeUpdate(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){ 
        List<Purchases_Processor__c> updatedValues = new List<Purchases_Processor__c>();
        updatedValues = purchaseLogic.setFlagValues(newObj,oldmap,false);
        purchaseLogic.relatedPurchasesToTransaction(updatedValues); 
    }
}