public virtual class TriggerBaseHandler implements ITrigger{ 
    
    public virtual void beforeInsert(SObject[] newObj, CommitHandler ch){ 
        
    } 
    public virtual void afterInsert(SObject[] newObj, Map<id,SObject> newmap, CommitHandler ch){ 
        
    } 
    public virtual void beforeUpdate(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){ 
        
    } 
    public virtual void afterUpdate(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){ 
        
    } 
    public virtual void beforeDelete(SObject[] oldObj, Map<id,SObject> oldmap, CommitHandler ch){ 
        
    } 
    public virtual void afterDelete(SObject[] oldObj, Map<id,SObject> oldmap, CommitHandler ch){ 
        
    } 
    public virtual void beforeUnDelete(SObject[] oldObj, Map<id,SObject> oldmap, CommitHandler ch){ 
        
    } 
    public virtual void afterUnDelete(SObject[] oldObj, SObject[] newObj, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){ 
        
    } 
}