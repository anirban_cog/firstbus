@isTest
public class FBIndividualEmailResultCleanupBatchTest {
    static TestDataComplexFunctions testDataComplexFunctions = new TestDataComplexFunctions();
    static final String LastName = 'LastName';
    static final String StringType = 'String';
    @isTest
    public static void testMethod1(){
        List<ContactCleanupSettings__c> cs = new List<ContactCleanupSettings__c>();
        cs.add(new ContactCleanupSettings__c(Name='DateRangeForCaseClosedDate',Value__c='2'));
        cs.add(new ContactCleanupSettings__c(Name='DateRangeForContactCleanupOperation',Value__c='1'));
        cs.add(new ContactCleanupSettings__c(Name='DateRangeForContactEmailOpened',Value__c='2'));
        cs.add(new ContactCleanupSettings__c(Name='DateRangeForIndividualEmailResult',Value__c='-1')); //in this also it should be set to 0 to get coverage!
        insert cs;
		
        DateTime dt = DateTime.now();
        
        List<KeyValue> contactKV = new List<KeyValue>();
        contactKV.add(new KeyValue(LastName,'contact - 1',StringType));
        contactKV.add(new KeyValue('MobilePhone','12345',StringType));
        contactKV.add(new KeyValue('Source__c','Mticket',StringType));
        contactKV.add(new KeyValue('Duplicate__c','false','BOOLEAN'));
        contactKV.add(new KeyValue('Original_Source__c','',StringType));
        contactKV.add(new KeyValue('Email__c','test@test.com',StringType));
        contactKV.add(new KeyValue('Town__c','2abcdefghijklmnopqrstuvwxyzabcdefghijklmn',StringType));
        contactKV.add(new KeyValue('FirstName__c','a2bcdefghijklmnopqrstuvwxyzabcdefghijklmnabcdefghijklmnopqrstuvwxyzabcde',StringType));
        contactKV.add(new KeyValue('Del_Town__c','2delivery_town_abcdef',StringType)); 
		contactKV.add(new KeyValue('Email_Opened__c',String.valueOf(dt),'DATETIME'));            
        
        Contact con1 = testDataComplexFunctions.insertContact(contactKV,null);
        system.debug('con1 : '+con1);
        
        String str = null;
        List<KeyValue> ierKV = new List<KeyValue>();
        ierKV.add(new KeyValue('et4ae5__Contact__c',con1.id,'ID'));
        //ierKV.add(new KeyValue('et4ae5__DateOpened__c',str,'DATETIME'));
        ierKV.add(new KeyValue('et4ae5__Opened__c','false','BOOLEAN'));
        ierKV.add(new KeyValue('et4ae5__Email__c','abc@xyz.com','String'));
        et4ae5__IndividualEmailResult__c ier = testDataComplexFunctions.insertIndividualEmailResult(ierKV,null);
		system.debug('ier :'+ier);
        system.debug('ier created : '+[select id,et4ae5__Email__c, et4ae5__Contact__c, et4ae5__Opened__c, et4ae5__DateOpened__c from et4ae5__IndividualEmailResult__c limit 1]);
        system.debug([SELECT et4ae5__Email__c, et4ae5__DateOpened__c, et4ae5__Contact__c, et4ae5__Clicked__c, Id, et4ae5__Opened__c FROM et4ae5__IndividualEmailResult__c ]);
        
        Test.startTest();
        	Database.executeBatch(new FBIndividualEmailResultCleanupBatch());
        Test.stopTest();
    }
    
    @isTest
    public static void testMethod2(){
        
        Test.startTest();
        Database.executeBatch(new FBIndividualEmailResultCleanupBatch());
        Test.stopTest();
    }
    
}