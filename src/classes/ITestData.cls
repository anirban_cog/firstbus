public interface ITestData {
    List<sObject> prepareSObject(String jsonStr, KeyValue[] kVals); 
}