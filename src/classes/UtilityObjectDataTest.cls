@istest
public class UtilityObjectDataTest {

	static TestDataComplexFunctions tdc = new TestDataComplexFunctions();

	static testMethod void setObjDataTest(){
		Account acc = tdc.insertAccount(null, null);
		test.starttest();			 
			UtilityObjectData.setObjData(acc, new List<KeyValue>{new KeyValue('Name','Test Account Data','String')});
			system.assert(acc.Name == 'Test Account Data');
		test.stoptest();
	}

	static testMethod void setFieldValTest(){
		Account acc = tdc.insertAccount(null, null);
				
		test.starttest();
			
			UtilityObjectData.setFieldVal(acc, new KeyValue('PersonHasOptedOutOfEmail','true','BOOLEAN'));
			system.assert(acc.IsPersonAccount);
			
			UtilityObjectData.setFieldVal(acc, new KeyValue('AnnualRevenue','2','INTEGER'));
			system.assert(acc.AnnualRevenue == 2);
			
			UtilityObjectData.setFieldVal(acc, new KeyValue('AnnualRevenue','10','LONG'));
			system.assert(acc.AnnualRevenue == 10);
			
			UtilityObjectData.setFieldVal(acc, new KeyValue('AnnualRevenue','10','DOUBLE'));
			system.assert(acc.AnnualRevenue == 10);
			
	/*		UtilityObjectData.setFieldVal(acc, new KeyValue('Genre__c',string.valueof(gre.id),'ID'));
			system.assert(acc.Genre__c == gre.id);
		*/	
			UtilityObjectData.setFieldVal(acc, new KeyValue('BillingCity','London','STRING'));
			system.assert(acc.BillingCity == 'London');
			
		test.stoptest();
	}

}