/*************************************************************
* Author: Paramita Roy Halder
* Created Date: 11/14/17
* Version1: B-16811
* Details: Logic class to be invoked by ContactHandler class 
* Purpose: Trigger logic class
**************************************************************/
public class ContactLogic
{
    public void onBeforeInsert(Contact[] newContactObjects, CommitHandler ch)
    {
        system.debug('inside ContactLogic --> onBeforeInsert');
        system.debug('newContactObjects --> ' + newContactObjects);
        
        Set<String> conPhoneList = new Set<String>();
        Set<String> conEmailList = new Set<String>();
        String[] arrExistingchannel = null;
        List<Contact> lstOriginalContact = new List<Contact>();
        Map<Id, Contact> tempUpdatedContact = new Map<Id, Contact>();
        
        Map<String,Integer> contactEmailPositionMap = new Map<String,Integer>();
        Map<String, Contact> contactMap = new Map<String,Contact>();
        
        //fetching default account
        List<Account> lstAccount = [SELECT Id,Name FROM Account WHERE Name = 'FirstBus Account' LIMIT 1];
        Account defaultAccount = null;
        if(!lstAccount.isEmpty())
            defaultAccount = lstAccount.get(0);
        
        for(Integer i = 0 ; i < newContactObjects.size() ; i++)
        {
            Contact contact = newContactObjects[i];
            
            if(contact != null && contact.MobilePhone != null && contact.MobilePhone != '')
                conPhoneList.add(contact.MobilePhone);
            
            if(contact != null && contact.Email__c != null && contact.Email__c != '')
                conEmailList.add(contact.Email__c);
            
            //handle WIFI data load for multiple contact having same Email__c in the bulk CSV. 
            //Consider the last last contactin the csv having the same Email__c and rest should be discarded.
            if(newContactObjects.size() > 1)
            {
                String conCustEmail = (contact.Email__c!= null && contact.Email__c!='')?contact.Email__c.trim().toLowerCase():'';
                if(contactEmailPositionMap.containsKey(conCustEmail) && contactMap.containsKey(conCustEmail))
                {
                    Integer position = contactEmailPositionMap.get(conCustEmail);
                    ((Contact)newContactObjects[position]).Email__c.addError('Duplicate contact');
                    contactEmailPositionMap.put(conCustEmail,i); 
                    contactMap.put(conCustEmail,contact); 
                }
                else
                {
                    contactMap.put(conCustEmail,contact); 
                    contactEmailPositionMap.put(conCustEmail,i);               
                }
            }
                        
            //populate first name, Email__c and town standard field by the custom fields
            contactStdAndCustomFieldMapping(contact);
        }
        system.debug('contactMap --> ' + contactMap);
        List<Contact> lstContactObjects = null;
        if(newContactObjects.size() > 1)
            lstContactObjects = contactMap.values();
        else
            lstContactObjects = newContactObjects;
        system.debug('lstContactObjects --> ' + lstContactObjects);
        system.debug('conPhoneList --> ' + conPhoneList);
        system.debug('conEmailList --> ' + conEmailList);
        
        List<Contact> lstContact = [Select CreatedDate, Organisation_1__c,Organisation_2__c,Organisation_3__c,Organisation_4__c,Organisation_5__c,Email__c,FirstName__c,Town__c,Del_Town__c,Id,FirstName, MailingCity, OtherCity, Email,MobilePhone, Channels__c, Last_Interaction__c, Original_Source__c, Original_Web_Source__c, Source__c, Web_Reference_ID__c, MTickets_Reference_ID__c from Contact where (Email__c IN: conEmailList OR MobilePhone in: conPhoneList) AND Duplicate__c = false AND IsPersonAccount = false AND (MobilePhone != NULL OR Email__c != NULL)];
        system.debug('lstContact --> ' + lstContact);
        
        List<Id> lstContactId = new List<Id>();
        for(Contact con : lstContact)
        {
            lstContactId.add(con.Id);
        }
        system.debug('lstContactId --> ' + lstContactId);
        List<ContactHistory> lstConHistory = [SELECT Id, ContactId, OldValue, NewValue, CreatedDate FROM ContactHistory WHERE field = 'Like_to_receive_latest_update_and_offer__c' AND ContactId IN : lstContactId ORDER BY CreatedDate DESC NULLS FIRST];
        system.debug('lstConHistory --> ' + lstConHistory);
        Map<Id, ContactHistory> mapConIdVsHist = new Map<Id, ContactHistory>();
        
        for(ContactHistory conHist : lstConHistory)
        {
            if(!mapConIdVsHist.containsKey(conHist.ContactId))
            {
                mapConIdVsHist.put(conHist.ContactId, conHist);
            }
            else 
            {
                ContactHistory hist = mapConIdVsHist.get(conHist.ContactId);
                if(hist.CreatedDate <= conHist.CreatedDate)
                    mapConIdVsHist.put(conHist.ContactId, conHist);
            }
        }
        system.debug('mapConIdVsHist --> ' + mapConIdVsHist);
        
        for(Contact con : lstContactObjects)
        {
            system.debug('inside 1st for loop - con --> ' + con);
            if(con.source__c == null)//if contact is getting created for person account then the source will be null. In that case we are setting default source as "CS Agent"
                con.source__c = 'CS Agent';
            system.debug('inside 1st for loop - con --> ' + con);
            
            //For Wifi contact, populate Source_System_Created_Date__c from WiFi_Entered_Datetime__c
            populateWIFISourceCreatedDate(con);
            populateAccount(con, defaultAccount);
            system.debug('con --> ' + con);
            /*for(Contact existingContact : [Select CreatedDate, Organisation_1__c,Organisation_2__c,Organisation_3__c,Organisation_4__c,Organisation_5__c,Email__c,FirstName__c,Town__c,Del_Town__c,Id,FirstName, MailingCity, OtherCity, Email,MobilePhone, Channels__c, Last_Interaction__c, Original_Source__c, Original_Web_Source__c, Source__c, Web_Reference_ID__c, MTickets_Reference_ID__c, (Select Id, OldValue, NewValue, CreatedDate FROM Histories where field = 'Like_to_receive_latest_update_and_offer__c' ORDER BY CreatedDate DESC NULLS FIRST) from Contact where (Email__c IN: conEmailList OR MobilePhone in: conPhoneList) AND Duplicate__c = false AND IsPersonAccount = false AND (MobilePhone != NULL OR Email__c != NULL)])*/
            for(Contact existingContact : lstContact)
            {
                system.debug('inside 2nd for loop - existingContact --> ' + existingContact);
                
                if((existingContact.MobilePhone == con.MobilePhone && con.MobilePhone != null && con.MobilePhone != '') || (existingContact.Email__c == con.Email__c && con.Email__c != null && con.Email__c != ''))
                {
                    system.debug('con('+ con +') has been matched with existingContact('+ existingContact +')');
                    
                    //setting duplicate flag ands self lookup in the contact
                    Contact existingcon = existingContact;
                    con.Duplicate__c=true;
                    con.Original_Contact__c = existingcon.Id;
                    system.debug('con --> ' + con);  
                    
                    
                    if(tempUpdatedContact.containsKey(existingcon.Id))
                    {
                        existingcon = tempUpdatedContact.get(existingcon.Id);
                    }
                    system.debug('existingcon --> ' + existingcon);
                    
                    //Updating source related fields in original contact | B-16964
                    updateSrcRelatedFieldInOriginalContact(existingcon, con);
                    
                    //commenting for B-16964
                    /*existingcon.Channels__c = getChannel(existingcon.Channels__c, con.Source__c);
                    existingcon.Last_Interaction__c = con.Source__c;
                    if(existingcon.Original_Source__c == null || existingcon.Original_Source__c == '')
                        existingcon.Original_Source__c = con.Source__c;
                    if(con.Source__c == 'Web')
                    {
                        existingcon.Original_Web_Source__c = con.Source__c;
                        
                        system.debug('existingcon.Web_Reference_ID__c --> ' + existingcon.Web_Reference_ID__c);
                        system.debug('con.Web_Reference_ID__c --> ' + con.Web_Reference_ID__c);
                        system.debug('' + (existingcon.Web_Reference_ID__c == null || existingcon.Web_Reference_ID__c == ''));
                        
                        if(existingcon.Web_Reference_ID__c == null || existingcon.Web_Reference_ID__c == '')
                            existingcon.Web_Reference_ID__c = con.Web_Reference_ID__c;
                        
                        //setting the duplicate contact's Web_Reference_ID__c to blank
                        con.Web_Reference_ID__c = '';
                    }
                    if(con.Source__c == FBUtilityClass.SOURCE_MTICKET)
                    {
                        system.debug('existingcon.MTickets_Reference_ID__c --> ' + existingcon.MTickets_Reference_ID__c);
                        system.debug('con.MTickets_Reference_ID__c --> ' + con.MTickets_Reference_ID__c);
                        system.debug('' + (existingcon.MTickets_Reference_ID__c == null || existingcon.MTickets_Reference_ID__c == ''));
                        if(existingcon.MTickets_Reference_ID__c == null || existingcon.MTickets_Reference_ID__c == '')
                            existingcon.MTickets_Reference_ID__c = con.MTickets_Reference_ID__c;
                        
                        //setting the duplicate contact's MTickets_Reference_ID__c to blank
                        con.MTickets_Reference_ID__c = '';
                    }
                    //set the source field of the original contact based on the importance of the source of new contact
                    Map<String, Integer> sourcePrioritymap = getSourcePriorityMap();
                    system.debug('sourcePrioritymap --> ' + sourcePrioritymap);
                    if(sourcePrioritymap != null && !sourcePrioritymap.isEmpty() &&
                        con.Source__c != null && con.Source__c != '' &&
                        existingcon.Source__c != null && existingcon.Source__c != '' &&
                        con.Source__c != existingcon.Source__c)
                    {
                        system.debug('Priority map fetched - inside the for loop ');
                        Integer sourcePriority = sourcePrioritymap.get(con.Source__c);
                        system.debug('sourcePriority --> ' + sourcePriority);
                        Integer originalSourcePriority = sourcePrioritymap.get(existingcon.Source__c);
                        system.debug('originalSourcePriority --> ' + originalSourcePriority);
                        if(originalSourcePriority > sourcePriority )
                            existingcon.Source__c = con.Source__c;
                    }
                    system.debug('existingcon --> ' + existingcon);*/
                    
                    //Set the personal details of duplicate contact to the original contact
                    moveInfoFromDuplicateContact(existingcon, con, mapConIdVsHist);                    
                    
                    //lstOriginalContact.add(existingcon);
                    tempUpdatedContact.put(existingcon.Id, existingcon);
                    system.debug('tempUpdatedContact --> ' + tempUpdatedContact);
                    break;
                }
            }
            
            
            if(!con.Duplicate__c && con.Original_Contact__c == null)
            {
                system.debug('con('+ con +') is not duplicate and has come for the first time');
                if(con.Source__c != null && con.Source__c != '')
                {
                    con.Channels__c = con.Source__c;//always append the source of new contact with the channel of the original contact using a seperator '/'
                    con.Last_Interaction__c = con.Source__c;//always set with the source of new contact
                    con.Original_Source__c = con.Source__c;//will only set for the first time
                    
                    if(con.Source__c == FBUtilityClass.SOURCE_WEB)
                    {
                        con.Original_Web_Source__c = con.Source__c;
                    }
                }
                system.debug('con --> ' + con);
            }
        }
        
        system.debug('tempUpdatedContact --> ' + tempUpdatedContact);
        if(!tempUpdatedContact.isEmpty())//if(tempUpdatedContact.size() > 0)
        {
            lstOriginalContact = tempUpdatedContact.values();
        }
        system.debug('lstOriginalContact --> ' + lstOriginalContact);
        //update lstOriginalContact;
        
        UtilDML_Master.updateObjects(lstOriginalContact);
        
    }
    public void onAfterInsert(Contact[] newContactObjects, Map<id,SObject> newmap, CommitHandler ch)
    {
        system.debug('inside ContactLogic --> onAfterInsert');
    }
    
    public void onBeforeUpdate(Contact[] oldContactObjects, Contact[] newContactObjects, Map<id,SObject> oldmap, Map<id,SObject> newmap, CommitHandler ch)
    {
        system.debug('inside ContactLogic --> onBeforeUpdate');
        system.debug('newContactObjects --> ' + newContactObjects);
        
        //fetching default account
        List<Account> lstAccount = [SELECT Id,Name FROM Account WHERE Name = 'FirstBus Account' LIMIT 1];
        Account defaultAccount = null;
        if(!lstAccount.isEmpty())
            defaultAccount = lstAccount.get(0);
        
        for(Contact contact : newContactObjects)
        {
            //populate first name, Email__c and town standard field by the custom fields
            contactStdAndCustomFieldMapping(contact);
            populateAccount(contact, defaultAccount);
            
            system.debug('contact --> contact');
            String oldOrganization1Value = ((Contact)oldmap.get(contact.Id)).Organisation_1__c;
            system.debug('oldOrganization1Value --> oldOrganization1Value');
            String newOrganization1Value = contact.Organisation_1__c;
            system.debug('newOrganization1Value --> newOrganization1Value');
            if(newOrganization1Value != oldOrganization1Value)
            {
                contact.Organisation_1__c = oldOrganization1Value;
                system.debug('contact --> contact');
                manupulateOrganizationFields(contact, newOrganization1Value);
                system.debug('contact --> contact');
            }
        }
        system.debug('newContactObjects --> ' + newContactObjects);
    }
    
    public void onAfterUpdate(Contact[] oldContactObjects, Contact[] newContactObjects, Map<id,SObject> oldmap, Map<id,SObject> newmap, CommitHandler ch)
    {
        system.debug('inside ContactLogic --> onAfterUpdate');
    }
    
    public void onBeforeDelete(Contact[] oldContactObjects, Map<id,SObject> oldmap, CommitHandler ch)
    {
        system.debug('inside ContactLogic --> onBeforeDelete');
    } 
    
    public void onAfterDelete(Contact[] oldContactObjects, Map<id,SObject> oldmap, CommitHandler ch)
    {
        system.debug('inside ContactLogic --> onAfterDelete');
    } 
    
    public void onBeforeUnDelete(Contact[] oldContactObjects, Map<id,SObject> oldmap, CommitHandler ch)
    {
        system.debug('inside ContactLogic --> onBeforeUnDelete');
    } 
    
    public void onAfterUnDelete(Contact[] oldContactObjects,  Contact[] newContactObjects, Map<id,SObject> oldmap, Map<id,SObject> newmap, CommitHandler ch)
    {
        system.debug('inside ContactLogic --> onAfterUnDelete');
    }
    
    //This function will push all source related field values from Duplicate to original contact | US - B-16964
    public static void updateSrcRelatedFieldInOriginalContact(Contact existingcon, Contact con)
    {
        if(existingcon!=null && con!=null)
        {
            existingcon.Channels__c = getChannel(existingcon.Channels__c, con.Source__c);
            existingcon.Last_Interaction__c = con.Source__c;
            if(existingcon.Original_Source__c == null || existingcon.Original_Source__c == '')
                existingcon.Original_Source__c = con.Source__c;
            if(con.Source__c == 'Web')
            {
                existingcon.Original_Web_Source__c = con.Source__c;
                
                system.debug('existingcon.Web_Reference_ID__c --> ' + existingcon.Web_Reference_ID__c);
                system.debug('con.Web_Reference_ID__c --> ' + con.Web_Reference_ID__c);
                system.debug('' + (existingcon.Web_Reference_ID__c == null || existingcon.Web_Reference_ID__c == ''));
                
                if(existingcon.Web_Reference_ID__c == null || existingcon.Web_Reference_ID__c == '')
                    existingcon.Web_Reference_ID__c = con.Web_Reference_ID__c;
                
                //setting the duplicate contact's Web_Reference_ID__c to blank
                con.Web_Reference_ID__c = '';
            }
            if(con.Source__c == FBUtilityClass.SOURCE_MTICKET)
            {
                system.debug('existingcon.MTickets_Reference_ID__c --> ' + existingcon.MTickets_Reference_ID__c);
                system.debug('con.MTickets_Reference_ID__c --> ' + con.MTickets_Reference_ID__c);
                system.debug('' + (existingcon.MTickets_Reference_ID__c == null || existingcon.MTickets_Reference_ID__c == ''));
                if(existingcon.MTickets_Reference_ID__c == null || existingcon.MTickets_Reference_ID__c == '')
                    existingcon.MTickets_Reference_ID__c = con.MTickets_Reference_ID__c;
                
                //setting the duplicate contact's MTickets_Reference_ID__c to blank
                con.MTickets_Reference_ID__c = '';
            }
            //set the source field of the original contact based on the importance of the source of new contact
            Map<String, Integer> sourcePrioritymap = getSourcePriorityMap();
            system.debug('sourcePrioritymap --> ' + sourcePrioritymap);
            if(sourcePrioritymap != null && !sourcePrioritymap.isEmpty() &&
                con.Source__c != null && con.Source__c != '' &&
                existingcon.Source__c != null && existingcon.Source__c != '' &&
                con.Source__c != existingcon.Source__c)
            {
                system.debug('Priority map fetched - inside the for loop ');
                Integer sourcePriority = sourcePrioritymap.get(con.Source__c);
                system.debug('sourcePriority --> ' + sourcePriority);
                Integer originalSourcePriority = sourcePrioritymap.get(existingcon.Source__c);
                system.debug('originalSourcePriority --> ' + originalSourcePriority);
                if(originalSourcePriority > sourcePriority )
                    existingcon.Source__c = con.Source__c;
            }
            system.debug('existingcon --> ' + existingcon);
        }       
    }
    
    public static Map<String, Integer> getSourcePriorityMap()
    {
        Map<String, Integer> sourcePriorityMap = new Map<String, Integer>();
        Map<String, SourcePriority__c> mapSourcePriority = SourcePriority__c.getAll();
        if(mapSourcePriority!=null && !mapSourcePriority.isEmpty())
        {
            Set<String> setSource = mapSourcePriority.keySet();
            for(String src : setSource)
                sourcePriorityMap.put(src, (Integer)(((SourcePriority__c)mapSourcePriority.get(src)).Priority__c));
        }
        system.debug('sourcePriorityMap - ' + sourcePriorityMap);
        return sourcePriorityMap;
    }
    
    //Generate the Channel string for the original contact when the new contact has been identified as duplicate
    public static String getChannel(String existingChannels, String newSource)
    {
        String channel = '';
        if(existingChannels != null && existingChannels != '')
        {
            List<String> lstExistingChannels = existingChannels.trim().split(';');
            system.debug('inside getChannel function - lstExistingChannels --> ' + lstExistingChannels);
            Set<String> setExistingChannels = new Set<String>(lstExistingChannels);
            system.debug('inside getChannel function - setExistingChannels --> ' + setExistingChannels);
            
            if(setExistingChannels.contains(newSource))
                channel =  existingChannels;
            else
                channel =  (existingChannels + ';' + newSource);
        }
        else
            channel =  newSource;
        system.debug('inside getChannel function - channel --> ' + channel);
        
        return channel;
    }
    
    private void contactStdAndCustomFieldMapping(Contact contact)
    {
        //populate first name, Email__c and town standard field by the custom fields
        if(contact.Email__c != null && contact.Email__c != '')
        {
            if(contact.Email__c.length() > 80)
            {
                //contact.Email = contact.Email__c.substring(0,80);//truncate contact.Email__c to 80 character and populate
                contact.Email = '';
            }
            else
                contact.Email = contact.Email__c;
        }
        
        if(contact.FirstName__c != null && contact.FirstName__c != '')
        {
            if(contact.FirstName__c.length() > 40)
                contact.FirstName = contact.FirstName__c.substring(0,40);//truncate contact.FirstName__c to 40 character and populate
            else
                contact.FirstName = contact.FirstName__c;
        }
        
        if(contact.Town__c != null && contact.Town__c != '')
        {
            if(contact.Town__c.length() > 40)
                contact.MailingCity = contact.Town__c.substring(0,40);//truncate contact.Town__c to 40 character and populate
            else
                contact.MailingCity = contact.Town__c;
        }
        
        if(contact.Del_Town__c != null && contact.Del_Town__c != '')
        {
            if(contact.Del_Town__c.length() > 40)
                contact.OtherCity = contact.Del_Town__c.substring(0,40);//truncate contact.Del_Town__c to 40 character and populate
            else
                contact.OtherCity = contact.Del_Town__c;
        }
    }
    
    private void moveInfoFromDuplicateContact(Contact existingcon, Contact con, Map<Id, ContactHistory> mapConIdVsHist)
    {
        //Set the personal details of duplicate contact to the original contact
        existingcon.FirstName = con.FirstName;
        existingcon.LastName = con.LastName;
        existingcon.Salutation = con.Salutation;
        existingcon.MailingStreet = con.MailingStreet;
        existingcon.MailingCity = con.MailingCity;
        existingcon.MailingCountry = con.MailingCountry;
        existingcon.MailingPostalCode = con.MailingPostalCode;
        existingcon.Email__c = con.Email__c;
        existingcon.Email = con.Email;
        existingcon.MobilePhone = con.MobilePhone;
        existingcon.Phone = con.Phone;
        
        if(con.OtherStreet != null && con.OtherStreet != '')
            existingcon.OtherStreet = con.OtherStreet;
        if(con.OtherCity != null && con.OtherCity != '')
            existingcon.OtherCity = con.OtherCity;
        if(con.OtherCountry != null && con.OtherCountry != '')
            existingcon.OtherCountry = con.OtherCountry;
        if(con.OtherPostalCode != null && con.OtherPostalCode != '')
            existingcon.OtherPostalCode = con.OtherPostalCode;
        
        //Additional fields coming from wifi load
        if(con.Age_Group__c != null && con.Age_Group__c != '')
            existingcon.Age_Group__c = con.Age_Group__c;
        
        if(con.Reason_for_use__c != null && con.Reason_for_use__c != '')
            existingcon.Reason_for_use__c = con.Reason_for_use__c;
        
        if(con.Frequency_of_Use__c != null && con.Frequency_of_Use__c != '')
            existingcon.Frequency_of_Use__c = con.Frequency_of_Use__c;
        
        Boolean canUpdateReceiveOfferField = isReceiveOfferFieldUpdatable(existingcon, con, mapConIdVsHist);        
        system.debug('canUpdateReceiveOfferField - ' + canUpdateReceiveOfferField);
        system.debug('con.Like_to_receive_latest_update_and_offer__c  --> ' + con.Like_to_receive_latest_update_and_offer__c );
        if(canUpdateReceiveOfferField && con.Like_to_receive_latest_update_and_offer__c != null)
        {
            existingcon.Like_to_receive_latest_update_and_offer__c = con.Like_to_receive_latest_update_and_offer__c;
        }
        system.debug('existingcon  --> ' + existingcon );
        
        //If the existing organisation field is empty this will be updated with the incoming value
        //if the existing organisation has a value but is different to the incoming value then the next inline 
        //organisation field will be populated with the incoming value 
        //B-16952
        manupulateOrganizationFields(existingcon, con.Organisation_1__c);           
    }
    
    private void manupulateOrganizationFields(Contact contact, String newOrganizationValue)
    {           
        if (contact.Organisation_1__c != null && contact.Organisation_1__c != newOrganizationValue){
            if (contact.Organisation_2__c != null && contact.Organisation_2__c != newOrganizationValue){
                if (contact.Organisation_3__c != null && contact.Organisation_3__c != newOrganizationValue){
                    if (contact.Organisation_4__c != null && contact.Organisation_4__c != newOrganizationValue){
                        contact.Organisation_5__c = newOrganizationValue;
                    }
                    else{
                        contact.Organisation_4__c = newOrganizationValue;
                    }
                }
                else{
                    contact.Organisation_3__c = newOrganizationValue;
                }
            }
            else{
                contact.Organisation_2__c = newOrganizationValue;
            }
        }
        else{
            contact.Organisation_1__c = newOrganizationValue;
        }//End B-16952
    }
    
    private boolean isReceiveOfferFieldUpdatable(Contact existingcon, Contact con, Map<Id, ContactHistory> mapConIdVsHist)
    {
        Boolean canUpdateReceiveOfferField = true;
        //List<ContactHistory> contactHistories = existingcon.Histories;
        //system.debug('Existing contact\'s history --> contactHistories - ' + contactHistories);
        ContactHistory conHist = mapConIdVsHist.get(existingcon.Id);
        system.debug('Existing contact\'s history --> conHist - ' + conHist);

        if(con.Source__c == FBUtilityClass.SOURCE_WIFI)
        {
            DateTime WifiContactCreatedDate = con.Source_System_Created_Date__c;
            system.debug('WifiContactCreatedDate - ' + WifiContactCreatedDate);
            //if(contactHistories != null && contactHistories.size() > 0)
            if(conHist != null)
            {
                
                ContactHistory latestContactHistory = conHist;//contactHistories.get(0);
                system.debug('latestContactHistory - ' + latestContactHistory);
                DateTime latestHistoryUpdateDate = latestContactHistory.CreatedDate;    
                system.debug('latestHistoryUpdateDate - ' + latestHistoryUpdateDate);
                system.debug('(latestHistoryUpdateDate > WifiContactCreatedDate) - ' + (latestHistoryUpdateDate > WifiContactCreatedDate));
                if(latestHistoryUpdateDate > WifiContactCreatedDate)
                    canUpdateReceiveOfferField = false;
            }
            else
            {
                system.debug('existingcon.CreatedDate - ' + existingcon.CreatedDate);
                system.debug('(existingcon.CreatedDate > WifiContactCreatedDate) - ' + (existingcon.CreatedDate > WifiContactCreatedDate));
                if(existingcon.CreatedDate > WifiContactCreatedDate)
                    canUpdateReceiveOfferField = false;
            }
        }
        system.debug('canUpdateReceiveOfferField - ' + canUpdateReceiveOfferField);
        return canUpdateReceiveOfferField;
    }
    
    //For Wifi contact, populate Source_System_Created_Date__c from WiFi_Entered_Datetime__c
    //WiFi_Entered_Datetime__c field is text field and will contain date time value as String in format "11/16/2017  5:27:00 AM"
    private void populateWIFISourceCreatedDate(Contact con)
    {
        system.debug('inside populateWIFISourceCreatedDate - ' + con);
        if(con.Source__c == FBUtilityClass.SOURCE_WIFI)
        {
            system.debug('con.Source__c - ' + con.Source__c);
            system.debug('con.WiFi_Entered_Datetime__c - ' + con.WiFi_Entered_Datetime__c );
            if(con.WiFi_Entered_Datetime__c != null && con.WiFi_Entered_Datetime__c != '')
            {
                DateTime WiFiEnteredDatetime = parseDateTimeFromString(con.WiFi_Entered_Datetime__c);
                system.debug('WiFiEnteredDatetime  - ' + WiFiEnteredDatetime  );
                con.Source_System_Created_Date__c = WiFiEnteredDatetime;
                system.debug('con.Source_System_Created_Date__c - ' + con.Source_System_Created_Date__c );
            }
        }
    }
    
    public static Datetime parseDateTimeFromString(String strDateTime)
    {
        system.debug('inside parseDateTimeFromString - ' + strDateTime);
        DateTime date_time = null;
        try
        {
            if(strDateTime != null && strDateTime != '')
            {
                strDateTime = strDateTime.trim(); //removing trailing spaces
                String[] dtStringArr = strDateTime.split(' '); //splitting by whitespace
                String part1 = dtStringArr[0]; //date
                String part2 = dtStringArr[1]; //time                
                String part3 = null; 
                if(dtStringArr.size()==3)
                    part3 = dtStringArr[2];//am or pm
                    
                //spliting date
                dtStringArr = part1.split('/');
                integer month = Integer.valueOf(dtStringArr[0]);
                integer day = Integer.valueOf(dtStringArr[1]);
                integer year = Integer.valueOf(dtStringArr[2]);
                
                dtStringArr = part2.split(':');
                integer hh = Integer.valueOf(dtStringArr[0]);
                integer mm = 0;
                if(dtStringArr.size()>1)
                    mm = Integer.valueOf(dtStringArr[1]);
                integer ss = 0;
                if(dtStringArr.size()>2)
                    ss = Integer.valueOf(dtStringArr[2]);
                
                if(part3!=null && part3!='')
                {
                    if(part3.containsIgnoreCase('am'))
                    { // conversion not required
                        date_time = datetime.newInstance(year, month, day, hh, mm, ss);    
                    }
                    else if(part3.containsIgnoreCase('pm'))
                    { //convert 12 hrs to 24 hrs
                        hh = (hh == 12) ? 00 : hh+12;
                        date_time = datetime.newInstance(year, month, day, hh, mm, ss);
                    }
                    else 
                    {
                        //invalid dateteime string 
                        date_time = null;
                    }
                }
                else
                {
                    date_time = datetime.newInstance(year, month, day, hh, mm, ss); 
                }                
            }
            else
            {
                //invalid string so return null
                date_time = null;
            }
        }
        catch(Exception ex)
        {
            system.debug('Exception occurred in conversion of string to Datetime format : '+ ex.getMessage());
        }
        system.debug('date_time - ' + date_time);
        return date_time;
    }
    
    private void populateAccount(Contact con, Account defaultAccount)
    {
        if(con != null && (con.AccountId == null) && defaultAccount != null)
            con.AccountId = defaultAccount.Id;
    }
}