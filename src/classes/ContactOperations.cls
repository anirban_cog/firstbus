/*************************************************************
* Author: Rajat Tripathi
* Created Date: 12/22/17
* Version1: B-16912
* Details: Batch class that deletes duplicate Contact, transaction processor and purchases processor 
* Purpose: Batch class that deletes duplicate Contact, transaction processor and purchases processor
**************************************************************/
public class ContactOperations implements Database.Batchable<sObject>{
    
    public Database.QueryLocator start(Database.BatchableContext batchableContext){
        System.debug('-->start method<--');
        return Database.getQueryLocator('SELECT Id,Name,Duplicate__c FROM Contact');
    }
    
    public void execute(Database.BatchableContext batchableContext, List<sObject> contactToBeDeleted){
        try{  
			
            ContactOperationsLogic contactLogic= new ContactOperationsLogic();
            
            //Duplicate Contact Deletion
            if(Schema.sObjectType.Contact.isDeletable())
            	DataBase.delete(DataBase.query(Constants.DuplicateContactDeletionQuery));
			
            Map<Id,Transaction_Processor__c> TPListToBeDeleted = new Map<Id,Transaction_Processor__c>();
            Map<Id,Purchases_Processor__c> PPListToBeDeleted = new Map<Id,Purchases_Processor__c>();
            //List all PP that fall outside of last 5 in a transaction and have processed = true and expiry time < current date
            for(Purchases_Processor__c pp : contactLogic.generateListOfPurchasesToDelete())
            	PPListToBeDeleted.put(pp.Id,pp);
            
            //List all TP that fall outside of last 5 in a transaction and have processed = true
            Map<String,List<SObject>> deleteMap = contactLogic.generateListOfTransactionsToDelete();
            for(SObject sobj : deleteMap.get('Transaction')){
                Transaction_Processor__c tp = (Transaction_Processor__c) sobj;
                TPListToBeDeleted.put(tp.Id,tp);
            }
            
            for(SObject sobj : deleteMap.get('Purchase')){
                Purchases_Processor__c pp = (Purchases_Processor__c) sobj;
                PPListToBeDeleted.put(pp.Id,pp);
            }
            
            if(!PPListToBeDeleted.isEmpty() && Schema.sObjectType.Purchases_Processor__c.isDeletable())
                delete PPListToBeDeleted.values();
            
            if(!TPListToBeDeleted.isEmpty() && Schema.sObjectType.Transaction_Processor__c.isDeletable())
                delete TPListToBeDeleted.values();
        }
        catch(DMLException e){
            System.debug('Exception caused: '+e.getMessage());
        }      
    }
    public void finish(Database.BatchableContext batchableContext){
        //Schedule the chain to execute again after 12 hrs
        Integer scheduledJobCount = [SELECT count() FROM CronTrigger where CronJobDetail.Name = :Constants.ContactOperationChainName];
        if(scheduledJobCount>0)
            system.debug('JOB IS ALREADY SCHEDULED, HENCE NOT SCHEDULING ONCE MORE');
        else{
            system.debug('JOB IS NOT SCHEDULED, HENCE SCHEDULING ONE NOW');
            System.scheduleBatch(new ContactOperations(), Constants.ContactOperationChainName, Constants.ContactOperationJobInterval); //String cronID = 
        }
    }
}