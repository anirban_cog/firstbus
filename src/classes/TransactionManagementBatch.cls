/*************************************************************
* Author: Ayan Hore
* Created Date: 11/23/17
* Version1: B-16786, B-16787
* Details: Transaction management batch job 
* Purpose: Convert Transaction Processor to Transaction Big Object
**************************************************************/
public class TransactionManagementBatch implements Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Constants.TransactionBatchQuery);
   }

   public void execute(Database.BatchableContext BC, List<sObject> scope){
       List<Transaction_Processor__c> transRecordList = new List<Transaction_Processor__c>();
       Map<Id, Transaction_Processor__c> transProcessMap = new Map<Id, Transaction_Processor__c>();
       
       for(SObject obj : scope){
           Transaction_Processor__c tempObj = (Transaction_Processor__c) obj;
           transRecordList.add(tempObj);
           transProcessMap.put(tempObj.Id,tempObj);
        }
       TransactionManagementLogic transactionLogic = new TransactionManagementLogic();
        
        Map<Id,Contact> correctContactMap = new Map<Id,Contact>();
        correctContactMap = transactionLogic.createContactToTransactionMap(transRecordList);
        List<Transaction__b> TransactionBOList = new List<Transaction__b>();
        TransactionBOList = transactionLogic.upsertTransactionBO(transRecordList, correctContactMap);
        
       // Insert transaction BO
        if(!TransactionBOList.isEmpty() && Schema.sObjectType.Transaction__b.isCreateable() && !Test.isRunningTest())
            database.insertImmediate(TransactionBOList);
       
       //Update Transaction Processor records with correct Contact information (if applicable)
       List<Transaction_Processor__c> transProcessorRecordToBeUpdated = new List<Transaction_Processor__c>();
       for(Transaction__b bo: TransactionBOList){
           Transaction_Processor__c tempObj = transProcessMap.get(bo.Transaction_Processor__c);
           tempObj.Processed__c = true;
           tempObj.Customer__c = bo.Customer__c;
           transProcessorRecordToBeUpdated.add(tempObj);
       }
       
       if(!transProcessorRecordToBeUpdated.isEmpty() && Schema.sObjectType.Transaction_Processor__c.isUpdateable()){
           try{
               update transProcessorRecordToBeUpdated;
           }
           catch(Exception e){
               system.debug('ERROR : '+e.getMessage());
           }
       }
          
   }

   public void finish(Database.BatchableContext BC){
       //if(!Test.isRunningTest())
        database.executeBatch(new PurchaseManagementBatch(),200);
   }
    
}