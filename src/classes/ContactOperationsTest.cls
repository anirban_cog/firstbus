@isTest
public class ContactOperationsTest {
    
    static TestDataComplexFunctions testDataComplexFunctions =  new TestDataComplexFunctions();
    static final String LastName = 'LastName';
    static final String Name = 'Name';
    static final String Price = 'Price__c';
    static final String ExpiryTime = 'Expiry_Time__c';
    static final String PurchaseDate = 'Purchase_Date__c';
    static final String Amount = 'Amount__c';
    static final String CustomerLookup = 'Customer__c';
    static final String TransactionLookup = 'Transaction__c';
    static final String Processed = 'Processed__c';
    static final String StringType = 'String';
    
    //Test method for batch class :ContactOperations
    @isTest
    static void batchTestMethod(){
        testCase1();
        testCase2();
        Test.startTest();
        System.debug('Testing started');
        try{
            Database.executeBatch(new ContactOperations(),200);
        }
        catch(Exception e){
            system.debug('EXCEPTION : '+e.getMessage()+' | Cause : '+e.getCause());
        }
        Test.stopTest();
        system.assert(2 ==  [select count() from contact], 'Contacts deletion mechanism is success');
        //system.assert(9 ==  [select count() from Transaction_Processor__c], 'Transaction Processors deletion mechanism is success');
        //system.assert(17 ==  [select count() from Purchases_Processor__c], 'Purchases Processor deletion mechanism is success');
        
    }
    
    //Test method for ContactHandler & ContactLogic class
    @isTest
    static void contactDeduplicationTestMethod(){
        
        List<SourcePriority__c> customSettings = new List<SourcePriority__c>();
        customSettings.add(new SourcePriority__c(Name='MTicket',Priority__c=1));
        customSettings.add(new SourcePriority__c(Name='CS Agent',Priority__c=4));
        customSettings.add(new SourcePriority__c(Name='Prospect',Priority__c=7));
        customSettings.add(new SourcePriority__c(Name='Social CS',Priority__c=6));
        customSettings.add(new SourcePriority__c(Name='Web',Priority__c=2));
        customSettings.add(new SourcePriority__c(Name='Web Chat',Priority__c=5));
        customSettings.add(new SourcePriority__c(Name='Wifi',Priority__c=3));
        insert customSettings;
        
        
        Test.startTest();
        //Contact List of Key Value pairs to insert
        List<KeyValue> contactKV = new List<KeyValue>();
        contactKV.add(new KeyValue(LastName,'contact - 1',StringType));
        contactKV.add(new KeyValue('MobilePhone','12345',StringType));
        contactKV.add(new KeyValue('Source__c','Mticket',StringType));
        contactKV.add(new KeyValue('Original_Source__c','',StringType));
        contactKV.add(new KeyValue('Email__c','test@test.com',StringType));
        contactKV.add(new KeyValue('Town__c','2abcdefghijklmnopqrstuvwxyzabcdefghijklmn',StringType));
        contactKV.add(new KeyValue('FirstName__c','a2bcdefghijklmnopqrstuvwxyzabcdefghijklmnabcdefghijklmnopqrstuvwxyzabcde',StringType));
        contactKV.add(new KeyValue('Del_Town__c','2delivery_town_abcdef',StringType));
        
        testDataComplexFunctions.insertContact(contactKV,null);
        
        contactKV = new List<KeyValue>();
        contactKV.add(new KeyValue(LastName,'contact - 2',StringType));
        contactKV.add(new KeyValue('MobilePhone','12345',StringType));
        contactKV.add(new KeyValue('Source__c','Web',StringType));
        contactKV.add(new KeyValue('Original_Source__c','',StringType));
        contactKV.add(new KeyValue('Email__c','test@test.com',StringType));
        contactKV.add(new KeyValue('Town__c','2abcdefghijklmnopqrstuvwxyz',StringType));
        contactKV.add(new KeyValue('FirstName__c','a2bcdefghijklm',StringType));
        contactKV.add(new KeyValue('Del_Town__c','12345678901234567890123456789012345678901',StringType));
        
        testDataComplexFunctions.insertContact(contactKV,null);
        
        contactKV = new List<KeyValue>();
        contactKV.add(new KeyValue(LastName,'contact - 3',StringType));
        contactKV.add(new KeyValue('MobilePhone','1234567',StringType));
        contactKV.add(new KeyValue('Email__c','test12345678901234567890123456789012345678901234567890123456789012345678901234567890@test.com',StringType));
        contactKV.add(new KeyValue('Source__c','Wifi',StringType));
        contactKV.add(new KeyValue('Original_Source__c','',StringType));
        
        Contact c = testDataComplexFunctions.insertContact(contactKV,null);
        System.debug('Result ---------------->: '+Database.query('SELECT ID,LastName, EMail, MobilePhone, Source__c,Duplicate__c from Contact'));
        
        
        List<Contact> conList = new List<Contact>();
        conList.add(c);
        delete conList;
        undelete conList;
        
        Test.stopTest();
        //System.assert([select count() from contact] == 3,'Success');
        
    }
    
    public static void testCase1(){
        
        DateTime dt = DateTime.now();
        List<KeyValue> contactKV = new List<KeyValue>();
        List<KeyValue> transactionProcessorKV = new List<KeyValue>();
        List<KeyValue> purchasesProcessorKV = new List<KeyValue>();
        Contact con;
        Transaction_Processor__c tp;
        
        
        contactKV.add(new KeyValue(LastName,'Batch-TestClass-Contact1',StringType));
        contactKV.add(new KeyValue('Gender__c','Male',StringType));
        contactKV.add(new KeyValue('Salutation','Mr.',StringType));
        con = testDataComplexFunctions.insertContact(contactKV,null);
        
        //transaction1 under contact1
        transactionProcessorKV.add(new KeyValue(Name,' contact-1-transaction - 1',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 6)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'1','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        //system.assert(false,tp);
        //purchase1 under transaction1
        purchasesProcessorKV.add(new KeyValue(Price,'11','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 2)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase2 under transaction1
        purchasesProcessorKV.add(new KeyValue(Price,'12','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 3)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        
        //transaction2 under contact1
        transactionProcessorKV.add(new KeyValue(Name,' contact-1-transaction - 2',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 5)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'2','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction2
        purchasesProcessorKV.add(new KeyValue(Price,'21','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 3)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase2 under transaction2
        purchasesProcessorKV.add(new KeyValue(Price,'22','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 2)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase3 under transaction2
        purchasesProcessorKV.add(new KeyValue(Price,'23','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 1)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase4 under transaction2
        purchasesProcessorKV.add(new KeyValue(Price,'24','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 4)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase5 under transaction2
        purchasesProcessorKV.add(new KeyValue(Price,'25','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 5)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);        
        
        //transaction3 under contact1
        transactionProcessorKV.add(new KeyValue(Name,' contact-1-transaction - 3',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 4)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'1','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction3
        purchasesProcessorKV.add(new KeyValue(Price,'31','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 2)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //transaction4 under contact1
        transactionProcessorKV.add(new KeyValue(Name,' contact-1-transaction - 4',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 3)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'1','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction4
        purchasesProcessorKV.add(new KeyValue(Price,'41','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 2)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        
        //transaction5 under contact1
        transactionProcessorKV.add(new KeyValue(Name,' contact-1-transaction - 5',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 2)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'1','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction5
        purchasesProcessorKV.add(new KeyValue(Price,'51','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 2)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        
        //transaction6 under contact1
        transactionProcessorKV.add(new KeyValue(Name,' contact-1-transaction - 6',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 1)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'1','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction6
        purchasesProcessorKV.add(new KeyValue(Price,'61','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 5)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        
        
        
    }
    
    
    public static void testCase2(){
        DateTime dt = DateTime.now(); 
        List<KeyValue> contactKV = new List<KeyValue>();
        List<KeyValue> transactionProcessorKV = new List<KeyValue>();
        List<KeyValue> purchasesProcessorKV = new List<KeyValue>();
        
        Contact con;
        Transaction_Processor__c tp;
        
        contactKV.add(new KeyValue(LastName,'Batch-TestClass-Contact2',StringType));
        con = testDataComplexFunctions.insertContact(contactKV,null);
        
        //transaction1 under contact2
        transactionProcessorKV.add(new KeyValue(Name,' contact-2-transaction - 1',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 4)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'1','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction1
        purchasesProcessorKV.add(new KeyValue(Price,'211','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 6)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase2 under transaction1
        purchasesProcessorKV.add(new KeyValue(Price,'222','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 5)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase3 under transaction1
        purchasesProcessorKV.add(new KeyValue(Price,'213','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 4)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase4 under transaction1
        purchasesProcessorKV.add(new KeyValue(Price,'214','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 3)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //purchase5 under transaction1
        purchasesProcessorKV.add(new KeyValue(Price,'215','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 2)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);        
        
        
        //purchase6 under transaction1
        purchasesProcessorKV.add(new KeyValue(Price,'216','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 1)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //transaction2 under contact1
        transactionProcessorKV.add(new KeyValue(Name,' contact-2-transaction - 2',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 3)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'2','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction2
        purchasesProcessorKV.add(new KeyValue(Price,'216','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 7)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //transaction3 under contact2
        transactionProcessorKV.add(new KeyValue(Name,' contact-2-transaction - 3',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 2)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'1','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction3
        purchasesProcessorKV.add(new KeyValue(Price,'31','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() + 3)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
        
        //transaction4 under contact2
        transactionProcessorKV.add(new KeyValue(Name,' contact-1-transaction - 4',StringType));
        transactionProcessorKV.add(new KeyValue(PurchaseDate,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 1)),'DATETIME'));
        transactionProcessorKV.add(new KeyValue(Amount,'1','DECIMAL'));
        transactionProcessorKV.add(new KeyValue(CustomerLookup,con.id,'ID'));
        transactionProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN'));
        tp = testDataComplexFunctions.insertTransactionProcessor(transactionProcessorKV,null);
        
        //purchase1 under transaction4
        purchasesProcessorKV.add(new KeyValue(Price,'41','DECIMAL')); //Need to confirm field type
        purchasesProcessorKV.add(new KeyValue(Processed,'true','BOOLEAN')); 
        purchasesProcessorKV.add(new KeyValue(ExpiryTime,String.valueOf(Datetime.newInstance(dt.year(),dt.month(),dt.day() - 4)),'DATETIME'));
        purchasesProcessorKV.add(new KeyValue(TransactionLookup,tp.id,'ID'));
        testDataComplexFunctions.insertPurchasesProcessor(purchasesProcessorKV,null);
    }        
    
}