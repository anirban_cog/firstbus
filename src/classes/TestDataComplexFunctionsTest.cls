/*************************************************************
* Author: Steve Fouracre
* Created Date: 05/01/18
* Version1: B-16771
* Details: Test class for TestDataComplexFunctions
**************************************************************/
@isTest
private class TestDataComplexFunctionsTest {

	@testsetup static void createSetupData(){
	    List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
	    CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
	    CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
	    CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
	    insert CSObjList;
	}
	
    static testMethod void insertAccountAndContactTest1() {
		Test.startTest();
				TestDataComplexFunctions tdc = new TestDataComplexFunctions();
				Account acc = tdc.insertAccountAndContact(null, null);
		Test.stopTest();		        
        system.assert([Select id From Account].size() > 0);		        
        system.assert([Select id From Contact].size() > 0);		        
    }

    static testMethod void insertAccountAndContactTest2() {
		Test.startTest();
				TestDataComplexFunctions tdc = new TestDataComplexFunctions();
				Map<System.Type, List<KeyValue>> kMaps = new Map<System.Type, List<KeyValue>>();
				List<KeyValue> ky = new List<KeyValue>();
				ky.add(new KeyValue('Phone','0775357389','String'));
				kMaps.put(Account.class, ky);

				tdc.insertAccountAndContact(kMaps, null);
		Test.stopTest();		        
        system.assert([Select id From Account where Phone = '0775357389'].size() > 0);		        
        system.assert([Select id From Contact].size() > 0);		        
    }

    static testMethod void insertAccountAndContactTest3() {
		Test.startTest();
				TestDataComplexFunctions tdc = new TestDataComplexFunctions();
				Map<System.Type, List<KeyValue>> kMaps = new Map<System.Type, List<KeyValue>>();
				List<KeyValue> ky = new List<KeyValue>();
				ky.add(new KeyValue('FirstName','Steve','String'));
				kMaps.put(Contact.class, ky);

				tdc.insertAccountAndContact(kMaps, null);
		Test.stopTest();		        
        system.assert([Select id From Account].size() > 0);		        
        system.assert([Select id From Contact].size() > 0);		        
    }

   static testMethod void insertContactAndTransactionProcessorTest1() {
		Test.startTest();
				TestDataComplexFunctions tdc = new TestDataComplexFunctions();
		        List<KeyValue> contactKV = new List<KeyValue>();
		        contactKV.add(new KeyValue('Duplicate__c','false','BOOLEAN')); //Need to confirm field type
		        contactKV.add(new KeyValue('LastName','last - 1','String'));
		        
		        //Transaction Processor List of key value pairs to insert
		        List<KeyValue> transactionProcessorKV = new List<KeyValue>();
		        transactionProcessorKV.add(new KeyValue('Name',' transaction - 1','String'));
				transactionProcessorKV.add(new KeyValue('First_Name__c','Steve','String'));
		
		        Map<System.Type, List<KeyValue>> contactAndTransactionProcessorMap = new Map<System.Type, List<KeyValue>>();
		        contactAndTransactionProcessorMap.put(Contact.class,contactKV);
		        contactAndTransactionProcessorMap.put(Transaction_Processor__c.class,transactionProcessorKV);

				tdc.insertContactAndTransactionProcessor(contactAndTransactionProcessorMap, null);
		Test.stopTest();		        
        //system.assert([Select id From Transaction_Processor__c where First_Name__c='Steve'].size() > 0);		        
        //system.assert([Select id From Contact].size() > 0);		        
    }

    static testMethod void insertContactAndTransactionProcessorTest2() {
		Test.startTest();
				TestDataComplexFunctions tdc = new TestDataComplexFunctions();
		        List<KeyValue> contactKV = new List<KeyValue>();
		        contactKV.add(new KeyValue('Duplicate__c','false','BOOLEAN')); //Need to confirm field type
		        contactKV.add(new KeyValue('LastName','last - 1','String'));
				contactKV.add(new KeyValue('FirstName','Steve','String'));
		        
		        //Transaction Processor List of key value pairs to insert
		        List<KeyValue> transactionProcessorKV = new List<KeyValue>();
		        transactionProcessorKV.add(new KeyValue('Name',' transaction - 1','String'));
		
		        Map<System.Type, List<KeyValue>> contactAndTransactionProcessorMap = new Map<System.Type, List<KeyValue>>();
		        contactAndTransactionProcessorMap.put(Contact.class,contactKV);
		        contactAndTransactionProcessorMap.put(Transaction_Processor__c.class,transactionProcessorKV);

				tdc.insertContactAndTransactionProcessor(contactAndTransactionProcessorMap, null);
		Test.stopTest();		        
        //system.assert([Select id From Transaction_Processor__c].size() > 0);		        
        //system.assert([Select id From Contact where FirstName='Steve'].size() > 0);		        
    }    

   static testMethod void insertContactAndDeviceIdTest1() {
		Test.startTest();
				TestDataComplexFunctions tdc = new TestDataComplexFunctions();
		        List<KeyValue> contactKV = new List<KeyValue>();
		        contactKV.add(new KeyValue('Duplicate__c','false','BOOLEAN')); //Need to confirm field type
		        contactKV.add(new KeyValue('LastName','last - 1','String'));
		        
		        Map<System.Type, List<KeyValue>> kMaps = new Map<System.Type, List<KeyValue>>();
		        kMaps.put(Contact.class,contactKV);

		        List<KeyValue> lstDeviceKV = new List<KeyValue>();
		        lstDeviceKV.add(new KeyValue('C3_User_Id__c','C12','String'));
		        lstDeviceKV.add(new KeyValue('AppVersion__c','V1','String'));
		        lstDeviceKV.add(new KeyValue('Device__c','D1','String'));
		        kMaps.put(Device_ID__c.class,lstDeviceKV);

				tdc.insertContactAndDeviceId(kMaps, null);
		Test.stopTest();		        
        system.assert([Select id From Device_ID__c ].size() > 0);		        
        system.assert([Select id From Contact].size() > 0);		        
    }

   static testMethod void insertTransactionProcessorAndPurchaseProcessorTest1() {
		Test.startTest();
				TestDataComplexFunctions tdc = new TestDataComplexFunctions();
		        
		        Map<System.Type, List<KeyValue>> kMaps = new Map<System.Type, List<KeyValue>>();
		        //Transaction Processor List of key value pairs to insert
		        List<KeyValue> transactionProcessorKV = new List<KeyValue>();
		        transactionProcessorKV.add(new KeyValue('Name',' transaction - 1','String'));
		
		        kMaps.put(Transaction_Processor__c.class,transactionProcessorKV);

		        DateTime expiryDate = Datetime.now();
		        expiryDate = DateTime.newInstance(expiryDate.year() + 1, 11, 10, 6, 6, 6);
		        List<KeyValue> purchasesProcessorKV = new List<KeyValue>();
		        purchasesProcessorKV.add(new KeyValue('Price__c','250','DECIMAL')); //Need to confirm field type
		        purchasesProcessorKV.add(new KeyValue('Ticket_Id__c','98789','String'));
		        purchasesProcessorKV.add(new KeyValue('Expiry_Time__c',String.valueOf(expiryDate),'DATETIME'));
		        
		        kMaps.put(Purchases_Processor__c.class,purchasesProcessorKV);

				tdc.insertTransactionProcessorAndPurchaseProcessor(kMaps, null);
		Test.stopTest();		        
        //system.assert([Select id From Purchases_Processor__c ].size() > 0);		        
        //system.assert([Select id From Transaction_Processor__c].size() > 0);		        
    }

   static testMethod void insertContactAndTransactionProcessorAndPurchaseProcessorTest1() {
		Test.startTest();
				TestDataComplexFunctions tdc = new TestDataComplexFunctions();
		        
		        Map<System.Type, List<KeyValue>> kMaps = new Map<System.Type, List<KeyValue>>();
		        List<KeyValue> contactKV = new List<KeyValue>();
		        contactKV.add(new KeyValue('Duplicate__c','false','BOOLEAN')); //Need to confirm field type
		        contactKV.add(new KeyValue('LastName','last - 1','String'));

		        kMaps.put(Contact.class,contactKV);

		        //Transaction Processor List of key value pairs to insert
		        List<KeyValue> transactionProcessorKV = new List<KeyValue>();
		        transactionProcessorKV.add(new KeyValue('Name',' transaction - 1','String'));
		
		        kMaps.put(Transaction_Processor__c.class,transactionProcessorKV);

		        DateTime expiryDate = Datetime.now();
		        expiryDate = DateTime.newInstance(expiryDate.year() + 1, 11, 10, 6, 6, 6);
		        List<KeyValue> purchasesProcessorKV = new List<KeyValue>();
		        purchasesProcessorKV.add(new KeyValue('Price__c','250','DECIMAL')); //Need to confirm field type
		        purchasesProcessorKV.add(new KeyValue('Ticket_Id__c','98789','String'));
		        purchasesProcessorKV.add(new KeyValue('Expiry_Time__c',String.valueOf(expiryDate),'DATETIME'));
		        
		        kMaps.put(Purchases_Processor__c.class,purchasesProcessorKV);

				tdc.insertContactAndTransactionProcessorAndPurchaseProcessor(kMaps, null);
		Test.stopTest();		        
        system.assert([Select id From Contact].size() > 0);		        
        system.assert([Select id From Purchases_Processor__c ].size() > 0);		        
        //system.assert([Select id From Transaction_Processor__c].size() > 0);		        
    }
}