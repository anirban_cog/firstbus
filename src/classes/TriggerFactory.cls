public with sharing class TriggerFactory { 
    
    public class TriggerException extends Exception{} 
    public static CommitHandler ch; 
    
    public static void createHandler(Schema.sObjectType thisobjType, String hdler){
        Schema.DescribeSObjectResult d = thisobjType.getDescribe(); 
        
        String thisType = d.getName(); 
        ITrigger handler = getHandler(hdler); 
        
        if (handler == null) 
            throw new TriggerException('No Trigger Handler registered for Object Type: ' + thisType); 
        
        //setup monitoring 
        UtilsMonitoring.setupMonitoring(); 
        
        execute(handler, thisType); 
    } 
    
    private static void execute(ITrigger handler, String objType){ 
        
        boolean notriggerSetting; 
        boolean noTriggersPerObject; 
        try{ 
            notriggerSetting = TriggerController.globalTriggerControlSetting();
            noTriggersPerObject = TriggerController.globalTriggerPerObjectControlSetting(objType); 
        } 
        catch (Exception ex){system.debug('error in trigger controller ' + ex); } 
        
        Type soType = Type.forName(objType);
        if (!notriggerSetting && !noTriggersPerObject && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_ALL)) { 
            
            if (Trigger.isBefore){ 
                if (Trigger.isUpdate && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_UPDATE)){ 
                    handler.beforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap, ch); 
                    TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(soType, TriggerController.TRIGGER_UPDATE), true); 
                } 
                else if (Trigger.isDelete && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_DELETE)){ 
                    handler.beforeDelete(Trigger.old, Trigger.oldMap, ch); 
                    TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(soType, TriggerController.TRIGGER_DELETE), true); 
                } 
                else if (Trigger.isInsert && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_INSERT)){ 
                    handler.beforeInsert(Trigger.new, ch); 
                    TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(soType, TriggerController.TRIGGER_INSERT), true); 
                } 
                else if (Trigger.isUnDelete && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_UNDELETE)){ 
                    handler.beforeUnDelete(Trigger.old, Trigger.oldMap, ch); 
                    TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(soType, TriggerController.TRIGGER_UNDELETE), true); 
                } 
                
            } 
            else{
                if (Trigger.isUpdate && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_UPDATE)){ 
                    handler.afterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap, ch); 
                    TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(soType, TriggerController.TRIGGER_UPDATE), true); 
                } 
                else if (Trigger.isDelete && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_DELETE)){ 
                    handler.afterDelete(Trigger.old, Trigger.oldMap, ch); 
                    TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(soType, TriggerController.TRIGGER_DELETE), true); 
                } 
                else if (Trigger.isInsert && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_INSERT)){ 
                    handler.afterInsert(Trigger.new, Trigger.newMap, ch); 
                    TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(soType, TriggerController.TRIGGER_INSERT), true); 
                } 
                else if (Trigger.isUnDelete && !TriggerController.getTriggerControlValue(soType, TriggerController.TRIGGER_UNDELETE)){ 
                    handler.afterUnDelete(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap, ch); 
                    TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(soType, TriggerController.TRIGGER_UNDELETE), true); 
                } 
            } 
            
            ch.mergedCommitToDataBase(); 
        } 
        
    } 
    
    private static ITrigger getHandler(String hdler){ 
        ch = new CommitHandler(); 
        Type typ = Type.forName(hdler); 
        return (ITrigger)typ.newInstance(); 
        
    } 
    
}