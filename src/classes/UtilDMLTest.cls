@istest
public class UtilDMLTest {

static TestDataComplexFunctions tdc = new TestDataComplexFunctions();
private static Debug_Error__c[] errors = new Debug_Error__c[]{};
    
private static testMethod void returnDMLErrorRecordsTest1(){
	//TestDataInsert will call DML_Master and because errors is passed UtilDML will be called and returnDMLErrorRecords()
    List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
    CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
    insert CSObjList;
	
	system.assert([Select id From Debug_Error__c].size() == 0);
	List<KeyValue> ky = new List<KeyValue>();
	ky.add(new KeyValue('Grade__c','None','String'));//this will cause an error in DML which is what this is testing
	tdc.insertAccount(ky,errors);	
	Test.startTest();
		//-ve
		system.assert([Select id From Account].size() > 0);
	Test.stopTest();
}

private static testMethod void returnDMLErrorRecordsTest2(){
	//TestDataInsert will call DML_Master and because errors is passed UtilDML will be called and returnDMLErrorRecords()
    List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
    CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
    insert CSObjList;
	
	tdc.insertAccount(null,null);	
	Test.startTest();
		//+ve
		system.assert([Select id From Account].size() == 1);
	Test.stopTest();
}

private static testMethod void returnDMLErrorRecordsTest3(){	
    List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
    CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
    insert CSObjList;
	
	Account acc = tdc.insertAccount(null,null);
	Test.startTest();
		//+ve
		system.assert([Select id From Account ].size() == 1);
		Database.Deleteresult Delres = Database.delete(acc, false);		
		UtilDML.returnDMLErrorRecords(new Database.Deleteresult[]{Delres}, null);
		system.assert([Select id From Account ].size() == 0);
		//-ve should fail already deleted
		UtilDML.returnDMLErrorRecords(new Database.Deleteresult[]{Delres}, null);		
	Test.stopTest();
}

private static testMethod void returnDMLErrorRecordsTest4(){	
    List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
    CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
    insert CSObjList;
	
	Account acc = tdc.insertAccount(null,null);
	Test.startTest();
		//+ve
		Database.Deleteresult Delres = Database.delete(acc, false);
		system.assert([Select id From Account where isdeleted=false].size() == 0);
		Database.UnDeleteresult unDelres = Database.undelete(acc, false);
		UtilDML.returnDMLErrorRecords(new Database.UnDeleteresult[]{unDelres}, null);
		system.assert([Select id From Account where isdeleted=false].size() == 1);
	Test.stopTest();
}

private static testMethod void returnDMLErrorRecordsTest5(){	
    List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
    CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
    insert CSObjList;
	
	Account acc = tdc.insertAccount(null,null);
	Test.startTest();
		//+ve
		Database.Upsertresult ups = Database.Upsert(acc,false);
		UtilDML.returnDMLErrorRecords(new Database.Upsertresult[]{ups}, null);
		system.assert([Select id From Account].size() == 1);
	Test.stopTest();
}

private static testMethod void returnDMLErrorRecordsTest6(){	
	Account[] acc = (Account[])(tdc.prepareSObject('{"attributes":{"type":"Account"},"Name":"Test ClassAccount","FirstName":"Test","Salutation":"Mr.","RecordTypeId":'+'"'+ new TestDataJsonLibrary.Standard().personAccountRecordTypeId+'"'+',"BillingStreet":"London","BillingCity":"London","BillingState":"London","BillingCountry":"United Kingdom","BillingAddress":{"city":"London","country":"United Kingdom","geocodeAccuracy":null,"latitude":null,"longitude":null,"postalCode":null,"state":"London","street":"London"},"ShippingAddress":null,"IsPersonAccount":true,"PersonMailingAddress":null,"Phone":"+4414568978","PersonEmail":"testaccount@firstbus.co.uk","PersonHasOptedOutOfEmail":false,"CelerityBusinessKey__c":"Indiv-000","Age_Range__c":"35","Like_to_receive_latest_offers__c":true,"Total_Purchase_Count__c":5,"FirstPurchaseDate__c":"2017-11-13","LastPurchaseDate__c":"2017-11-30","University__c":"Test University","SupplierBusinessKey__c":"Web","Like_to_receive_offers__c":false,"Reference_ID__c":"Ref-000","How_often_do_you_travel_per_week__c":"3","Gender__c":"Male","Organisation_1__c":"Bath","Organisation_2__c":"New York","Organisation_3__c":"Bath","Archive__c":false,"Last_Purchase_Count__c":-17,"First_Purchase_Type__c":"Temp","Last_Purchase_Type__c":"Temp","Purchase_Count__c":5,"Source__c":"NOT VALID","Ticket_Activation_Date__c":"2017-11-30","Purpose_of_journey__c":"Testing","Organisation_4__c":"Bath","Organisation_5__c":"Bath","Source_Created_Date__c":"2017-11-14T12:50:00.000+0000","Source_Amended_Date__c":"2017-11-22T12:50:00.000+0000","Reason_for_use__c":"Testing","Amended_Transaction_Date__c":"2017-11-29T12:50:00.000+0000","Frequency_of_use__c":"Work","MailingList1__c":"MailingList-1","MailingList2__c":"MailingList-3","MailingList3__c":"MailingList-3","Control_Group__c":true,"Graduation_year__c":2011,"Group_Opt_In__c":true,"Customer_First_Hub_Member__pc":true}', null));
	Test.startTest();
		//+ve
		Database.SaveResult[] ups = Database.insert(acc,false);
		Debug_Error__c[] dbg = UtilDML.returnDMLErrorRecords(ups, null);
		system.assert(dbg.size() > 0);
		system.assert([Select id From Account].size() == 0);
	Test.stopTest();
}
    
private static testMethod void returnDMLErrorRecordsTest7(){	
    List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
    CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
    CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
    insert CSObjList;
	
	Account acc = new Account();
	acc.Name = 'Test ClassAccount';
	acc.RecordTypeId= new TestDataJsonLibrary.Standard().personAccountRecordTypeId;
	acc.Organisation_2__c = 'New York';
	acc.Source__c='NOT VALID';
	
	Account[] accs = new Account[]{acc};
	Test.startTest();
		//+ve
		Database.Upsertresult[] ups = Database.Upsert(accs,false);
		Debug_Error__c[] dbg = UtilDML.returnDMLErrorRecords(ups, null);
		system.assert(dbg.size() > 0);
	Test.stopTest();
}

}