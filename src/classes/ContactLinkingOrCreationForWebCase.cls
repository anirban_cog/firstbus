public class ContactLinkingOrCreationForWebCase {
    
    public static List<Contact> getExistingOrNewContact(List<Case> lstRecord){
        
        Set<String> setSuppliedEmail = new Set<String>();
        Set<String> setWebReferenceId = new Set<String>();
        List<Contact> lstContactToUpsert = new List<Contact>();
        Map<Case,List<Contact>> mapCaseTocontact = new Map<Case,List<Contact>>();
        
        Account firstBusAccount = new Account();
        firstBusAccount = [SELECT Id, Name FROM Account WHERE Name = 'FirstBus Account' LIMIT 1];
        
        for(Case objCase : lstRecord){
            
            setSuppliedEmail.add(objCase.SuppliedEmail);
            setWebReferenceId.add(objCase.Web_Reference_ID__c);
        }
        List<Contact> lstContact = new List<Contact>();
        lstContact = [SELECT Id, Web_Reference_ID__c, Email__c, Channels__c
                                        FROM Contact 
                                        WHERE ((Web_Reference_ID__c != null AND Web_Reference_ID__c IN :setWebReferenceId) 
                                        OR (Email__c != null AND Email__c IN :setSuppliedEmail))
                                        AND Duplicate__c = false];
                
        System.debug('----------lstContact'+lstContact);
        
        for(Case objCase : lstRecord){
            
            mapCaseTocontact.put(objCase, new List<Contact>());
            for(Contact objcontact : lstContact){
                
                if(objcontact.Email__c == objCase.SuppliedEmail || objcontact.Web_Reference_ID__c == objCase.Web_Reference_ID__c){
                    
                    if(mapCaseTocontact.containsKey(objCase))
                        mapCaseTocontact.get(objCase).add(objcontact);
                    else
                        mapCaseTocontact.put(objCase, new List<Contact>{objcontact});
                }
            }
        }
        
        for(Case record : mapCaseTocontact.keyset()){
            if(mapCaseTocontact.get(record).isEmpty()){
                Contact objNewCustomer = new Contact(FirstName__c = record.Firstname__c,
                                            LastName = record.Lastname__c,
                                            Salutation = record.Salutation__c,
                                            //LastName = 'New Customer',
                                            MailingCountry = record.MailingCountry__c,
                                            MailingPostalCode = record.MailingPostalCode__c,
                                            MailingStreet = record.MailingStreet__c,
                                            MailingCity = record.MailingCity__c,
                                            Phone =  record.Phone__c,
                                            /*Source__c = record.Source__c,
                                            Original_Source__c = record.Source__c,
                                            Channels__c = record.Source__c,
                                            Last_Interaction__c = record.Source__c,*/ //Commented by Paramita | B-16964  
                                            Source_System_Created_Date__c = system.Now(),
                                            Email__c = record.SuppliedEmail,
                                            AccountId = firstBusAccount.Id
                                            );
                updateSourceFieldsInContact(objNewCustomer, record.Source__c);//Paramita | B-16964
                lstContactToUpsert.add(objNewCustomer);
            }
             
            Contact objTemp = new Contact();
            if(!mapCaseTocontact.get(record).isEmpty()){
                objTemp = mapCaseTocontact.get(record)[0];
                /*objTemp.Source__c = record.Source__c;
                objTemp.Original_Source__c = record.Source__c;
                objTemp.Channels__c += (objTemp.Channels__c  != null && objTemp.Channels__c != '' ) ? ';'+ record.Source__c : record.Source__c;
                objTemp.Last_Interaction__c = record.Source__c;*/ //Commented by Paramita | B-16964
                updateSourceFieldsInContact(objTemp, record.Source__c);//Paramita | B-16964
                objTemp.Source_System_Created_Date__c = system.today();
                objTemp.AccountId = firstBusAccount.Id;  
                lstContactToUpsert.add(objTemp);
            }
            
        }
        upsert lstContactToUpsert;
        
        System.debug('----------lstContactToUpsert-------'+lstContactToUpsert);
        
        return lstContactToUpsert;
            
    }
    
    public static List<Contact> updateParentContactSourceRelatedFields(List<Case> lstRecord)//Paramita | B-16964 
    {
        List<Id> lstContactId = new List<Id>();
        List<Contact> lstContactToUpdate = new List<Contact>();
        Map<Id,Contact> mapCaseIdTocontact = new Map<Id,Contact>();
                   
        for(Case objCase : lstRecord)
        {
            lstContactId.add(objCase.ContactId);
        }
        List<Contact> lstContact = new List<Contact>();
        lstContact = [SELECT Id, Source__c, Original_Source__c, Channels__c, Last_Interaction__c, 
        MTickets_Reference_ID__c, Web_Reference_ID__c, Original_Web_Source__c 
        FROM Contact WHERE (Id IN :lstContactId) AND Duplicate__c = false];
                    
        System.debug('----------lstContact'+lstContact);
        for(Case objCase : lstRecord)
        {
            for(Contact objcontact : lstContact)
            {                
                if(objcontact.Id == objCase.ContactId)
                {
                    if(!mapCaseIdTocontact.containsKey(objCase.Id))
                    mapCaseIdTocontact.put(objCase.Id, objcontact);
                }
            }
        }
        System.debug('----------mapCaseIdTocontact'+mapCaseIdTocontact);
        for(Case caseRecord : lstRecord)
        {
            System.debug('----------caseRecord'+caseRecord);
            Contact parentContact = mapCaseIdTocontact.get(caseRecord.Id);
            System.debug('----------parentContact'+parentContact);
            if(parentContact != null)
            {
                updateSourceFieldsInContact(parentContact, caseRecord.Source__c);
                lstContactToUpdate.add(parentContact);
            }
        }
        System.debug('----------lstContactToUpdate'+lstContactToUpdate);
        UtilDML_Master.updateObjects(lstContactToUpdate);
        return lstContactToUpdate;
    }
    
    private static void updateSourceFieldsInContact(Contact contact, String caseSource)//Paramita | B-16964 
    {
        system.debug('inside updateSourceFieldsInContact -> contact - ' + contact);
        Contact tempContact = null;
        
        if(contact != null)
        {
            tempContact = new Contact();
            tempContact.Source__c = caseSource == 'Email' ? 'CS Agent' : caseSource;
            system.debug('inside updateSourceFieldsInContact -> tempContact - ' + tempContact);
            ContactLogic.updateSrcRelatedFieldInOriginalContact(contact, tempContact);  
            system.debug('inside updateSourceFieldsInContact -> contact - ' + contact ); 
        } 
    }
    
    
}