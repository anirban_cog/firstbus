@isTest
private class UtilDML_MasterTest {

	@testsetup private static void CreateTestData(){
        List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
        CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
        insert CSObjList;
	}

private static testmethod void insertObjectsTest1(){
	Test.startTest(); 
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
	Test.stopTest();
}  

private static testmethod void insertObjectsTest2(){
	//THIS WILL FAIL BECAUSE BillingPostalCode IS NOT ALLOWED TO BE > 10 (Note: WHY 10 I DONT KNOW - WHICH LOOKS INCORRECT TO ME)
	Test.startTest();
		Debug_Error__c[] errors = new Debug_Error__c[]{};
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, errors, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		system.assert([Select id From Debug_Error__c].size() == 0);
		system.assert(errors.isempty() == true);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
}

private static testmethod void insertObjectsTest3(){
	//THIS WILL FAIL BECAUSE BillingPostalCode IS NOT ALLOWED TO BE > 10 (Note: WHY 10 I DONT KNOW - WHICH LOOKS INCORRECT TO ME)
	Test.startTest();
		Debug_Error__c[] errors = new Debug_Error__c[]{};
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, errors, true, 'ALL');
		system.assert([Select id From Account].size() == 1);
		system.assert([Select id From Debug_Error__c].size() == 0);
		system.assert(errors.isempty() == true);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
}

private static testmethod void insertObjectsTest4(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')});
		system.assert([Select id From Account].size() == 1);
	Test.stopTest();
} 

private static testmethod void insertObjectsTest5(){
	Test.startTest();
		Debug_Error__c[] errors = new Debug_Error__c[]{};
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, errors, true, UtilDML_Master.TRIGGER_INSERT);
		system.assert([Select id From Account].size() == 1);
		system.assert([Select id From Debug_Error__c].size() == 0);
		system.assert(errors.isempty() == true);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
}

private static testmethod void updateObjectsTest1(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		acc.BillingPostalCode='SW123DF';
		UtilDML_Master.updateObjects(new Sobject[]{acc}, false, null, null, false, 'ALL');
		system.assert([Select id From Account where BillingPostalCode='SW123DF'].size() == 1);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
} 

private static testmethod void updateObjectsTest2(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];

		Debug_Error__c[] errors = new Debug_Error__c[]{};
		UtilDML_Master.updateObjects(new Sobject[]{acc}, false, null, errors, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		system.assert([Select id From Debug_Error__c].size() == 0);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
} 

private static testmethod void updateObjectsTest3(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];

		Debug_Error__c[] errors = new Debug_Error__c[]{};
		UtilDML_Master.updateObjects(new Sobject[]{acc}, false, null, errors, true, 'ALL');
		system.assert([Select id From Account ].size() == 1);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
		/*This didnt work?
		system.assert([Select id From Debug_Error__c].size() == 0);
		system.assert(errors.isempty() == false);
		*/
	Test.stopTest();
} 

private static testmethod void updateObjectsTest4(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')});
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		acc.BillingPostalCode='SW123DF';
		UtilDML_Master.updateObjects(new Sobject[]{acc});
		system.assert([Select id From Account where BillingPostalCode='SW123DF'].size() == 1);
	Test.stopTest();
}

private static testmethod void deleteObjectsTest1(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		UtilDML_Master.deleteObjects(new Sobject[]{acc}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 0);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
} 

private static testmethod void deleteObjectsTest2(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')});
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		UtilDML_Master.deleteObjects(new Sobject[]{acc});
		system.assert([Select id From Account].size() == 0);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
} 

private static testmethod void undeleteObjectsTest1(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		UtilDML_Master.deleteObjects(new Sobject[]{acc}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 0);
		UtilDML_Master.undeleteObjects(new Sobject[]{acc}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
} 

private static testmethod void undeleteObjectsTest2(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')});
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		UtilDML_Master.deleteObjects(new Sobject[]{acc});
		system.assert([Select id From Account].size() == 0);
		UtilDML_Master.undeleteObjects(new Sobject[]{acc});
		system.assert([Select id From Account].size() == 1);
	Test.stopTest();
} 

//bypass triggers
private static testmethod void insertObjectsTest6(){
	Test.startTest(); 
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'None');
		system.assert([Select id From Account].size() == 1);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
}  

private static testmethod void updateObjectsTest5(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];

		Debug_Error__c[] errors = new Debug_Error__c[]{};
		UtilDML_Master.updateObjects(new Sobject[]{acc}, false, null, errors, true, UtilDML_Master.TRIGGER_UPDATE);
		system.assert([Select id From Account ].size() == 1);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_UPDATE) == false);
	Test.stopTest();
} 

private static testmethod void deleteObjectsTest3(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		UtilDML_Master.deleteObjects(new Sobject[]{acc}, false, null, null, false, UtilDML_Master.TRIGGER_DELETE);
		system.assert([Select id From Account].size() == 0);
		system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
	Test.stopTest();
} 

private static testmethod void processRecordsByBatchTest1(){
	Test.startTest();
		UtilDML_Master.insertBatchObjects(new Sobject[]{new Account(name='Acc1')}, true, null, null, false, 'ALL');		
	Test.stopTest();
	system.assert([Select id From Account].size() == 1);
}  

private static testmethod void processRecordsByBatchTest2(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		acc.BillingPostalCode='SW123DF';
		UtilDML_Master.updateBatchObjects(new Sobject[]{acc}, false, null, null, false, 'ALL');
	Test.stopTest();
	system.assert([Select id From Account where BillingPostalCode='SW123DF'].size() == 1);
}  

private static testmethod void processRecordsByBatchTest3(){
	Test.startTest();
		UtilDML_Master.insertObjects(new Sobject[]{new Account(name='Acc1')}, false, null, null, false, 'ALL');
		system.assert([Select id From Account].size() == 1);
		Account acc = [Select id, BillingPostalCode From Account limit 1];
		UtilDML_Master.deleteBatchObjects(new Sobject[]{acc}, false, null, null, false, UtilDML_Master.TRIGGER_DELETE);		
	Test.stopTest();
	system.assert([Select id From Account].size() == 0);
} 


}