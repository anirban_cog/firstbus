public class TriggerController {

    public static map<TriggerControlKeyValue, boolean> triggerDisableMap = new map<TriggerControlKeyValue, boolean>();
    public static map<TriggerControlKeyValue, boolean> triggerSuccessMap = new map<TriggerControlKeyValue, boolean>();
    
    public static final String TRIGGER_NONE = 'NONE';
    public static final String TRIGGER_ALL = 'ALL';
    public static final String TRIGGER_INSERT = 'INSERT';
    public static final String TRIGGER_UPDATE = 'UPDATE';
    public static final String TRIGGER_DELETE = 'DELETE';
    public static final String TRIGGER_UNDELETE = 'UNDELETE';
    
    public static Boolean getTriggerControlValue(System.Type objType, String triggerType){
        TriggerControlKeyValue tkv = new TriggerControlKeyValue(objType ,triggerType);
        Boolean triggerDisable = false;
        if (triggerDisableMap != null && triggerDisableMap.containskey(tkv))
            triggerDisable = triggerDisableMap.get(tkv);
            
        return triggerDisable; 
    }

    public static void setTriggerControlValue(System.Type objType, String triggerType, Boolean triggerDisable){
        TriggerControlKeyValue tkv = new TriggerControlKeyValue(objType ,triggerType);
                    
        for (TriggerControlKeyValue eachtk : triggerDisableMap.keyset()){
            if (eachtk == tkv){
                tkv = eachtk;                   
                break;
            }
        }
        triggerDisableMap.put(tkv, triggerDisable);     
    }
    
    public static Boolean getTriggerSuccessValue(System.Type objType, String triggerType){
        TriggerControlKeyValue tkv = new TriggerControlKeyValue(objType ,triggerType);
        Boolean triggerSuccess = false;
            

        for (TriggerControlKeyValue eachtk : triggerSuccessMap.keyset()){
            if (eachtk == tkv){
                triggerSuccess = triggerSuccessMap.get(eachtk);
                break;
            }
        }
        
        return triggerSuccess; 
    }
    
    
public static boolean globalTriggerControlSetting(){        
    return (((Triggers_Off__c.getOrgDefaults() != null) ? Triggers_Off__c.getOrgDefaults().value__c : false) || Triggers_Off__c.getInstance(UserInfo.getUserId()).value__c  || Triggers_Off__c.getInstance(UserInfo.getProfileId()).value__c) ;
}

public static boolean globalTriggerPerObjectControlSetting(String obj){
    if (obj != null && obj != '') {
        try{
            if (!obj.endswith('__c')) obj += '__c';
                boolean s = false;
                if (Trigger_Per_Object__c.getOrgDefaults() != null && Trigger_Per_Object__c.getOrgDefaults().get(obj) != null) 
                    s =  (boolean)Trigger_Per_Object__c.getOrgDefaults().get(obj);
    
                boolean t = false;
                if (Trigger_Per_Object__c.getInstance(UserInfo.getUserId()) != null && Trigger_Per_Object__c.getInstance(UserInfo.getUserId()).get(obj) != null) 
                    t =  (boolean)Trigger_Per_Object__c.getInstance(UserInfo.getUserId()).get(obj);
    
                boolean u = false;
                if (Trigger_Per_Object__c.getInstance(UserInfo.getProfileId()) != null && Trigger_Per_Object__c.getInstance(UserInfo.getProfileId()).get(obj) != null)
                    u =  (boolean)Trigger_Per_Object__c.getInstance(UserInfo.getProfileId()).get(obj);
                
                return (s || t || u);
        }
        catch(Exception ex){
            return false;
        }
    }else 
        return false;
}

}