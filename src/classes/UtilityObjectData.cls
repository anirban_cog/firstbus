public class UtilityObjectData { 
    public static Sobject setObjData(Sobject aobj, KeyValue[] kVals){
        Sobject thisobj;
        if (kVals != null){
            for (KeyValue eachval : kVals){
                try{
                    thisobj = setFieldVal(aobj, eachval);
                }
                catch(Exception ex){system.debug('## ex ' + ex); }
            }
        } 
        return thisobj;
    } 
    public static Sobject setFieldVal(Sobject obj, KeyValue thisKeyVal){ 
        if(thisKeyVal.fieldtype == 'DATE'){ 
            obj.put(thisKeyVal.key, Date.valueOf(thisKeyVal.value.substring(0,10).trim()));
        }else if(thisKeyVal.fieldtype == 'DATETIME'){ 
            obj.put(thisKeyVal.key, Datetime.valueOf(thisKeyVal.value));
        }else if(thisKeyVal.fieldtype == 'DECIMAL'){ 
            obj.put(thisKeyVal.key, Decimal.valueof(thisKeyVal.value));
        }else if(thisKeyVal.fieldtype == 'INTEGER'){ 
            obj.put(thisKeyVal.key, Integer.valueof(thisKeyVal.value));
        }else if(thisKeyVal.fieldtype == 'LONG'){ 
            obj.put(thisKeyVal.key, Long.valueof(thisKeyVal.value));
        }else if(thisKeyVal.fieldtype == 'DOUBLE'){ 
            obj.put(thisKeyVal.key, Double.valueof(thisKeyVal.value));
        }else if(thisKeyVal.fieldtype == 'BOOLEAN'){ 
            obj.put(thisKeyVal.key, ((thisKeyVal.value).toUpperCase() == 'TRUE'));
        }else if(thisKeyVal.fieldtype == 'ID'){ 
            obj.put(thisKeyVal.key, ((ID) thisKeyVal.value));
        }else{// String 
            obj.put(thisKeyVal.key, thisKeyVal.value);
        } 
        return obj;
    } 
 
}