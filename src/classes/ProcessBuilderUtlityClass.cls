/*************************************************************
* Author: Ayan Hore
* Created Date: 11/09/17
* Version1: This is the initial version
* Details: Utility class which includes methods to be invoked by process builders 
* Purpose: Utility class which includes methods to be invoked by process builders 
**************************************************************/
public without sharing class ProcessBuilderUtlityClass {
    @InvocableMethod(label='UpsertContact' description='Creates or updates Account information to Contact')
    public static void upsertContact(List<Account> accounts) {
        List<Contact> contactList = new List<Contact>();
        List<AccountToContactMapping__c> mappingCS = AccountToContactMapping__c.getall().values();
        List<Id> accountIdList = new List<Id>();
        for(Account accObj : accounts){
            accountIdList.add(accObj.Id);
        }
        
        Map<Id,Id> accountContactMap = new Map<Id,Id>();
        for(Contact obj : [select Id,Account_External_Id__c from Contact where Account_External_Id__c IN :accountIdList]){
            accountContactMap.put(obj.Account_External_Id__c, obj.Id);
        }
        for(Account accObj : accounts){
            if(accObj.IsPersonAccount){
                Contact tempContact = new Contact();
                for(AccountToContactMapping__c obj : mappingCS){
                    if(obj.Account_API__c == 'Gender__c' && accObj.get(obj.Account_API__c) == 'Decline to state')
                        tempContact.put(obj.Contact_API__c,'Prefer not to say');
                    else
                      tempContact.put(obj.Contact_API__c,accObj.get(obj.Account_API__c));
                }
                
                /*Additional Processing Start*/
                if(!String.isBlank(accObj.Reference_ID__c)){
                    if(!String.isBlank(accObj.Source__c) && accObj.Source__c.containsIgnoreCase('Eticket'))
                        tempContact.put('Web_Reference_ID__c',accObj.Reference_ID__c);
                    else if(accObj.Source__c.containsIgnoreCase('MTicket'))
                        tempContact.put('MTickets_Reference_ID__c',accObj.Reference_ID__c);
                }
                if(!String.isBlank(accObj.Phone)){
                    if(accObj.Phone.startsWith('01') || accObj.Phone.startsWith('02') ||accObj.Phone.startsWith('44') || accObj.Phone.startsWith('+44')){
                        /*Phone*/
                        tempContact.put('Phone',accObj.Phone);
                        tempContact.put('MobilePhone',null);
                    }
                    else if (accObj.Phone.startsWith('07') || accObj.Phone.startsWith('08')){
                        /*Mobile*/
                        tempContact.put('Phone',null);
                        tempContact.put('MobilePhone',accObj.Phone);
                    }
                }
                //Start Defect I-03135 fix | Paramita | Date : 4th Dec, 2017 | mapping account source to contact source : if account source is null then set the contact source as "Prospect" + if account source is not null and having value "Prospect" or "MTicket" or "Wifi" then put the value in contact source as is + if account source is not null but having value other that "Prospect" or "MTicket" or "Wifi" then put "Web" in contact source.
                if(accObj.Source__c == null || accObj.Source__c == '')
                    tempContact.Source__c = 'Prospect';
                else if(accObj.Source__c == 'Prospect' || accObj.Source__c == 'Wifi' || accObj.Source__c == 'MTicket')
                    tempContact.Source__c = accObj.Source__c;
                else
                    tempContact.Source__c = 'Web';
                //End Defect I-03135 fix
                /*Additional Processing End*/
                if(accountContactMap.containsKey(accObj.Id)){
                    tempContact.put('Id',accountContactMap.get(accObj.Id));
                }
                contactList.add(tempContact);
            }
        }
        
        if(contactList.size()>0 && Schema.sObjectType.Contact.isUpdateable())
            upsert contactList;
        system.debug('Contact created/updated : '+contactList);
    }
    
}