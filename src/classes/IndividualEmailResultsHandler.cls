/*************************************************************
* Author: Paramita Roy Halder
* Created Date: 01/03/2018
* Version1: B-16873 and B-16874
* Details: Handler class to be invoked from Individual Email Results trigger
* Purpose: Trigger handler class
**************************************************************/
public with sharing class IndividualEmailResultsHandler extends TriggerBaseHandler{
    //the handler passes to each Logic class to perform whatever actions this allows the handler to have multiple logic
    // classes for different purposes and sobj that this class doesnt grow exponentially
    private IndividualEmailResultsLogic individualEmailResultsLogic;
    
    public IndividualEmailResultsHandler () {
        //Note can create multiple instances of different classes if required to segregate functionality into appropriate classes
        this.individualEmailResultsLogic = new IndividualEmailResultsLogic();
    }

    private static IndividualEmailResultsHandler instance; 

    public static IndividualEmailResultsHandler getInstance() {
        System.debug(LoggingLevel.FINE, 'Executing static method " IndividualEmailResultsHandler.getInstance"...');

        if (instance == null) instance = new IndividualEmailResultsHandler();
        return instance;
    } 
    
    public override void beforeInsert(SObject[] newObjects, CommitHandler ch){
        individualEmailResultsLogic.onBeforeInsert((et4ae5__IndividualEmailResult__c[])newObjects, ch); 
    }
    
    public override void afterInsert(Sobject[] newObjects, Map<id,SObject> newmap, CommitHandler ch){
        individualEmailResultsLogic.onAfterInsert((et4ae5__IndividualEmailResult__c[])newObjects, newmap, ch); //ch can optionally be passed to the logic class if DML Consolidation is required or not
    }
    
    public override void beforeUpdate(SObject[] oldObjects, SObject[] newObjects, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){
        individualEmailResultsLogic.onBeforeUpdate((et4ae5__IndividualEmailResult__c[])oldObjects, (et4ae5__IndividualEmailResult__c[])newObjects, oldmap, newmap, ch);
    }
    
    public override void afterUpdate(SObject[] oldObjects, SObject[] newObjects, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){
        individualEmailResultsLogic.onAfterUpdate((et4ae5__IndividualEmailResult__c[])oldObjects, (et4ae5__IndividualEmailResult__c[])newObjects, oldmap, newmap, ch);
    }
    
    public override void beforeDelete(SObject[] oldObjects, Map<id,SObject> oldmap, CommitHandler ch){ 
        individualEmailResultsLogic.onBeforeDelete((et4ae5__IndividualEmailResult__c[])oldObjects, oldmap, ch);
    } 
    
    public override void afterDelete(SObject[] oldObjects, Map<id,SObject> oldmap, CommitHandler ch){
        individualEmailResultsLogic.onAfterDelete((et4ae5__IndividualEmailResult__c[])oldObjects, oldmap, ch);
    } 
    
    public override void beforeUnDelete(SObject[] oldObjects, Map<id,SObject> oldmap, CommitHandler ch){ 
        individualEmailResultsLogic.onBeforeUnDelete((et4ae5__IndividualEmailResult__c[])oldObjects, oldmap, ch);
    } 
    
    public override void afterUnDelete(SObject[] oldObjects, SObject[] newObjects, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){
        individualEmailResultsLogic.onAfterUnDelete((et4ae5__IndividualEmailResult__c[])oldObjects, (et4ae5__IndividualEmailResult__c[])newObjects, oldmap, newmap, ch);
    } 

}