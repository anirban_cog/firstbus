@istest
public class TriggerControllerTest {
	public static TestDataComplexFunctions td = new TestDataComplexFunctions();
	public static Triggers_Off__c trig; 
	public static Trigger_Per_Object__c trigPerObject; 
	
	@testsetup private static void CreateTestData(){
        List<AccountToContactMapping__c> CSObjList = new List<AccountToContactMapping__c>();
        CSObjList.add(new AccountToContactMapping__c(Name='Record-16',Account_API__c='Id',Contact_API__c='Account_External_Id__c'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-20',Account_API__c='FirstName',Contact_API__c='FirstName'));
        CSObjList.add(new AccountToContactMapping__c(Name='Record-15',Account_API__c='LastName',Contact_API__c='LastName'));
        insert CSObjList;

		List<KeyValue> accountKV = new List<KeyValue>();
        accountKV.add(new KeyValue('FirstName','FirstBus','String'));
        accountKV.add(new KeyValue('LastName','Account','String'));
        accountKV.add(new KeyValue('Email__c','account@test.com','String')); 

		Account acc = td.insertAccount(accountKV, null);
	}
	/*
	static testMethod void TransactionProcessorTriggerTest1() {
		//test global CS on/off
		TransactionProcessorTest.createTestData1();
		Account acc = [Select Name From Account limit 1];
		trig = td.insertTriggersOff(null,null);
		Test.startTest();
			//record should be inserted			
			system.assert(TriggerController.getTriggerSuccessValue(Transaction_Processor__c.class,TriggerController.TRIGGER_INSERT) == true);
			
			//should change
			acc.Name = 'ChangeCusName';
			update acc;			
			system.assert(TriggerController.getTriggerSuccessValue(Transaction_Processor__c.class,TriggerController.TRIGGER_UPDATE) == true);

			//reset			
			TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(Transaction_Processor__c.class, TriggerController.TRIGGER_UPDATE), false);
			
			trig.value__c = true;
			update trig;
			
			//should not change
			acc.Name = 'DefaultCusName';
			update acc;			
			system.assert(TriggerController.getTriggerSuccessValue(Transaction_Processor__c.class,TriggerController.TRIGGER_UPDATE) == false);
			
			//disable insert
			
		test.stopTest();
	}*/

	static testMethod void AccountTriggerPerObjectTest() {
		//test trigger control using Per Object CS
		trigPerObject = td.insertTriggersPerObject(null,null);
		Test.startTest();
			//reset
			TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_INSERT), true);
			
			//record should be inserted	but shouldnt set Account_Insert_Succeeded 
			trigPerObject.Account__c = true;
			update trigPerObject;
			
			//reset			
			TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_INSERT), false);
			
			//system.assertequals(1,[Select id From Account].size());
			
			Account acc2 = td.insertAccount(null, null);
	
			//system.assert([Select id From Account].size() == 2);
			//system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);

		test.stopTest();
	}	
/*
	static testMethod void AccountTriggerTest2() {
		//test trigger control using static variables
		Account acc = [Select Name From Account limit 1];
		Test.startTest();
			//disable update
			TriggerController.triggerDisableMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_ALL), true);
			
			acc.Name = 'ChangeCusName';
			update acc;			
			system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_UPDATE) == false);

			//reset
			TriggerController.triggerDisableMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_ALL), false);
			TriggerController.triggerDisableMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_UPDATE), true);
			
			acc.Name = 'DefaultCusName';
			update acc;			
			
			//should not change
			system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_UPDATE) == false);
			
			//update should run
			TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_UPDATE), false);
			TriggerController.triggerDisableMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_UPDATE), false);
			acc.Name = 'DefaultCusName';
			update acc;
			system.debug('## TriggerController ' + TriggerController.triggerSuccessMap);			
			system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_UPDATE) == true);
			
			//test insert trigger code off
			TriggerController.triggerSuccessMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_INSERT), false);
			TriggerController.triggerDisableMap.put(new TriggerControlKeyValue(Account.class, TriggerController.TRIGGER_INSERT), true);
			system.assert([Select id From Account].size() == 1);
			
			Account acc2 = td.insertAccount(null, null);
	
			system.assert([Select id From Account].size() == 2);
			//should not change
			system.assert(TriggerController.getTriggerSuccessValue(Account.class,TriggerController.TRIGGER_INSERT) == false);
			
		test.stopTest();
	}
*/
}