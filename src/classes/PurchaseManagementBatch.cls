/*************************************************************
* Author: Ayan Hore
* Created Date: 11/23/17
* Version1: B-16753, B-16813
* Details: Purchase management batch job 
* Purpose: Convert Purchase Processor to Purchase Big Object and update summary information
**************************************************************/
public class PurchaseManagementBatch implements Database.Batchable<sObject>{
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Constants.PurchasesBatchQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        Map<Id,Purchases_Processor__c> newmap = new Map<Id,Purchases_Processor__c>();
        for(SObject obj : scope){
            Purchases_Processor__c tempObj = (Purchases_Processor__c) obj;
            newmap.put(tempObj.Id,tempObj);
        }
        TransactionManagementLogic transactionLogic = new TransactionManagementLogic();
        
        //Create a map of purchases processor  to transaction BO
        Map<Id,Id> purchaseToTransBOMap = new Map<Id,Id>();
        Map<Id,Transaction_Processor__c> transMap = new Map<Id,Transaction_Processor__c>();
        List<Id> listOfTransProcessIds = new List<Id>();
        for(Purchases_Processor__c obj : newmap.values()){
            listOfTransProcessIds.add(obj.Transaction__c);
        }
        
        transMap = transactionLogic.getTransMap(listOfTransProcessIds);
        purchaseToTransBOMap = transactionLogic.getPurchaseToTransactionMap(newmap,transMap);
        List<Purchase__b> purchaseBOList = new List<Purchase__b>();
        purchaseBOList = transactionLogic.upsertPurchaseBO(newmap.values(), purchaseToTransBOMap);
        
        // Insert purchase BO
        try{
            if(!purchaseBOList.isEmpty() && Schema.sObjectType.Purchase__b.isCreateable() && !Test.isRunningTest())
                database.insertImmediate(purchaseBOList);
        }
        catch(Exception e){
            system.debug('ERROR : '+e.getMessage());
        }
        
        List<Purchases_Processor__c> purchaseTransactionToBeUpdated = new List<Purchases_Processor__c>();
        for(Purchase__b bo : purchaseBOList){
            purchaseTransactionToBeUpdated.add(new Purchases_Processor__c(id=bo.Purchases_Processor__c, Processed__c = true));
        }
        if(!purchaseTransactionToBeUpdated.isEmpty() && Schema.sObjectType.Purchases_Processor__c.isUpdateable())
            update purchaseTransactionToBeUpdated;
        
        try{ 
            transactionLogic.updateSummaryInformation(transMap,newmap);
        }
        catch(Exception e){
            system.debug('ERROR : '+e.getMessage());
        }
        
    }
    
    public void finish(Database.BatchableContext BC){
        //Schedule the chain to execute again after 3 hrs
        Integer scheduledJobCount = [SELECT count() FROM CronTrigger where CronJobDetail.Name = :Constants.TransactionManagementChainName];
        if(scheduledJobCount>0)
            system.debug('JOB IS ALREADY SCHEDULED, HENCE NOT SCHEDULING ONCE MORE');
        else{
            system.debug('JOB IS NOT SCHEDULED, HENCE SCHEDULING ONE NOW');
            System.scheduleBatch(new TransactionManagementBatch(), Constants.TransactionManagementChainName, Constants.TransactionManagementJobInterval);
        }
    }
}