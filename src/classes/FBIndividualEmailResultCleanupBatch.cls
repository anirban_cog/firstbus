global class FBIndividualEmailResultCleanupBatch implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug('**************Hello1**************');
        String query = ''; 
        query ='SELECT CreatedDate,Id, et4ae5__Opened__c FROM et4ae5__IndividualEmailResult__c WHERE et4ae5__Opened__c = false';   
        query += !Test.isRunningTest() ? ' AND et4ae5__DateOpened__c = null' : '';
        system.debug('**************query**************'+ query);
        return database.getQueryLocator(query);
   }

    global void execute(Database.BatchableContext BC, List<et4ae5__IndividualEmailResult__c> lstEmailResults){
        system.debug('**************Hello2**************');
        system.debug('**************lstEmailResults**************' + lstEmailResults);
        system.debug('**************lstEmailResults.size()**************' + lstEmailResults.size());

        String dateRangeForIndividualEmailResult = ContactCleanupSettings__c.getInstance('DateRangeForIndividualEmailResult').Value__c;
        Integer indEmailResultDateRange = Integer.valueof(dateRangeForIndividualEmailResult);         
        Date today = DateTime.now().date();
        system.debug('**************indEmailResultDateRange**************' + indEmailResultDateRange);
        
        List<et4ae5__IndividualEmailResult__c> lstIndEmailResultToBeDeleted = new List<et4ae5__IndividualEmailResult__c>();
        if(lstEmailResults != null && lstEmailResults.size() > 0)
        {
            for(et4ae5__IndividualEmailResult__c ier : lstEmailResults)
            {
                system.debug('**************ier**************' + ier);
                system.debug('**************ier.CreatedDate**************' + ier.CreatedDate);
                DateTime tempDtTime = ier.CreatedDate.addDays(indEmailResultDateRange);
                system.debug('**************tempDtTime**************' + tempDtTime);
                Date emailResultDtRange = tempDtTime.date();//Date.newinstance(tempDtTime.year(), tempDtTime.month(), tempDtTime.day());
                system.debug('**************emailResultDtRange**************' + emailResultDtRange);
                if(today > emailResultDtRange)
                {
                    lstIndEmailResultToBeDeleted.add(ier);
                }
            }
        }        
        system.debug('**************lstIndEmailResultToBeDeleted**************' + lstIndEmailResultToBeDeleted);
        system.debug('**************lstIndEmailResultToBeDeleted.size()**************' + lstIndEmailResultToBeDeleted.size());        
        
        Database.DeleteResult[] indEmailResultList = Database.delete(lstIndEmailResultToBeDeleted, false);
        handleDeleteResultList(indEmailResultList);
        
    }

    global void finish(Database.BatchableContext BC){
       system.debug('**************Hello3**************');
    }
   
    private void handleDeleteResultList(Database.DeleteResult[] resultList)
    {
        for (Database.DeleteResult result : resultList) 
        {
            if (result.isSuccess()) 
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted record. Record ID: ' + result.getId());
            }
            else 
            {
                // Operation failed, so get all errors                
                for(Database.Error err : result.getErrors()) 
                {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }    
    }
}