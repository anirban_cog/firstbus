public class UtilDML { 
    
    public static list<Debug_Error__c> returnDMLErrorRecords(Database.Saveresult[] Saveres, String[] errorLst){
        list<Debug_Error__c> errors = new list<Debug_Error__c>(); 
        for(Database.SaveResult sr : Saveres) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors 
                for(Database.Error err : sr.getErrors()) {
                    errors = buildErrors(errors, err, errorLst);
                }
            }
        } 
        return errors;
    } 
    public static list<Debug_Error__c> returnDMLErrorRecords(Database.Upsertresult[] Saveres, String[] errorLst){
        list<Debug_Error__c> errors = new list<Debug_Error__c>(); 
        for(Database.Upsertresult sr : Saveres) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors 
                for(Database.Error err : sr.getErrors()) {
                    errors = buildErrors(errors, err, errorLst);
                }
            }
        } 
        return errors;
    } 
    public static list<Debug_Error__c> returnDMLErrorRecords(Database.Deleteresult[] Delres, String[] errorLst){
        list<Debug_Error__c> errors = new list<Debug_Error__c>(); 
        for(Database.Deleteresult sr : Delres) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors 
                for(Database.Error err : sr.getErrors()) {
                    errors = buildErrors(errors, err, errorLst);
                }
            }
        } 
        return errors;
    } 
    public static list<Debug_Error__c> returnDMLErrorRecords(Database.Undeleteresult[] Delres, String[] errorLst){
        list<Debug_Error__c> errors = new list<Debug_Error__c>(); 
        for(Database.Undeleteresult sr : Delres) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors 
                for(Database.Error err : sr.getErrors()) {
                    errors = buildErrors(errors, err, errorLst);
                }
            }
        } 
        return errors;
    }
    
    public static list<Debug_Error__c> buildErrors(Debug_Error__c[] errs, Database.Error err, String[] errorLst){
        Debug_Error__c e = new Debug_Error__c();
        e.Error__c = err.getStatusCode() + ': ' + err.getMessage();
        if (errorLst != null) errorLst.add(e.Error__c);//this is optional
        errs.add(e);

    	return errs;
    }
}