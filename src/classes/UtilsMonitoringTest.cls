@istest
public class UtilsMonitoringTest {

//MonitoringCoverage__c not active and Monitoring__c is not set then no messages should be outputted

//MonitoringCoverage__c not active and Monitoring__c is set but Active__c = false then no messages should be outputted

//MonitoringCoverage__c not active and Monitoring__c is set but Active__c = true but Monitor_Datetime_From__c  and Monitor_Datetime_To__c is out of range then no messages should be outputted

//MonitoringCoverage__c not active and Monitoring__c is set but Active__c = true then messages should be outputted but only for the Type specified in the CS

//MonitoringCoverage__c not active and Monitoring__c is set but Active__c = true, Monitor_Datetime_From__c  and Monitor_Datetime_To__c is in range then messages should be outputted but only for the Type specified in the CS
static TestDataComplexFunctions td = new TestDataComplexFunctions();
private static final String CONST_MONITORTYPE = 'AccountTrigger';
private static final string CONST_MONITOR_MESSAGE = 'Monitor Msg'; 

	static testMethod void setupMonitoringTest1() {
		td.insertAccount(null, null);
		test.starttest();
			UtilsMonitoring.setupMonitoring();
			system.assert(UtilsMonitoring.saveMonitoringMessagesMap == null);
		test.stoptest();
	}

	static testMethod void setupMonitoringTest2() { 
		td.insertMonitoring(null);
		td.insertAccount(null, null);
		test.starttest();
			//system.debug('## Debug_Error__c ' + [Select id,Name,Error__c From Debug_Error__c]);
			system.assertequals([Select id From Debug_Error__c ].size() , 0);
		test.stoptest();
	}

	static testMethod void setupMonitoringTest3() {
		test.starttest();
			UtilsMonitoring.setupMonitoring();
			UtilsMonitoring.buildMonitoringMessage(Account.class, CONST_MONITORTYPE, CONST_MONITOR_MESSAGE, null);
			system.assert(UtilsMonitoring.saveMonitoringMessagesMap == null);
			system.assert([Select id From Debug_Error__c ].size() == 0);
		test.stoptest();
	}

	static testMethod void recordMonitoringMessageTest() {
		td.insertMonitoring(null);
		test.starttest();
			UtilsMonitoring.setupMonitoring();
			UtilsMonitoring.buildMonitoringMessage(Account.class, CONST_MONITORTYPE, CONST_MONITOR_MESSAGE, null);
			
			system.assert(!UtilsMonitoring.saveMonitoringMessagesMap.isempty());
			system.assert((UtilsMonitoring.saveMonitoringMessagesMap).containskey(Account.class));
			system.assertequals((UtilsMonitoring.saveMonitoringMessagesMap).get(Account.class).get(CONST_MONITORTYPE)[0].msg , CONST_MONITOR_MESSAGE);
		test.stoptest();
	}

	static testMethod void saveMonitoringMesagesTest1() {
		td.insertMonitoring(null);
		test.starttest();
			UtilsMonitoring.setupMonitoring();
			UtilsMonitoring.buildMonitoringMessage(Account.class, CONST_MONITORTYPE, CONST_MONITOR_MESSAGE, null);
			
			system.assert(!UtilsMonitoring.saveMonitoringMessagesMap.isempty());
			system.assert((UtilsMonitoring.saveMonitoringMessagesMap).containskey(Account.class));
			system.assertequals(UtilsMonitoring.saveMonitoringMessagesMap.get(Account.class).get(CONST_MONITORTYPE)[0].msg , CONST_MONITOR_MESSAGE);

			UtilsMonitoring.saveMonitoringMesages(Account.class);
			DebugParent__c[] newDbgParents = [Select id, Name from DebugParent__c Order By Name];
			system.assertequals(newDbgParents.size(), 1);
			
			set<id> parentSet = new set<id>();
			for (DebugParent__c eachpr : newDbgParents)
				parentSet.add(eachpr.id);
				
			Debug_Error__c[] allErrs = [Select Error__c, Debug_Parent__c from Debug_Error__c where Debug_Parent__c In :parentSet];
			system.assertequals(allErrs.size(), 0);
			//system.assert(allErrs[0].Error__c != null);
		test.stoptest();
	}

	static testMethod void saveMonitoringMesagesTest2() {
		td.insertMonitoring(null);
		test.starttest();
			UtilsMonitoring.setupMonitoring();
			UtilsMonitoring.buildMonitoringMessage(Account.class, CONST_MONITORTYPE, CONST_MONITOR_MESSAGE, null);
			
			system.assert(!UtilsMonitoring.saveMonitoringMessagesMap.isempty());
			system.assert((UtilsMonitoring.saveMonitoringMessagesMap).containskey(Account.class));
			system.assertequals(UtilsMonitoring.saveMonitoringMessagesMap.get(Account.class).get(CONST_MONITORTYPE)[0].msg , CONST_MONITOR_MESSAGE);

			UtilsMonitoring.saveMonitoringMesages();
			DebugParent__c[] newDbgParents = [Select id, Name from DebugParent__c Order By Name];
			system.assertequals(newDbgParents.size(), 1);
			
			set<id> parentSet = new set<id>();
			for (DebugParent__c eachpr : newDbgParents)
				parentSet.add(eachpr.id);
				
			DebugParent__c[] prt = [Select id from DebugParent__c];
			system.assertequals(1, prt.size());
			Debug_Error__c[] allErrs = [Select Error__c, Debug_Parent__c from Debug_Error__c where Debug_Parent__c In :parentSet];
			system.assertequals(allErrs.size(), 0);
			//system.assert(allErrs[0].Error__c != null);
		test.stoptest();
	}
}