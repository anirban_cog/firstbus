public with sharing class CommitHandler extends UtilDML_Master{ 
    
    public enum Operation {INSERT_OPERATION, UPDATE_OPERATION, DELETE_OPERATION} 
    public Integer DEFAULT_UPDATE_LIST = 0; 
    private List<sObject> commitInsertList; 
    private List<sObject> commitUpdateList; 
    private List<sObject> commitDeleteList; 
    private Map<Schema.SObjectType, String> qryFieldBaselineMap = new Map<Schema.SObjectType, String>(); 
    private Map<Schema.SObjectType, ID[]> qryWhereClauseBaselineMap = new Map<Schema.SObjectType, ID[]>(); 
    private Map<Id,sObject> baseLineSObjs; 
    
    public CommitHandler(){ 
        this.commitInsertList = new List<sObject>(); 
        this.commitUpdateList = new List<sObject>(); 
        this.commitDeleteList = new List<sObject>(); 
    } 
    
    
    public void addToCommit(List<sObject> sObjList, String operation){
		if(sObjList != null && operation != null){
			if(operation.toUpperCase() == 'INSERT'){
				this.commitInsertList.addAll(sObjList);
			}else if(operation.toUpperCase() == 'UPDATE'){
				this.commitUpdateList.addAll(sObjList);
			}else if(operation.toUpperCase() == 'DELETE'){
				this.commitDeleteList.addAll(sObjList);		
			}
		}
	}
	
	public void mergedCommitToDataBase(){ 
        list<Debug_Error__c> errors = new list<Debug_Error__c>(); 
        
        //INSERTS 
        insertOrdering(errors); 
        
        if (!commitUpdateList.isempty()){ 
            // UPDATE LOGIC 
            //builds a map of queries per object for the baseline 
            Map<Schema.SObjectType, List<sObject>> commitUpdateMap = new Map<Schema.SObjectType, List<sObject>>(); 
            Schema.SObjectType sObjType; 
            Boolean objectUpdatesMultipleTimes = false; 
            Map<Id,sObject> tmpCommitUpdateMapByID = new Map<Id,sObject>(); 
            
            //groups all updates by object Type 
            for(sObject sObj : commitUpdateList){ 
                sObjType = sObj.getSObjectType(); 
                
                // if sObjType exists 
                if(!commitUpdateMap.containsKey(sObjType)) 
                    commitUpdateMap.put(sObjType, new List<sObject>()); 
                
                if(tmpCommitUpdateMapByID.containsKey(sObj.id)){ 
                    objectUpdatesMultipleTimes = true; 
                } 
                
                tmpCommitUpdateMapByID.put(sObj.Id, sObj); 
                commitUpdateMap.get(sObjType).add(sObj); 
            } 
            
            
            //baseline is only required when at least 2 separate updates dmls are to be made on the same object 
            //because only then can a record be potentially be changed back to the original value 
            if (objectUpdatesMultipleTimes) getBaselineQuery(commitUpdateMap); 
            
            //if(baseLineSObjs != null){ 
            
            // UPDATE - MERGE 
            Map<Id,sObject> commitUpdateMapByID = new Map<Id,sObject>(); 
            for(sObject sObj : commitUpdateList){ 
                if(commitUpdateMapByID.containsKey(sObj.id) ){ 
                    if (baseLineSObjs != null && baseLineSObjs.containsKey(sObj.id)) 
                        //sObj = CURRENT OBJECT 
                        //commitUpdateMapByID = PREVIOUS OBJECT 
                        commitUpdateMapByID.put(sObj.Id,mergeSObject(sObj, commitUpdateMapByID.get(sObj.id), baseLineSObjs.get(sObj.id), false)); 
                    else 
                        commitUpdateMapByID.put(sObj.Id, sObj); 
                }else{ 
                    commitUpdateMapByID.put(sObj.Id, sObj); 
                } 
            } 
            
            Map<Schema.SObjectType, List<sObject>> commitReorganiseUpdateMap = new Map<Schema.SObjectType, List<sObject>>(); 
            
            //To Do : Duplicate code from commitToDataBase() 
            for(sObject sObj : commitUpdateMapByID.values()){ 
                
                sObjType = sObj.getSObjectType(); 
                
                if(commitReorganiseUpdateMap.containsKey(sObjType)){ 
                    
                    commitReorganiseUpdateMap.get(sObjType).add(sObj); 
                }else{ 
                    
                    commitReorganiseUpdateMap.put(sObjType, new List<sObject>{sObj}); 
                } 
            } 
            
            // UPDATE 
            for(Schema.SObjectType sObjectType : commitReorganiseUpdateMap.keySet()){ 
                updateObjects(commitReorganiseUpdateMap.get(sObjectType), true, defaultBatchQuantity, errors, false, TRIGGER_NONE); 
            } 
        } 
        
        //DELETES 
        deleteOrdering(errors); 
        
        
        if (!errors.isempty()){ 
            insertObjects(errors, true, defaultBatchQuantity, null, false, TRIGGER_NONE); 
        } 
    } 
    
    
    //Helper functions for mergedCommitToDataBase() 
    public void getBaselineQuery(Map<Schema.SObjectType, List<sObject>> commitUpdateMap){ 
        Schema.DescribeFieldResult fd; 
        for (Schema.SObjectType eachType : commitUpdateMap.keyset()){ 
            sObject[] objList = commitUpdateMap.get(eachType); 
            for(sObject sObj : objList){ 
                String qryFields = ''; 
                if (qryFieldBaselineMap.containskey(sObj.getSobjectType())) 
                    qryFields = qryFieldBaselineMap.get(sObj.getSobjectType()) + ','; 
                
                Schema.SObjectType sobjectType = sObj.getSObjectType(); 
                Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(String.valueOf(sobjectType).toLowerCase()).getDescribe().Fields.getMap(); 
                for(Schema.SObjectField ft : fMap.values()){ 
                    
                    fd = ft.getDescribe(); 
                    
                    Boolean sObjQueried = false; 
                    String fieldName = ''; 
                    if(fd.isUpdateable()){ // fd.isAccessible() 
                        
                        fieldName = fd.getName(); 
                        
                        //To Do: Dont like setting through failure 
                        try{ String sObjMasterValue = String.valueOf(sObj.get(fieldName)); sObjQueried = true; }catch(Exception e){ sObjQueried = false; } 
                    } 
                    
                    if(sObjQueried && !qryFields.contains(fieldName)){ 
                        //this builds up a list of field names that need to be included in the baseline soql 
                        qryFields += fieldName + ','; 
                    } 
                } 
                qryFields = qryFields.removeEnd(','); 
                qryFieldBaselineMap.put(sObj.getSobjectType(), qryFields); 
                
                if (!qryWhereClauseBaselineMap.containskey(sObj.getSobjectType())) qryWhereClauseBaselineMap.put(sObj.getSobjectType(), new ID[]{}); 
                qryWhereClauseBaselineMap.get(sObj.getSobjectType()).add(sObj.id); 
            } 
        } 
        system.debug('##qryFieldBaselineMap ' + qryFieldBaselineMap); 
        system.debug('##qryWhereClauseBaselineMap ' + qryWhereClauseBaselineMap); 
        baseLineSObjs = new Map<Id,sObject>(); 
        
        for (Schema.SObjectType eachType : qryFieldBaselineMap.keyset()){ 
            Schema.DescribeSObjectResult r = eachType.getDescribe(); 
            String qry = 'Select ' + qryFieldBaselineMap.get(eachType) + ' From ' + r.getName(); 
            ID[] qryIds; 
            if (qryWhereClauseBaselineMap.containskey(eachType)) { 
                qryIds = qryWhereClauseBaselineMap.get(eachType); 
                qry += ' Where Id In :qryIds'; 
            } 
            system.debug('##qry ' + qry); 
            baseLineSObjs.putall(Database.query(qry)); 
        } 
        
        system.debug('##baseLineSObjs ' + baseLineSObjs); 
    } 
    
    public void insertOrdering(Debug_Error__c[] errs){ 
        //groups the orders per Sobject type because you cannot insert different Sobject types at the same time eg: Account and Contact 
        if (!commitInsertList.isempty()){ 
            Map<Schema.SObjectType, List<sObject>> commitInsertMap = new Map<Schema.SObjectType, List<sObject>>(); 
            // Loop variables init 
            Schema.SObjectType sObjType; 
            
            // INSERT PREPARE - order all the commitInsertList sObjs 
            for(sObject sObj : commitInsertList){ 
                
                sObjType = sObj.getSObjectType(); 
                
                if(commitInsertMap.containsKey(sObjType)){ 
                    
                    commitInsertMap.get(sObjType).add(sObj); 
                }else{ 
                    
                    commitInsertMap.put(sObjType, new List<sObject>{sObj}); 
                } 
            } 
            
            // INSERT 
            //To Do : need to check how many dmls 
            for(Schema.SObjectType sObjectType : commitInsertMap.keySet()){ 
                insertObjects(commitInsertMap.get(sObjectType), true, defaultBatchQuantity, errs, false, TRIGGER_NONE); 
            } 
        } 
    } 
    
    public void deleteOrdering(Debug_Error__c[] errs){ 
        if (!commitDeleteList.isempty()){ 
            Map<Schema.SObjectType, List<sObject>> commitDeleteMap = new Map<Schema.SObjectType, List<sObject>>(); 
            Schema.SObjectType sObjType; 
            
            // DELETE PREPARE - order all the commitDeleteList sObjs 
            for(sObject sObj : commitDeleteList){ 
                
                if(commitDeleteMap.containsKey(sObjType)){ 
                    
                    commitDeleteMap.get(sObjType).add(sObj); 
                }else{ 
                    
                    commitDeleteMap.put(sObjType, new List<sObject>{sObj}); 
                } 
            } 
            
            // DELETE 
            for(Schema.SObjectType sObjectType : commitDeleteMap.keySet()){ 
                deleteObjects(commitDeleteMap.get(sObjectType), true, defaultBatchQuantity, errs, false, TRIGGER_NONE); 
            } 
        } 
    } 
    
    public sObject mergeSObject(sObject sObjMaster, sObject sObj, sObject baseLineSObj, Boolean ignoreBaseLineSObj){ 
        //sObjMaster = Record to be changed 
        //sObj = Previous changed record held in memory 
        //baseLineSObj = original state of record already saved before trigger starts 
        
        //If the field being changed exists in sObjMaster, then regardless if it exists in sObj or not the value in sObjMaster is used and overwrites in sObj 
        //If the field does not exist in sObjMaster but does in sObj and the baseline has a value and is different to the baseline then sObjMaster is updated with the value in sObj 
        //If the field does not exist in sObjMaster and not in baseline but does in sObj then sObjMaster is updated with the value in sObj 
        
        Schema.SObjectType sobjectType = sObjMaster.getSObjectType(); 
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(String.valueOf(sobjectType).toLowerCase()).getDescribe().Fields.getMap(); 
        
        // Loop variables init 
        String sObjMasterValue; 
        Boolean sObjMasterQueried; 
        String baseLineSObjValue; 
        Boolean baseLineSObjQueried; 
        String sObjValue; 
        Boolean sObjQueried; 
        String fieldName; 
        Schema.DescribeFieldResult fd; 
        
        for(Schema.SObjectField ft : fMap.values()){ 
            
            fd = ft.getDescribe(); 
            
            
            if(fd.isUpdateable()){ // fd.isAccessible() 
                
                fieldName = fd.getName(); 
                
                //To Do: Dont like setting through failure 
                try{ sObjMasterValue = String.valueOf(sObjMaster.get(fieldName)); sObjMasterQueried = (sObjMasterValue != null);}catch(Exception e){ sObjMasterQueried = false; sObjMasterValue = null; 
                                                                                                                                                   } 
                try{ sObjValue = String.valueOf(sObj.get(fieldName)); sObjQueried = (sObjValue != null);}catch(Exception e){ sObjQueried = false; sObjValue = null; } 
                try{ baseLineSObjValue = String.valueOf(baseLineSObj.get(fieldName)); baseLineSObjQueried = (baseLineSObjValue != null);}catch(Exception e){ baseLineSObjQueried= false; baseLineSObjValue = null; 
                                                                                                                                                           } 
                
                
                
                if (!sObjMasterQueried && sObjQueried){ 
                    if (baseLineSObjQueried){ 
                        if(equals(sObjValue, baseLineSObjValue, fd.getType().Name()) == false) 
                            updateField(sObjMaster,fieldName, sObjValue, fd.getType().Name()); 
                    } 
                    else 
                        updateField(sObjMaster,fieldName, sObjValue, fd.getType().Name()); 
                } 
                
            } 
        } 
        
        return sObjMaster; 
    } 
    
    private static void updateField(sObject sObj, String fieldName, String newfieldValue, String fieldType){ 
        
        
        if(fieldType == 'DATE'){ 
            sObj.put(fieldName, Date.valueOf(newfieldValue)); 
        }else if(fieldType == 'DATETIME'){ 
            
            sObj.put(fieldName, Datetime.valueOf(newfieldValue)); 
        }else if(fieldType == 'DECIMAL'){ 
            
            
            sObj.put(fieldName, Decimal.valueOf(newfieldValue)); 
        }else if(fieldType == 'INTEGER'){ 
            
            sObj.put(fieldName, Integer.valueOf(newfieldValue)); 
        }else if(fieldType == 'LONG'){ 
            
            sObj.put(fieldName, Long.valueOf(newfieldValue)); 
        }else if(fieldType == 'DOUBLE'){ 
            
            sObj.put(fieldName, Double.valueOf(newfieldValue)); 
        }else if(fieldType == 'BOOLEAN'){ 
            
            sObj.put(fieldName, (newfieldValue.toUpperCase() == 'TRUE')); 
        }else{// String, picklist 
            
            sObj.put(fieldName, newfieldValue); 
        } 
    } 
    
    private static Boolean equals(String fieldValue1, String fieldValue2, String fieldType){ 
        
        if(fieldValue1 == null && fieldValue2 == null){ 
            return true; 
        }else if(fieldValue1 != null && fieldValue2 == null){
            return false; //neturn statemnet is missing 
        }
        return false;   // retrun statement is missing
    }  
}