public with sharing class TestDataJsonLibrary { 
    
    public class Standard{ 
        public Date todaysDate= System.today(); 
        public Id personAccountRecordTypeId	= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        //public final Map<String, String> jsonMap = new Map<String, String>{'ACCOUNT' => '{"attributes":{"type":"Account"},"Name":"Test Account Data","ShippingState":"Test State"}'};
        
        public String accountJSONString = '{"attributes":{"type":"Account"},"Name":"Test ClassAccount","LastName":"ClassAccount","FirstName":"Test","Salutation":"Mr.","RecordTypeId":'+'"'+personAccountRecordTypeId+'"'+',"BillingStreet":"London","BillingCity":"London","BillingState":"London","BillingCountry":"United Kingdom","BillingAddress":{"city":"London","country":"United Kingdom","geocodeAccuracy":null,"latitude":null,"longitude":null,"postalCode":null,"state":"London","street":"London"},"ShippingAddress":null,"IsPersonAccount":true,"PersonMailingAddress":null,"Phone":"+4414568978","PersonEmail":"testaccount@firstbus.co.uk","PersonHasOptedOutOfEmail":false,"CelerityBusinessKey__c":"Indiv-000","Age_Range__c":"35","Like_to_receive_latest_offers__c":true,"Total_Purchase_Count__c":5,"FirstPurchaseDate__c":"2017-11-13","LastPurchaseDate__c":"2017-11-30","University__c":"Test University","SupplierBusinessKey__c":"Web","Like_to_receive_offers__c":false,"Reference_ID__c":"Ref-000","How_often_do_you_travel_per_week__c":"3","Gender__c":"Male","Organisation_1__c":"Bath","Organisation_2__c":"Bath","Organisation_3__c":"Bath","Archive__c":false,"Last_Purchase_Count__c":-17,"First_Purchase_Type__c":"Temp","Last_Purchase_Type__c":"Temp","Purchase_Count__c":5,"Source__c":"Web","Ticket_Activation_Date__c":"2017-11-30","Purpose_of_journey__c":"Testing","Organisation_4__c":"Bath","Organisation_5__c":"Bath","Source_Created_Date__c":"2017-11-14T12:50:00.000+0000","Source_Amended_Date__c":"2017-11-22T12:50:00.000+0000","Reason_for_use__c":"Testing","Amended_Transaction_Date__c":"2017-11-29T12:50:00.000+0000","Frequency_of_use__c":"Work","MailingList1__c":"MailingList-1","MailingList2__c":"MailingList-3","MailingList3__c":"MailingList-3","Control_Group__c":true,"Graduation_year__c":2011,"Group_Opt_In__c":true,"Customer_First_Hub_Member__pc":true}';
        public String contactJSONString = '{	"attributes": {		"type": "Contact"	},	"Name": "Test Contact ", "Duplicate__c": false, "LastName": "TestLast"}';
        public String caseJSONString = '{    "attributes": {      "type": "Case"    },        "Type": "Complaint",    "Status": "New",    "Origin": "Other third party",    "Priority": "Medium",    "IsClosed": false,    "ContactId": "0030E00000FJCv9QAH"  }';
        
        public Map<String, String> jsonMap = new Map<String, String>{
            'ACCOUNT' => accountJSONString,
                'CONTACT' => contactJSONString,
                'CASE' => caseJSONString,
                'TRIGGERS_OFF__C'		=> 	'[{"attributes":{"type":"Triggers_Off__c"},"value__c":false}]',
                'TRIGGER_PER_OBJECT__C'	=> 	'[{"attributes":{"type":"Trigger_Per_Object__c"},"Account__c":false}]'
                };
                    //jsonMap.put('ACCOUNT',accountJSONString);
                    
                    }
    
    public class Custom{ 
        public Date todaysDate= System.today();
        
        public String transactionProcessorJSONString = '{"attributes": {"type": "Transaction_Processor__c"},"Name": "transaction 1","Other_Street__c": "Sample","Other_City__c": "Other",	"Other_Postal_Code__c": "84396",	"Other_Country__c": "INDIA",	"Mailing_Postal_Code__c": "9264596",	"Mailing_City__c": "lahflah",	"Mailing_Street__c": "lahflha",	"Mailing_Country__c": "INDIA",	"First_Name__c": "first - 1"}';
        public String purchasesProcessorJSONString = '{	"attributes": {"type": "Purchases_Processor__c"},"Name": "purchase 1","Ticket_Category__c": "MTicket","Promotional_Code__c": "9865",	"Product_Issued__c": "2017-11-23T16:21:00.000+0000",	"Price__c": 50.00,	"Activation_Time__c": "2017-11-22T16:21:00.000+0000",	"Expiry_Time__c": "2017-11-30T16:21:00.000+0000",	"Device_Id__c": "123",	"TTL__c": 321,	"Ticket_Id__c": "1111"}';
        public String deviceIDJSONString = '{	"attributes": {"type": "Device_ID__c"},"AppVersion__c":"ApV1","C3_User_Id__c":"C3","Device__c":"D1","OS__c":"Android"}';
        public String individualEmailResultJSONString = '{"attributes": {"type": "et4ae5__IndividualEmailResult__c"}, "et4ae5__Contact__c": "0030E00000FJdJ0QAL",    "et4ae5__DateOpened__c": "2017-12-26T13:26:00.000+0000",    "et4ae5__Opened__c": true  }'; 
        public final Map<String, String> jsonMap = new Map<String, String>{
            Constants.CONST_CUSTOM_ACCOUNT => '{"attributes":{"type":"Account"},"XXX__c":"2","ShippingState":"Test State2"}',
                Constants.TP => transactionProcessorJSONString,
                Constants.PP => purchasesProcessorJSONString,
                Constants.DeviceID => deviceIDJSONString,
                Constants.IndividualEmailResult => individualEmailResultJSONString
                }; 
                    
                    } 
    
    public class StandardCustSettings{
        
        public final Map<String, String> custJsonMap = new Map<String, String>{
            'MONITORING__C'							=>	'[{"attributes":{"type":"Monitoring__c"},"Name":"AccountTrigger","Active__c":true}]',
                'TRIGGERS_OFF__C'						=> 	'[{"attributes":{"type":"Triggers_Off__c"},"value__c":false}]',
                'TRIGGER_PER_OBJECT__C'					=> 	'[{"attributes":{"type":"Trigger_Per_Object__c"},"Account__c":false}]',
                'MONITORINGCOVERAGE__C'					=>	'[{"attributes":{"type":"MonitoringCoverage__c"},"value__c":true}]'
                };
                    }
    
}