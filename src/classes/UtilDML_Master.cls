global with sharing virtual class UtilDML_Master {
    
    //the functions below will check governor limits and if they are going to be exceeded it will automatically check if the user has allowed the records to be processed by batch
    //if processByBatch=true and the context is currently not in a batch or a future the records will be processed by a batch and the user can specify a batch quantity
    //otherwise if limits are not going to be exceeded and the user has passed Debug_Error__c[] to the function then the records will be processed by Saveresult
    //any errors are returned by the function or if saveErrors=True then the erors are saved to the database directly from the function
    //only merge and convertlead are not included but these are not used that often and merge will require at least 2 separate objects to be passed which doesnt really work for the structure of this framework 
    global enum dmlType {INSERT_OBJECT,UPDATE_OBJECT,UPSERT_OBJECT,DELETE_OBJECT,UNDELETE_OBJECT}
    global static integer defaultBatchQuantity = 100;
    global static final String TRIGGER_NONE = 'None'; 
    global static final String TRIGGER_ALL = 'ALL';
    global static final String TRIGGER_INSERT = 'INSERT';
    global static final String TRIGGER_UPDATE = 'UPDATE';
    global static final String TRIGGER_DELETE = 'DELETE';
    global static final String TRIGGER_UNDELETE = 'UNDELETE';
    global static Debug_Error__c[] masterErrors; 
    global static final String NO_CREATE_PERMISSION= 'Has No Create Permission';
    global static final String NO_UPDATE_PERMISSION= 'Has No Update Permission';
    global static final String NO_DELETE_PERMISSION= 'Has No Delete Permission';
    global static final String NO_UNDELETE_PERMISSION= 'Has No UnDelete Permission'; 
    global static boolean insertObjects(Sobject[] sobj){
        //this is the default insert function which allows the user to pass fewer arguments 
        masterErrors = new Debug_Error__c[]{};
            return genericDML(sobj, true, 200, masterErrors, true, dmlType.INSERT_OBJECT, 'Insert Objects', TRIGGER_NONE);
    } 
    global static boolean insertObjects(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity, Debug_Error__c[] errors, Boolean saveErrors, String trigOff){
        return genericDML(sobj, processByBatch, batchQuantity, errors, saveErrors, dmlType.INSERT_OBJECT, 'Insert Objects', trigOff);
    } 
    global static boolean insertBatchObjects(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity, Debug_Error__c[] errors, Boolean saveErrors, String trigOff){
        return processRecordsByBatch(sobj, processByBatch, batchQuantity, errors, saveErrors, dmlType.INSERT_OBJECT, 'Insert Objects', trigOff);
    } 
    global static boolean updateObjects(Sobject[] sobj){
        masterErrors = new Debug_Error__c[]{};
            return genericDML(sobj, true, 200, masterErrors, true, dmlType.UPDATE_OBJECT, 'Update Objects', TRIGGER_NONE);
    } 
    global static boolean updateObjects(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity, Debug_Error__c[] errors, Boolean saveErrors, String trigOff){
        return genericDML(sobj, processByBatch, batchQuantity, errors, saveErrors, dmlType.UPDATE_OBJECT, 'Update Objects', trigOff);
    } 
    global static boolean updateBatchObjects(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity, Debug_Error__c[] errors, Boolean saveErrors, String trigOff){
        return processRecordsByBatch(sobj, processByBatch, batchQuantity, errors, saveErrors, dmlType.UPDATE_OBJECT, 'Update Objects', trigOff);
    } 
    global static boolean deleteObjects(Sobject[] sobj){
        masterErrors = new Debug_Error__c[]{};
            return genericDML(sobj, true, 200, masterErrors, true, dmlType.DELETE_OBJECT, 'Delete Objects', TRIGGER_NONE);
    } 
    global static boolean deleteObjects(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity, Debug_Error__c[] errors, Boolean saveErrors, String trigOff){
        return genericDML(sobj, processByBatch, batchQuantity, errors, saveErrors, dmlType.DELETE_OBJECT, 'Delete Objects', trigOff);
    } 
    global static boolean deleteBatchObjects(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity, Debug_Error__c[] errors, Boolean saveErrors, String trigOff){
        return processRecordsByBatch(sobj, processByBatch, batchQuantity, errors, saveErrors, dmlType.DELETE_OBJECT, 'Delete Objects', trigOff);
    } 
    global static boolean undeleteObjects(Sobject[] sobj){
        masterErrors = new Debug_Error__c[]{};
            return genericDML(sobj, true, 200, masterErrors, true, dmlType.UNDELETE_OBJECT, 'UnDelete Objects', TRIGGER_NONE);
    } 
    global static boolean undeleteObjects(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity, Debug_Error__c[] errors, Boolean saveErrors, String trigOff){
        return genericDML(sobj, processByBatch, batchQuantity, errors, saveErrors, dmlType.UNDELETE_OBJECT, 'UnDelete Objects', trigOff);
    } 
    global static boolean genericDML(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity,
                                     Debug_Error__c[] errors, Boolean saveErrors, dmlType thisDMLType, 
                                     String setErrorMsg, String trigOff){
        try{
            UtilsMonitoring.setupMonitoring();
            Boolean processMonitoringMsg = false; 
            Type convertedType = Type.forname(String.valueOf(sobj[0].getSobjectType()));
            if (trigOff != TRIGGER_NONE){
                //this allows you to bypass triggers to improve efficiency of processing
                if (trigOff == TRIGGER_ALL)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_NONE, true);
                else if (trigOff == TRIGGER_INSERT)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_INSERT, true);
                else if (trigOff == TRIGGER_UPDATE)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_UPDATE, true);
                else if (trigOff == TRIGGER_DELETE)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_DELETE, true);
                else if (trigOff == TRIGGER_UNDELETE)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_UNDELETE, true);
            } 
            if (Limits.getDmlStatements() <= (Limits.getLimitDmlStatements() - 1)){ 
                if (errors != null){
                    Database.saveresult[] res;
                    if (thisDMLType == dmlType.INSERT_OBJECT) {
                        if (sobj[0].getSObjectType().getDescribe().isCreateable()) {
                            res = Database.insert(sobj,false);
                            errors.addall(UtilDML.returnDMLErrorRecords(res, null));
                        }
                        else
                            errors.add(new Debug_Error__c(Error__c = sobj[0].getSObjectType().getDescribe().getName() + ' ' + NO_CREATE_PERMISSION));
                    }
                    else if (thisDMLType == dmlType.UPDATE_OBJECT){
                        if (sobj[0].getSObjectType().getDescribe().isUpdateable()) {
                            res = Database.update(sobj,false);
                            errors.addall(UtilDML.returnDMLErrorRecords(res, null));
                        }
                        else
                            errors.add(new Debug_Error__c(Error__c = sobj[0].getSObjectType().getDescribe().getName() + ' ' + NO_UPDATE_PERMISSION));
                    } 
                    else if (thisDMLType == dmlType.UPSERT_OBJECT){
                        return false;//NOT ALLOWED ON GENERIC SOBJECT
                    }
                    else if (thisDMLType == dmlType.DELETE_OBJECT){
                        if (sobj[0].getSObjectType().getDescribe().isDeletable()) {
                            Database.Deleteresult[] Upsres = Database.delete(sobj,false);
                            errors.addall(UtilDML.returnDMLErrorRecords(Upsres, null));
                        }
                        else
                            errors.add(new Debug_Error__c(Error__c = sobj[0].getSObjectType().getDescribe().getName() + ' ' + NO_DELETE_PERMISSION));
                    }
                    else if (thisDMLType == dmlType.UNDELETE_OBJECT){
                        if (sobj[0].getSObjectType().getDescribe().isUnDeletable()) {
                            Database.Undeleteresult[] Upsres = Database.undelete(sobj,false);
                            errors.addall(UtilDML.returnDMLErrorRecords(Upsres, null));
                        }
                        else
                            errors.add(new Debug_Error__c(Error__c = sobj[0].getSObjectType().getDescribe().getName() + ' ' + NO_UNDELETE_PERMISSION));
                    }
                }
                else{ 
                    if (thisDMLType == dmlType.INSERT_OBJECT){ 
                        if (sobj[0].getSObjectType().getDescribe().isCreateable()) insert sobj;
                    }else if (thisDMLType == dmlType.UPDATE_OBJECT){
                        if (sobj[0].getSObjectType().getDescribe().isUpdateable()) update sobj; 
                    }else if (thisDMLType == dmlType.UPSERT_OBJECT){
                        return false;//NOT ALLOWED ON GENERIC SOBJECT
                    }else if (thisDMLType == dmlType.DELETE_OBJECT){
                        if (sobj[0].getSObjectType().getDescribe().isDeletable()) delete sobj;
                    }else if (thisDMLType == dmlType.UNDELETE_OBJECT)
                        if (sobj[0].getSObjectType().getDescribe().isUnDeletable()) undelete sobj;
                }
            }
            else{
                if (processByBatch){
                    processRecordsByBatch(sobj, processByBatch, batchQuantity, errors, saveErrors, thisDMLType, setErrorMsg, trigOff);
                }
                else{
                    UtilsMonitoring.buildMonitoringMessage(UtilDML_Master.class, setErrorMsg, 'Could not save records. Processing by batch was not allowed.',null);
                    processMonitoringMsg = true;
                }
            } 
            try{
                if (saveErrors)
                    insert errors;
            }
            catch(Exception ex){
                processMonitoringMsg = true;
                UtilsMonitoring.buildMonitoringMessage(UtilDML_Master.class, setErrorMsg, 'Could not save error messages.' + ex.getMessage(),null);
            } 
            
            if (trigOff != TRIGGER_NONE){
                //this resets trigger control
                if (trigOff == TRIGGER_ALL)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_NONE, false);
                else if (trigOff == TRIGGER_INSERT)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_INSERT, false);
                else if (trigOff == TRIGGER_UPDATE)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_UPDATE, false);
                else if (trigOff == TRIGGER_DELETE)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_DELETE, false);
                else if (trigOff == TRIGGER_UNDELETE)
                    TriggerController.setTriggerControlValue(convertedType, TriggerController.TRIGGER_UNDELETE, false);
            } 
            
            if (processMonitoringMsg){
                UtilsMonitoring.saveMonitoringMesages(UtilDML_Master.class);
                return false;
            }
            else
                return true;
        }
        catch(Exception ex){
            UtilsMonitoring.buildMonitoringMessage(UtilDML_Master.class, setErrorMsg, setErrorMsg + ' failed ' + ex.getMessage(),null);
            if (saveErrors) UtilsMonitoring.saveMonitoringMesages(UtilDML_Master.class); 
            return false; 
        } 
    } 
    global static Boolean processRecordsByBatch(Sobject[] sobj, Boolean processByBatch, Integer batchQuantity, Debug_Error__c[] errors, Boolean saveErrors, dmlType thisDMLType, String setErrorMsg, String trigOff){
        Boolean processMonitoringMsg = false;
        String strng = 'Could not save records. Processing by batch was not allowed as context is already in batch or a future.';
        if (!System.isBatch() && !System.isFuture()) {
            if (thisDMLType == dmlType.INSERT_OBJECT) {
                if (sobj[0].getSObjectType().getDescribe().isCreateable()) 
                    Database.executebatch(new batchProcessDMLConsolidation(sobj, 'INSERT'), (batchQuantity != null) ? batchQuantity : 200);
                else
                    if (errors != null) errors.add(new Debug_Error__c(Error__c = sobj[0].getSObjectType().getDescribe().getName() + ' ' + NO_CREATE_PERMISSION));
            }else if (thisDMLType == dmlType.UPDATE_OBJECT){ 
                if (sobj[0].getSObjectType().getDescribe().isUpdateable()) 
                    Database.executebatch(new batchProcessDMLConsolidation(sobj, 'UPDATE'), (batchQuantity != null) ? batchQuantity : 200);
                else
                    if (errors != null) errors.add(new Debug_Error__c(Error__c = sobj[0].getSObjectType().getDescribe().getName() + ' ' + NO_UPDATE_PERMISSION));
            }else if (thisDMLType == dmlType.UPSERT_OBJECT)//NOT ALLOWED ON GENERIC SOBJECT
                return false;
            else if (thisDMLType == dmlType.DELETE_OBJECT){
                if (sobj[0].getSObjectType().getDescribe().isDeletable()) 
                    Database.executebatch(new batchProcessDMLConsolidation(sobj, 'DELETE'), (batchQuantity != null) ? batchQuantity : 200);
                else
                    if (errors != null) errors.add(new Debug_Error__c(Error__c = sobj[0].getSObjectType().getDescribe().getName() + ' ' + NO_DELETE_PERMISSION));
            }else if (thisDMLType == dmlType.UNDELETE_OBJECT){
                if (sobj[0].getSObjectType().getDescribe().isUnDeletable()) 
                    Database.executebatch(new batchProcessDMLConsolidation(sobj, 'UNDELETE'), (batchQuantity != null) ? batchQuantity : 200);
                else
                    if (errors != null) errors.add(new Debug_Error__c(Error__c = sobj[0].getSObjectType().getDescribe().getName() + ' ' + NO_UNDELETE_PERMISSION));
            }
        }else{
            UtilsMonitoring.buildMonitoringMessage(UtilDML_Master.class, setErrorMsg, strng,null);
            processMonitoringMsg = true; 
        } 
        return processMonitoringMsg;
    } 
    
}