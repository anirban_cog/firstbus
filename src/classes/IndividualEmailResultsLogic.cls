/*************************************************************
* Author: Paramita Roy Halder
* Created Date: 01/03/2018
* Version1: B-16873 and B-16874
* Details: Logic class to be invoked by IndividualEmailResultsHandler class 
* Purpose: Trigger logic class
**************************************************************/
public class IndividualEmailResultsLogic
{   
    public void onBeforeInsert(et4ae5__IndividualEmailResult__c[] newIndividualEmailResultObjects, CommitHandler ch)
    {
        system.debug('*****************inside IndividualEmailResultsLogic --> onBeforeInsert*****************');
    }
    public void onAfterInsert(et4ae5__IndividualEmailResult__c[] newIndividualEmailResultObjects, Map<id,SObject> newmap, CommitHandler ch)
    {
        system.debug('*****************inside IndividualEmailResultsLogic --> onAfterInsert*****************');
    }
    
    public void onBeforeUpdate(et4ae5__IndividualEmailResult__c[] oldIndividualEmailResultObjects, et4ae5__IndividualEmailResult__c[] newIndividualEmailResultObjects, Map<id,SObject> oldmap, Map<id,SObject> newmap, CommitHandler ch)
    {
        system.debug('*****************inside IndividualEmailResultsLogic --> onBeforeUpdate*****************');
    }
    
    public void onAfterUpdate(et4ae5__IndividualEmailResult__c[] oldIndividualEmailResultObjects, et4ae5__IndividualEmailResult__c[] newIndividualEmailResultObjects, Map<id,SObject> oldmap, Map<id,SObject> newmap, CommitHandler ch)
    {
        system.debug('*****************inside IndividualEmailResultsLogic --> onAfterUpdate*****************');
        system.debug('*****************newIndividualEmailResultObjects*****************' + newIndividualEmailResultObjects);
        List<Id> lstContactId = new List<Id>();
        if(newIndividualEmailResultObjects != null && newIndividualEmailResultObjects.size() > 0)
        {
            for(et4ae5__IndividualEmailResult__c ier : newIndividualEmailResultObjects)
            {
                if(ier.et4ae5__Contact__c != null)
                    lstContactId.add(ier.et4ae5__Contact__c);
            }
        }
        system.debug('*****************lstContactId*****************' + lstContactId);
        
        List<Contact> lstContact = [Select Id, Email_Opened__c  from Contact where Id IN: lstContactId];
        system.debug('*****************lstContact*****************' + lstContact);
        Map<Id, Contact> mapContactId = new Map<Id, Contact>(lstContact);
        system.debug('*****************mapContactId*****************' + mapContactId);
        
        List<Contact> lstContactsToUpdate = new List<Contact>();
        List<et4ae5__IndividualEmailResult__c> emailResultTobeDeleted = new List<et4ae5__IndividualEmailResult__c>();
         
        for(et4ae5__IndividualEmailResult__c ier : newIndividualEmailResultObjects)
        {
            if(ier.et4ae5__Opened__c == true && ier.et4ae5__DateOpened__c != null && mapContactId.containsKey(ier.et4ae5__Contact__c))
            {
                Contact con = mapContactId.get(ier.et4ae5__Contact__c);
                if((con.Email_Opened__c != null && con.Email_Opened__c < ier.et4ae5__DateOpened__c) || con.Email_Opened__c == null)
                {
                    con.Email_Opened__c = ier.et4ae5__DateOpened__c;
                    lstContactsToUpdate.add(con);
                    et4ae5__IndividualEmailResult__c ierTemp = new et4ae5__IndividualEmailResult__c ();
                    ierTemp.Id = ier.Id;
                    emailResultTobeDeleted.add(ierTemp);
                }
            }               
        }
        system.debug('*****************lstContactsToUpdate*****************' + lstContactsToUpdate);
        system.debug('*****************emailResultTobeDeleted*****************' + emailResultTobeDeleted);
        UtilDML_Master.updateObjects(lstContactsToUpdate);
        UtilDML_Master.deleteObjects(emailResultTobeDeleted); 
    }
    
    public void onBeforeDelete(et4ae5__IndividualEmailResult__c[] oldIndividualEmailResultObjects, Map<id,SObject> oldmap, CommitHandler ch)
    {
        system.debug('*****************inside IndividualEmailResultsLogic --> onBeforeDelete*****************');
    } 
    
    public void onAfterDelete(et4ae5__IndividualEmailResult__c[] oldIndividualEmailResultObjects, Map<id,SObject> oldmap, CommitHandler ch)
    {
        system.debug('*****************inside IndividualEmailResultsLogic --> onAfterDelete*****************');
    } 
    
    public void onBeforeUnDelete(et4ae5__IndividualEmailResult__c[] oldIndividualEmailResultObjects, Map<id,SObject> oldmap, CommitHandler ch)
    {
        system.debug('*****************inside IndividualEmailResultsLogic --> onBeforeUnDelete*****************');
    } 
    
    public void onAfterUnDelete(et4ae5__IndividualEmailResult__c[] oldIndividualEmailResultObjects,  et4ae5__IndividualEmailResult__c[] newIndividualEmailResultObjects, Map<id,SObject> oldmap, Map<id,SObject> newmap, CommitHandler ch)
    {
        system.debug('*****************inside IndividualEmailResultsLogic --> onAfterUnDelete*****************');
    }
}