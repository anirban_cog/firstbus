/*************************************************************
* Author: Paramita Roy Halder
* Created Date: 11/14/17
* Version1: B-16811
* Details: Handler class to be invoked from contact trigger
* Purpose: Trigger handler class
**************************************************************/
public with sharing class ContactHandler extends TriggerBaseHandler{
    //the handler passes to each Logic class to perform whatever actions this allows the handler to have multiple logic
    // classes for different purposes and sobj that this class doesnt grow exponentially
    private ContactLogic contactLogic;
    
    public ContactHandler () {
        //Note can create multiple instances of different classes if required to segregate functionality into appropriate classes
        this.contactLogic = new ContactLogic();
    }

    private static ContactHandler instance; 

    public static ContactHandler getInstance() {
        System.debug(LoggingLevel.FINE, 'Executing static method " ContactHandler.getInstance"...');

        if (instance == null) instance = new ContactHandler();
        return instance;
    } 
    
    public override void beforeInsert(SObject[] newObjects, CommitHandler ch){
        contactLogic.onBeforeInsert((Contact[])newObjects, ch); 
    }
    
    public override void afterInsert(Sobject[] newObjects, Map<id,SObject> newmap, CommitHandler ch){
        //ch is optional.Pass if DML Consolidation is required.
        contactLogic.onAfterInsert((Contact[])newObjects, newmap, ch); 
    }
    
    public override void beforeUpdate(SObject[] oldObjects, SObject[] newObjects, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){
        contactLogic.onBeforeUpdate((Contact[])oldObjects, (Contact[])newObjects, oldmap, newmap, ch);
    }
    
    public override void afterUpdate(SObject[] oldObjects, SObject[] newObjects, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){
        contactLogic.onAfterUpdate((Contact[])oldObjects, (Contact[])newObjects, oldmap, newmap, ch);
    }
    
    public override void beforeDelete(SObject[] oldObjects, Map<id,SObject> oldmap, CommitHandler ch){ 
        contactLogic.onBeforeDelete((Contact[])oldObjects, oldmap, ch);
    } 
    
    public override void afterDelete(SObject[] oldObjects, Map<id,SObject> oldmap, CommitHandler ch){
        contactLogic.onAfterDelete((Contact[])oldObjects, oldmap, ch);
    } 
    
    public override void beforeUnDelete(SObject[] oldObjects, Map<id,SObject> oldmap, CommitHandler ch){ 
        contactLogic.onBeforeUnDelete((Contact[])oldObjects, oldmap, ch);
    } 
    
    public override void afterUnDelete(SObject[] oldObjects, SObject[] newObjects, Map<id,SObject> oldmap, Map<id,SObject> newMap, CommitHandler ch){
        contactLogic.onAfterUnDelete((Contact[])oldObjects, (Contact[])newObjects, oldmap, newmap, ch);
    } 

}