trigger IndividualEmailResultsTrigger on et4ae5__IndividualEmailResult__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(et4ae5__IndividualEmailResult__c.sObjectType, 'IndividualEmailResultsHandler');
    if (UtilsMonitoring.getMonitoringCoverage())
        UtilsMonitoring.saveMonitoringMesages();
    else 
        UtilsMonitoring.saveMonitoringMesages(et4ae5__IndividualEmailResult__c.class);
}