/*************************************************************
* Author: Steve Fouracre
* Created Date: 12/07/17
* Version1: 
* Details: CaseTrigger on Case object 
**************************************************************/
trigger CaseTrigger on Case (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    TriggerFactory.createHandler(Case.sObjectType, 'CaseHandler');
    if (UtilsMonitoring.getMonitoringCoverage())
        UtilsMonitoring.saveMonitoringMesages();
    else 
        UtilsMonitoring.saveMonitoringMesages(Case.class);
    
}