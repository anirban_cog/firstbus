trigger trgContact on Contact (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(Contact.sObjectType, 'ContactHandler');
    if (UtilsMonitoring.getMonitoringCoverage())
        UtilsMonitoring.saveMonitoringMesages();
    else 
        UtilsMonitoring.saveMonitoringMesages(Contact.class);
}