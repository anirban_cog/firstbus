/*************************************************************
* Author: Ayan Hore
* Created Date: 11/21/17
* Version1: B-16760, B-16759
* Details: Trigger to be invoked on after insert and after update context 
* Purpose:Trigger on Purchases Processor object
**************************************************************/
trigger PurchasesProcessorTrigger on Purchases_Processor__c (before insert, after insert, before update, after update) {    
    PurchasesProcessorHandler purchaseHandler = new PurchasesProcessorHandler();
    if(Trigger.isInsert){       
        /*Processing for Insert context*/
        if(Trigger.isBefore){
            /*Processing for Before context*/
            purchaseHandler.beforeInsert(Trigger.New, new CommitHandler());
        }
    }
    
    if(Trigger.isUpdate){
        /*Processing for Update context*/
        if(Trigger.isBefore){
            /*Processing for Before context*/
            purchaseHandler.beforeUpdate(Trigger.Old, Trigger.New, Trigger.OldMap, Trigger.NewMap, new CommitHandler());
        }
    }
    
}