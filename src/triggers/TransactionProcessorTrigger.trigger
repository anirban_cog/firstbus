/*************************************************************
* Author: Rajat Tripathi
* Created Date: 11/22/17
* Version1: B-16759, B-16891, B-16893
* Details: Trigger to be invoked on after insert and after update context 
* Purpose:Trigger on Transaction Processor object
**************************************************************/
trigger TransactionProcessorTrigger on Transaction_Processor__c (before insert, after insert, before update, after update) {
    TransactionProcessorHandler transactionHandler = new TransactionProcessorHandler();
    if(Trigger.isInsert){       
        /*Processing for Insert context*/
        if(Trigger.isBefore){
            /*Processing for Before context*/
            transactionHandler.beforeInsert(Trigger.New, new CommitHandler());
        }
    }
    
    if(Trigger.isUpdate){
        /*Processing for Update context*/
        if(Trigger.isBefore){
            /*Processing for Before context*/
            transactionHandler.beforeUpdate(Trigger.Old, Trigger.New, Trigger.OldMap, Trigger.NewMap, new CommitHandler());
        }
    }
    
}