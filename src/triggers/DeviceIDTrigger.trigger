/*************************************************************
* Author: Ayan Hore
* Created Date: 12/07/17
* Version1: B-16878
* Details: Trigger for Device_ID__c object 
* Purpose:Trigger for Device_ID__c object 
**************************************************************/
trigger DeviceIDTrigger on Device_ID__c (before insert, after insert, before update, after update) {
    DeviceIDHandler deviceHandler = new DeviceIDHandler();
    if(Trigger.isInsert){       
        /*Processing for Insert context*/
        if(Trigger.isBefore){
            /*Processing for Before context*/
            deviceHandler.beforeInsert(Trigger.New, new CommitHandler());
        }
    }
    
    if(Trigger.isUpdate){
        /*Processing for Update context*/
        if(Trigger.isBefore){
            /*Processing for Before context*/
            deviceHandler.beforeUpdate(Trigger.Old, Trigger.New, Trigger.OldMap, Trigger.NewMap, new CommitHandler());
        }
    }
}