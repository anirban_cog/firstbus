<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Case Sub Detail Field Rule10</label>
    <protected>false</protected>
    <values>
        <field>Display_Type__c</field>
        <value xsi:type="xsd:string">Checkbox</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Picklist_Value__c</field>
        <value xsi:type="xsd:string">Vehicle full</value>
    </values>
</CustomMetadata>
