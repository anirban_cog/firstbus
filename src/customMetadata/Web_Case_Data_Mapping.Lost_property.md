<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lost property</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Display_TBC_For_Category__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Display_TBC_For_Sub_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Display_TBC_For_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Reason__c</field>
        <value xsi:type="xsd:string">Lost property</value>
    </values>
    <values>
        <field>Sub_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Lost Property</value>
    </values>
</CustomMetadata>
